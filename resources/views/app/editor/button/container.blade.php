
	<div class="box box-primary direct-chat direct-chat-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Página de enlace <span class="direct-chat-timestamp">(arrastre la página a la que enlazará este botón)</span>
      	&nbsp;<span data-toggle="tooltip" title="Total de elementos" id="list_add_zone_total" class="badge bg-aqua">{{count($items)}}</span>
      </h3>
      <div class="box-tools pull-right">
      	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<ul class="list-container direct-chat-messages container-border container-button" id="list_add_zone"></ul>
    </div>
    <div class="box-footer">

    </div>
  </div>

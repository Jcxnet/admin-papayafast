<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Menu;
use App\Comercio;
use App\Local;

use Validator;


class MenusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show all comercios in database to show menus
     * @return View
     */
    public function index(){
    	if(!$this->userhasPermission('list-menu'))
    		return $this->ajaxError('No tiene permiso para realizar esta acción');
      if($this->userhasRole('admin'))
      	$comercios = Comercio::orderBy('name')->get();
      else
      	$comercios = Comercio::orderBy('name')->where('id',$this->userComercio())->get();
     	$comercios = $this->getCollection($comercios->toArray(),'App\Comercio','App\Http\Transformers\ComercioTransformer');
      return view('app.menus.index',['comercios'=>$comercios]);
    }

    /**
     * retorna la información de un Menú
     * @param  Request $request
     * @return json
     */
    public function comercio(Request $request){
	   	if($request->ajax()){
	   		if(!$comercio = Comercio::where('uuid',$request->id)->first())
	   			return $this->ajaxError('No se pudo encontrar el comercio');
	   		if(!$menus = Menu::where('comercio_id',$comercio->id)->orderBy('title')->get())
	   			return $this->ajaxError('No se encontraron menús');
	   		$menus 	= $this->getCollection($menus->toArray(),'App\Menu','App\Http\Transformers\MenucomercioTransformer');
	   		$locals = $this->getCollection($comercio->locals->toArray(),'App\Local','App\Http\Transformers\LocalmenuTransformer');
	   		$menus  = $this->getLocals($menus,$locals);
	   		return $this->ajaxData($this->tableData($menus));
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * retorna los Locales que utilizan un Menú
     * @param  array $menus
     * @param  array $locals
     * @return array
     */
    private function getLocals($menus, $locals){
    	$list = [];
    	$menus = collect($menus)->keyBy('id')->toArray();
    	foreach ($locals as $local) {
    		$local['menu'] = collect($local['menu'])->flatten()->toArray();
    		foreach ($local['menu'] as $menu => $value) {
    			if($value != ''){
    				if(array_key_exists($value,$menus))
    					$menus[$value]['locals'][] = ['id'=>$local['id'],'name'=>$local['name']];
    			}
    		}
    		$list[] = ['id'=>$local['id'],'name'=>$local['name']];
    	}
    	foreach ($menus as $key => $info) {
    		if(array_key_exists('locals',$menus[$key])){
    			$locals = collect($menus[$key]['locals'])->keyBy('id');
    			$list 	= collect($list)->keyBy('id');
    			$menus[$key]['select'] = $list->diffKeys($locals)->sortBy('name')->values()->toArray();
    			$menus[$key]['locals'] = collect($menus[$key]['locals'])->sortBy('name')->values()->toArray();
    		}else{
    			$menus[$key]['select'] = collect($list)->sortBy('name')->values()->toArray();
    		}
    	}
    	return $menus;
    }

    /**
     * formatea la información de un Menú
     * @param  array $menus
     * @return array
     */
    private function tableData($menus){
    	$list = [];
    	foreach($menus as $menu){
    		$list[] = $this->tableCols($menu);
    	}
    	return $list;
    }

    /**
     * formatea la información de un Menú
     * @param  array $menu
     * @return array
     */
    private function tableCols($menu){
    	$cols =[];
    	if(!array_key_exists('locals',$menu))
    		$menu['locals'] = [];
    	if(!array_key_exists('select',$menu))
    		$menu['select'] = [];
    	$cols['menu'] 		= view('app.menus.item.menu',	 	['menu' => $menu])->render();
		  $cols['type'] 		= view('app.menus.item.type',		['type' => $menu['type'], 'active' => $menu['active']])->render();
		  $cols['totals'] 	= view('app.menus.item.totals', ['totals' => $menu['totals']])->render();
		  $cols['locals'] 	= view('app.menus.item.locals',	['locals' => $menu['locals'], 'select' => $menu['select'], 'id' => $menu['id']])->render();
		  $cols['actions'] 	= view('app.menus.item.actions',['id' => $menu['id'], 'active' => $menu['active']])->render();
		  $cols['id']				= $menu['id'];
		  //$cols['totals']		= $menu['totals'];
    	return $cols;
    }

    /**
     * elimina la asignación de un Menú para un Local
     * @param  Request $request
     * @return json
     */
    public function deleteLocal(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('deletelocal-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($this->userhasRole('admin')){
    			$local = Local::where('uuid',$request->local)->first();
    		}else{
    			$local = Local::where('uuid',$request->local)->where('comercio_id',$this->userComercio())->first();
    		}
    		if(!$local)
    			return $this->ajaxError('No se encontró el Local');
				if(!$menu = Menu::where('uuid',$request->menu)->first())
    			return $this->ajaxError('No se encontró el Menú');
    		if($this->userhasRole('admin'))
    			$comercio = Comercio::where('id',$local->comercio_id)->first();
    		else
    			$comercio = Comercio::where('id',$this->userComercio())->first();
    		if(!$comercio)
    			return $this->ajaxError('No se encontró el Comercio');
    		$menus = $local->menu;
    		$list  = [];
    		foreach ($menus as $item) {
    			$type = strtolower($menu->type);
    			if(array_key_exists($type,$item)){
    				if($item[$type] == $menu->uuid)
    					$item[$type] = '';
    			}
    			$list[] = $item;
    		}
    		$local->menu = $list;
    		$local->save();
    		$locals = $this->getCollection($comercio->locals->toArray(),'App\Local','App\Http\Transformers\LocalmenuTransformer');
    		$menu 	= $this->getItem($menu,'App\Http\Transformers\MenucomercioTransformer');
    		$menu 	= $this->formatMenu($menu,$locals);
    		$menu['cols'] = json_encode($this->tableCols($menu));
    		return $this->ajaxData($menu);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * elimina todos los Locales asignados a un Menú
     * @param  Request $request
     * @return json
     */
    public function deleteAllLocals(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('deletelocal-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
				if($this->userhasRole('admin'))
    			$menu = Menu::where('uuid',$request->menu)->first();
    		else
    			$menu = Menu::where('uuid',$request->menu)->where('comercio_id',$this->userComercio())->first();
				if(!$menu)
    			return $this->ajaxError('No se encontró el Menú');
    		if($this->userhasRole('admin'))
    			$comercio = Comercio::where('id',$menu->comercio_id)->first();
    		else
    			$comercio = Comercio::where('id',$this->userComercio())->first();
    		if(!$comercio)
    			return $this->ajaxError('No se encontró el Comercio');
    		if(!$locals = Local::where('comercio_id',$comercio->id)->get())
    			return $this->ajaxError('No se encontraron Locales');

    		$this->removeMenuFromLocals($locals, $menu);

    		$locals = $this->getCollection($comercio->locals->toArray(),'App\Local','App\Http\Transformers\LocalmenuTransformer');
    		$menu 	= $this->getItem($menu,'App\Http\Transformers\MenucomercioTransformer');
    		$menu 	= $this->formatMenu($menu,$locals);
    		$menu['cols'] = json_encode($this->tableCols($menu));
    		return $this->ajaxData($menu);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * elimina la referecnia del Menú en los Locales
     * @param  array $locals
     * @param  array $menu
     * @return void
     */
    private function removeMenuFromLocals($locals, $menu){
    	foreach($locals as $local){
  			$menus = $local->menu;
    		$list  = [];
    		foreach ($menus as $item) {
    			$type = strtolower($menu->type);
    			if(array_key_exists($type,$item)){
    				if($item[$type] == $menu->uuid)
    					$item[$type] = '';
    			}
    			$list[] = $item;
    		}
    		$local->menu = $list;
    		$local->save();
  		}
    }

    /**
     * agrega la referencia de un Menú a un Local
     * @param Request $request
     * @return json
     */
    public function addLocal(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('addlocal-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$locals = Local::whereIn('uuid',$request->locals)->get())
    			return $this->ajaxError('No se encontraron locales');
				if(!$menu = Menu::where('uuid',$request->menu)->first())
    			return $this->ajaxError('No se encontró el Menú');
    		if($this->userhasRole('admin')){
    			$comercio = Comercio::where('id',$menu->comercio_id)->first();
    		}else{
    			$comercio = Comercio::where('id',$this->userComercio())->first();
    		}
    		if(!$comercio)
    			return $this->ajaxError('No se encontró el Comercio');
    		foreach ($locals  as $local) {
    			$menus = $local->menu;
	    		$list  = [];
	    		foreach ($menus as $item) {
	    			$type = strtolower($menu->type);
	    			if(array_key_exists($type,$item)){
	    					$item[$type] = $menu->uuid;
	    			}
	    			$list[] = $item;
	    		}
	    		$local->menu = $list;
	    		$local->save();
    		}
    		$locals = $this->getCollection($comercio->locals->toArray(),'App\Local','App\Http\Transformers\LocalmenuTransformer');
    		$menu 	= $this->getItem($menu,'App\Http\Transformers\MenucomercioTransformer');
    		$menu 	= $this->formatMenu($menu,$locals);
    		$menu['cols'] = json_encode($this->tableCols($menu));
    		return $this->ajaxData($menu);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * formatea la información del Menú
     * @param  array $menu
     * @param  array $locals
     * @return array
     */
    private function formatMenu($menu, $locals){
    	$list = [];
    	$menu['locals'] = [];
    	foreach ($locals as $local) {
    		$local['menu'] = collect($local['menu'])->flatten()->toArray();
    		foreach ($local['menu'] as $item => $value) {
    			if($value == $menu['id'])
    				$menu['locals'][] = ['id'=>$local['id'],'name'=>$local['name']];
    		}
    		$list[] = ['id'=>$local['id'],'name'=>$local['name']];
    	}
    	$locals = collect($menu['locals'])->keyBy('id');
    	$list 	= collect($list)->keyBy('id');
    	$menu['select'] = $list->diffKeys($locals)->sortBy('name')->values()->toArray();
    	$menu['locals'] = collect($menu['locals'])->sortBy('name')->values()->toArray();
    	return $menu;
    }

    /**
     * duplica un Menú
     * @param  Request $request
     * @return json
     */
    public function duplicate(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('clone-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$menu = Menu::where('uuid',$request->id)->first())
    			return $this->ajaxError('No se encontró el menú');
    		if($this->userhasRole('admin')){
    			$comercio = Comercio::where('id',$menu->comercio_id)->first();
    		}else{
    			$comercio = Comercio::where('id',$this->userComercio())->first();
    		}
    		if(!$comercio)
    			return $this->ajaxError('No se encontró el comercio');
    		$newMenu = $menu->replicate();
				$faker = \Faker\Factory::create();
    		$newMenu->uuid = $faker->uuid();
    		$newMenu->version = date("YmjHi00");
    		$newMenu->save();
    		$newMenu->fresh();

    		$locals = $this->getCollection($comercio->locals->toArray(),'App\Local','App\Http\Transformers\LocalmenuTransformer');
    		$menu 	= $this->getItem($newMenu,'App\Http\Transformers\MenucomercioTransformer');
    		$menu 	= $this->formatMenu($menu,$locals);
    		$menu['cols'] = json_encode($this->tableCols($menu));
    		return $this->ajaxData($menu);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * agrega / edita un Menú
     * @param Request $request
     * @return json
     */
    public function add(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('add-menu') && !$this->userhasPermission('edit-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($this->userhasRole('admin')){
    			$comercio = Comercio::where('id',$request->comercio)->first();
    		}else{
    			$comercio = Comercio::where('id',$this->userComercio())->first();
    		}
    		if(!$comercio)
    			return $this->ajaxError('No se encontró el Comercio');
    		if(!$menu = Menu::where('uuid',$request->id)->first()){
    			$menu = new Menu();
    			$faker 				= \Faker\Factory::create();
    			$menu->uuid 	= $faker->uuid();
    		}
  			$data = $request->only(['name','type']);
  			$validator = Validator::make($data, [
		      'name'	=> 'required|string|min:4|max:100',
		      'type' 	=> 'required|string|in:DELIVERY,TAKEIN,TAKEOUT',
		    ]);
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    try{
		    	$menu->title	= $data['name'];
					$menu->type 	= $data['type'];
					$menu->version = date("YmjHi00");
    			$menu->comercio_id = $comercio->id;
					$menu->save();
					$menu->fresh();

					$locals = $this->getCollection($comercio->locals->toArray(),'App\Local','App\Http\Transformers\LocalmenuTransformer');
	    		$menu 	= $this->getItem($menu,'App\Http\Transformers\MenucomercioTransformer');
	    		$menu 	= $this->formatMenu($menu,$locals);
	    		$menu['cols'] = json_encode($this->tableCols($menu));
	    		return $this->ajaxData($menu);
		    }catch(Exception $e){
		    	return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
		    }
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }


    /**
     * elimina un Menú
     * @param  Request $request
     * @return json
     */
    public function delete(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('delete-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($this->userhasRole('admin'))
    			$menu = Menu::where('uuid',$request->id)->first();
    		else
    			$menu = Menu::where('uuid',$request->id)->where('comercio_id',$this->userComercio())->first();
    		if(!$menu)
    			return $this->ajaxError('No se encontró el menú');
    		if(!$comercio = Comercio::where('id',$menu->comercio_id)->first())
    			return $this->ajaxError('No se encontró el comercio');
    		if($locals = Local::where('comercio_id',$comercio->id)->get())
    			$this->removeMenuFromLocals($locals, $menu);
    		$menu->title .= sprintf("[deleted %s]",date('Y-m-j H:i:s'));
    		$menu->save();
    		$menu->delete();
    		return $this->ajaxData([],'ok','Menú eliminado');
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * activa / desactiva un Menú
     * @param  Request $request
     * @return json
     */
    public function activate(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('activate-menu') && !$this->userhasPermission('deactivate-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($this->userhasRole('admin'))
    			$menu = Menu::where('uuid',$request->id)->first();
    		else
    			$menu = Menu::where('uuid',$request->id)->where('comercio_id',$this->userComercio())->first();
    		if(!$menu)
    			return $this->ajaxError('No se encontró el menú');
    		if(!$comercio = Comercio::where('id',$menu->comercio_id)->first())
    			return $this->ajaxError('No se encontró el comercio');

    		$menu->active = !$menu->active;
    		$menu->save();
    		$menu->fresh();

    		$locals = $this->getCollection($comercio->locals->toArray(),'App\Local','App\Http\Transformers\LocalmenuTransformer');
    		$menu 	= $this->getItem($menu,'App\Http\Transformers\MenucomercioTransformer');
    		$menu 	= $this->formatMenu($menu,$locals);
    		$menu['cols'] = json_encode($this->tableCols($menu));
    		return $this->ajaxData($menu);
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * vista previa de un Menú
     * @param  Request $request
     * @return json
     */
    public function preview(Request $request){
	  	if($request->ajax()){
	  		if(!$this->userhasPermission('preview-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($this->userhasRole('admin'))
    			$menu = Menu::where('uuid',$request->id)->first();
    		else
    			$menu = Menu::where('uuid',$request->id)->where('comercio_id',$this->userComercio())->first();
	  		if(!$menu)
	  			return $this->ajaxError('No se encontró el menú');
	  		if(!$comercio = Comercio::where('id',$menu->comercio_id)->first())
    			return $this->ajaxError('No se encontró el comercio');
    		$item = ['menus'=>[$menu->uuid]];
    		$item = $this->sendData( config('apiendpoints.API_MENUS_LIST'),$item,[],"POST",$comercio->apikey->token);

    		if($item['meta']['status'] != 'ok')
    			return $this->ajaxError('No se pudo obtener información del menú');
    		$data['title'] = $menu->title;
    		$data['version'] = $menu->version;
    		$data['comercio'] = $comercio->name;
    		$data['menu'] = $this->menuPageLinks($item['data'][0]);

    		$menu = $data['menu'];
    		if(!$menu['page'])
    			$page = collect($menu['pages'])->first();
    		else
    			$page = $menu['pages'][(integer)$menu['page']];

    		$items =  $this->typeItems([],$page['buttons'],$menu['buttons'],'button');
    		$items =  $this->typeItems($items,$page['products'], $menu['products'], 'product');
    		$items = collect($items)->sortBy('order')->toArray();

    		$view = view('app.editor.page.preview',['page'=>$page, 'items'=>$items, 'back' => $page['back']]);
    		$data['content'] = $view->render();

    		return $this->ajaxData($data);

	  	}else{
	   		return $this->ajaxError('Acción no permitida',[],401);
	   	}
    }

    /**
     * define los enlaces a las páginas de cada botón así como el enlace de retorno
     * @param  array $menu
     * @return array
     */
    private function menuPageLinks($menu){
	  	$pages = $menu['pages'];
	  	$buttons = $menu['buttons'];
	  	foreach ($pages as $page) {
	  		foreach($page['buttons'] as $button){
	  			$idpage = ($buttons[$button]['page'])?(integer)$buttons[$button]['page']:false;
	  			if($idpage){
	  				$pages[$idpage]['back'] = $page['id'];
	  			}
	  		}
	  	}
	  	$menu['pages'] = $pages;
	  	return $menu;
	  }

    /**
     * retorna la vista previa de una página
     * @param  Request $request
     * @return json
     */
    public function previewPage(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('preview-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$data = $request->only(['page','back']);
    		if(!$page = json_decode($data['page'],true))
    			return $this->ajaxError('No se encontró la página, intente nuevamente');
    		$items = collect($page['buttons'])->merge($page['products'])->sortBy('order')->toArray();
    		$view = view('app.editor.page.preview',['page'=>$page, 'items'=>$items, 'back' => $page['back']]);
    		return $this->ajaxData(['content' => $view->render()]);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * clasifica los ítems de un menú
     * @param  array $list
     * @param  array $ids
     * @param  array $items
     * @param  array $type
     * @return array
     */
    private function typeItems($list, $ids, $items, $type){
    	$tmp = collect($items)->whereIn('id',$ids)->toArray();
    	foreach($tmp as $item){
    		$item['type']  = $type;
    		$item['order'] = (float)$item['order'];
    		$list[] = $item;
    	}
    	return $list;
    }



}
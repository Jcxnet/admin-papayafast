<div class="box box-primary box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-th-large" aria-hidden="true"></i>
&nbsp;Elementos</h3>
    </div>
    <div class="box-body">
      <div class="accordion-container">
			  @include('app.editor.item.accordionset',[
			  		'header'		=> '<i class="fa fa-file-text-o"></i>&nbsp;Páginas',
			  		'type'			=> 'page',
			  		'items'			=> $menu['pages'],
			  		'bgcolor' 	=> 'bg-blue',
			  		'typename' 	=> 'página',
			  		'plural'		=> 'páginas',
			  		'main'			=> $menu['page']])
			  @include('app.editor.item.accordionset',[
			  		'header'		=> '<i class="fa fa-external-link-square"></i>&nbsp;Botones',
			  		'type'			=> 'button',
			  		'items' 		=> $menu['buttons'],
			  		'bgcolor' 	=> 'bg-aqua',
			  		'typename' 	=> 'botón',
			  		'plural'		=> 'botones',
			  		'main'			=> false])
			  @include('app.editor.item.accordionset',[
			  		'header' 		=> '<i class="fa fa-tasks"></i>&nbsp;Productos',
			  		'type' 			=> 'product',
			  		'items' 		=> $menu['products'],
			  		'bgcolor' 	=> 'bg-green',
			  		'typename' 	=> 'producto',
			  		'plural'		=> 'productos',
			  		'main'			=> false])
			</div>
    </div>
    <div class="box-footer">
    	<div class="row">
    		<div class="col-sm-6">
    			<div class="small-box bg-red">
		        <div class="drop-zone sidebar-zone inner" id="sidebar_drop_zone">
		        </div>
		        <div class="icon"><i class="fa fa-trash-o"></i></div>
		        <span class="small-box-footer">
		          arrastre un elemento para Eliminarlo
		        </span>
		      </div>
    		</div>
    		<div class="col-sm-6">
    			<div class="small-box bg-orange">
		        <div class="drop-zone sidebar-zone inner" id="sidebar_clone_zone">
		        </div>
		        <div class="icon"><i class="fa fa-clone"></i></div>
		        <span class="small-box-footer">
		          arrastre un elemento para Duplicarlo
		        </span>
		      </div>
    		</div>
    	</div>
    </div>
  </div>

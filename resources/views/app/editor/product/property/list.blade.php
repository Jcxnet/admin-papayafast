
	<div class="box box-success direct-chat direct-chat-primary" id="box-properties">
    <div class="box-header with-border">
      <h3 class="box-title">Propiedades <span class="direct-chat-timestamp">(elementos del producto que no cambian)</span>
      	&nbsp;<span data-toggle="tooltip" title="Total de elementos" id="list_properties_total" class="badge bg-green">{{count($items)}}</span>
      </h3>
      <div class="box-tools pull-right">
      	<a class="btn btn-box-tool bg-green" href="#" id="btn-add-property"><i class="fa fa-plus-square"></i></a>
      	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<ul class="list-container direct-chat-messages container-border" id="list_properties_zone"></ul>

    	<div class="direct-chat-contacts" id="form_property_container">
    		<div class="margin">
    			@include('app.editor.product.property.form')
    		</div>
	    </div>

    </div>

    <div class="box-footer">
    </div>
  </div>

<script type="text/javascript">

	$(function(){
		$('#btn-add-property').on('click',function(){
			$(this).fadeOut(500);
			//$('#txtpropertyid').val(getNextItemId('property'));
			$('#txtpropertyid').val(0);
    	$('#txtpropertyorder').val(getNextItemOrder('item-property'));
			showPropertyForm();
    	return false;
		});

		sortPropertiesList();
	});

	function sortPropertiesList(){
		if($('#list_properties_zone').length){
			Sortable.create(list_properties_zone, {
	  			group: {
	  				name: 'list_origin_properties',
	  				put: false, // add list from all groups
	  			},
	  			animation: 100,
	  			sort: true,
	  			dataIdAttr: 'sort-id',
	  			onAdd: function (evt) {
        		updateTotalContainer('#list_properties_zone','#list_properties_total');
    			},
	  			onSort: function (evt) {
        		updateTotalContainer('#list_properties_zone','#list_properties_total');
    			},
			});
		}
	}

	function getNextItemOrder(itemClass){
		var total = $('.'+itemClass).length;
		var items = $('.'+itemClass);
		var max = 0;
		for(i=0;i<total;i++){
			order = parseInt($(items[i]).attr('item-order'));
			max = (max<order)?order:max;
		}
		return max+1;
	}

</script>
<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Local;
use App\Comercio;
use App\Menu;
use Validator;


class LocalsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show all locals in database
     * @return View
     */
    public function index(){
    	if(!$this->userhasPermission('list-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
      if($this->userhasRole('admin'))
    		$comercios = Comercio::orderBy('name')->get();
    	else
    		$comercios = Comercio::where('id',$this->userComercio())->orderBy('name')->get();
     	$comercios = $this->getCollection($comercios->toArray(),'App\Comercio','App\Http\Transformers\ComercioTransformer');
     	return view('app.locals.index',['comercios'=>$comercios]);
    }

    /**
     * formatea la información de los locales
     * @param  Request $request
     * @return json
     */
    public function locals(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('format-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$locals = json_decode($request->locals,true);
    		$menus 	= json_decode($request->menus,true);
    		if(!$locals || count($locals)==0)
    			return $this->ajaxError('No se encontraron locales');
    		return $this->ajaxData($this->tableData($locals,$menus));
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * obtiene la información de un Local
     * @param  Request $request
     * @return json
     */
    public function info(Request $request){
	   	if($request->ajax()){
	   		if(!$this->userhasPermission('info-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($this->userhasRole('admin')){
	   			if(!$local = Local::where('uuid',$request->id)->first())
	   				return $this->ajaxError('No se pudo encontrar el local');
    		}else{
    			if(!$local = Local::where('uuid',$request->id)->where('comercio_id',$this->userComercio())->first())
	   				return $this->ajaxError('No se pudo encontrar el local');
    		}
				$local = $this->getItem($local,'App\Http\Transformers\LocalinfoTransformer');
				return $this->ajaxData($local);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * formatea los datos de un local
     * @param  array $locals
     * @param  array $menus
     * @return array
     */
    private function tableData($locals,$menus){
    	$list = [];
    	foreach($locals as $local){
    		$list[] = $this->tableCols($local,$menus);
    	}
    	return $list;
    }

    /**
     * agrega / edita un Local
     * @param Request $request
     * @return json
     */
    public function add(Request $request){
    	if($request->ajax()){
    		$request->info = json_decode($request->info,true);
    		$request->address = json_decode($request->address,true);
    		$request->contact = json_decode($request->contact,true);
    		$request->timetable = json_decode($request->timetable,true);
    		$request->zones = json_decode($request->zones,true);

    		if(!$this->userhasPermission('add-local') && !$this->userhasPermission('edit-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');

    		if($this->userhasRole('admin')){
    			if(!$comercio = Comercio::select('id')->where('uuid',$request->info['comercio'])->first()){
	    			return $this->ajaxError('No se pudo encontrar el comercio');
	    		}
    		}else{
    			if(!$comercio = Comercio::select('id')->where('uuid',$request->info['comercio'])->where('id',$this->userComercio())->first()){
	    			return $this->ajaxError('No se pudo encontrar el comercio');
	    		}
    		}

	    	if(!$local = Local::where('uuid',$request->info['local'])->first()){
	    		$local = new Local();
	    		$local->menu 	= $this->formatMenu();
	    	}
	    	// validate basic info
	    	$validator = Validator::make($request->info, [
		      'name'					=> 'required|string|min:4|max:100',
		      'code'					=> 'required|string',
		    ]);

		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }

		    // validate address
		    $validator = Validator::make($request->address, [
		      'lat'			=> 'required|numeric',
		      'lng'			=> 'required|numeric',
		      'address'	=> 'required|string'
		    ]);

		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }

		    //validate contact
		    foreach ($request->contact as $contact) {
		    	$validator = Validator::make($contact, [
			      'email'			=> 'email',
			      'phone'			=> 'numeric',
			    ]);

			    if($validator->fails()) {
			        $errors = implode('<br/>',$validator->errors()->all());
			        return $this->ajaxError($errors);
			    }
		    }

		    //validate zones
		    foreach ($request->zones as $id => $zonedata) {
		    	$zonedata['points'] = explode(',',$zonedata['points']);
		    	$validator = Validator::make($zonedata, [
			      'points'			=> 'array'
			    ]);

			    if($validator->fails()) {
			        $errors = implode('<br/>',$validator->errors()->all());
			        return $this->ajaxError($errors);
			    }

			    foreach ($zonedata['points'] as $point) {
			    	$validator = Validator::make(['point' =>$point], [
				      'point'			=> 'numeric'
				    ]);
				    if($validator->fails()) {
				        $errors = implode('<br/>',$validator->errors()->all());
				        return $this->ajaxError($errors);
				    }
			    }
		    }

		    try{
		    	$local->comercio_id = $comercio->id;
		    	$local->code  		= $request->info['code'];
					$local->name 			= $request->info['name'];
					$local->address  	= $this->formatAddress($request->info,$request->address);
					$local->contact 	= $this->formatContact($request->contact);
					$local->timetable = $this->formatTimetable($request->timetable);
					$local->zone 			= $this->formatZone($request->zones);
					$local->delivery	= true;
					$local->save();
					$local = Local::find($local->id);

					// upload image
					if($request->hasFile('image'))
			    	$local['image'] = $this->imageUpload($request->file('image'),$local->uuid,'LOCAL','locals');

					$local = $this->getItem($local,'App\Http\Transformers\LocalTransformer');

					$local['cols'] = json_encode($this->tableCols($local,$this->comercioMenus($comercio->id)));
					return $this->ajaxData($local);
		    }catch(Exception $e){
		    	return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
		    }
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * reduce los datos del menú
     * @return array
     */
    private function formatMenu(){
    	return [
    		['delivery' => ''],
    		['takein'		=> ''],
    		['takeout'	=> '']
    	];
    }

    /**
     * formatea la información de la dirección de un Local
     * @param  array $info
     * @param  array $address
     * @return array
     */
    private function formatAddress($info,$address){
    	return [
				'address'	=>	$address['address'],
				'name'		=>	$info['name'],
				'point'		=>	[
						'lat'	=>	$address['lat'],
						'lng'=>		$address['lng']
						]
			];
    }

    /**
     * formatea la información de contacto del Local
     * @param  array $contacts
     * @return array
     */
    private function formatContact($contacts){
    	$list = [];
    	foreach ($contacts as $key => $contact) {
    		$list[] = [
					'name'	=>	$key,
					'phone'	=>	[$contact['phone']],
					'email'	=>	[$contact['email']]
				];
    	}
    	return $list;
    }

    /**
     * formatea la información de los horarios de atención del Local
     * @param  array $timetable
     * @return array
     */
    private function formatTimetable($timetable){
    	$days=['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
    	$list = [];
    	for($day=0;$day<7;$day++){
    		$list[] = [
    				$timetable[$day]['day'] => $days[$day],
    				'open'	=> $timetable[$day]['open'],
    				'close'	=> $timetable[$day]['close']
    		];
    	}
    	return $list;
    }

    /**
     * formatea la información de las zonas de cobertura de un Local
     * @param  array $zones
     * @return array
     */
    private function formatZone($zones){
    	$list = [];
    	foreach ($zones as $id => $zone) {
    		$list[] = [
							'name'	=>	$zone['zone'],
							'point'	=>	explode(',',$zone['points']),
							];
    	}
    	return $list;
    }

    /**
     * formatea la información de un Local para mostrarlo en la lista
     * @param  Local $local
     * @param  array $menus
     * @return array
     */
    private function tableCols($local,$menus){
    	$cols =[];
    	$cols['name'] 		= view('app.locals.item.name',	 ['local'	  => $local])->render();
		  $cols['address'] 	= view('app.locals.item.address',['address' => $local['address']])->render();
		  $cols['contact'] 	= view('app.locals.item.contact',['contact' => $local['contact']])->render();
		  $cols['menu'] 		= view('app.locals.item.menu',	 ['menu'		=> $local['menu'],'menus'=>$menus, 'localid' => $local['id']])->render();
		  $cols['actions'] 	= view('app.locals.item.actions',['id'		  => $local['id'], 'delivery' => $local['delivery']])->render();
		  $cols['id']				= $local['id'];
    	return $cols;
    }

    /**
     * elimina un Local
     * @param  Request $request
     * @return json
     */
    public function delete(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('delete-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($id = $request->id){
    			if($this->userhasRole('admin')){
    				$local = Local::where('uuid',$id)->first();
    			}else{
    				$local = Local::where('uuid',$id)->where('comercio_id',$this->userComercio())->first();
    			}
    			if($local){
    				$local->name .= sprintf("[deleted %s]",date('Y-m-j H:i:s'));
    				$local->save();
						if($local->delete())
	    				return $this->ajaxData([],'ok','Local eliminado');
	    			else
	    				return $this->ajaxError('No se pudo eliminar el local');
    			}else{
    				return $this->ajaxError('No se encontró el local');
    			}
	   		}else{
    			return $this->ajaxError('Se necesita el identificador del local');
    		}
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

  /**
   * activa / deactiva la funcionalidad del servicio de delivery de un Local
   * @param  Request $request
   * @return json
   */
  public function delivery(Request $request){
  	if($request->ajax()){
  		if(!$this->userhasPermission('activate-local') && !$this->userhasPermission('deactivate-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
  		if($id = $request->id){
  			if($this->userhasRole('admin')){
  				$local = Local::where('uuid',$id)->first();
  			}else{
  				$local = Local::where('uuid',$id)->where('comercio_id',$this->userComercio())->first();
  			}
  			if($local){
  				$local->delivery = !$local->delivery;
  				try{
  					$local->save();
  					$menus = $this->comercioMenus($local->comercio_id);
  					$local = $this->getItem($local,'App\Http\Transformers\LocalTransformer');
						$local['cols'] = json_encode($this->tableCols($local, $menus));
						return $this->ajaxData($local);
  				}catch(Exception $e){
  					return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
  				}
  			}else{
  				return $this->ajaxError('No se encontró el local');
  			}
   		}else{
  			return $this->ajaxError('Se necesita el identificador del local');
  		}
  	}else{
   		return $this->ajaxError('Acción no permitida');
   	}
  }

  /**
   * asigna un menú para el Local
   * @param  Request $request
   * @return json
   */
  public function menu(Request $request){
  	if($request->ajax()){
  		if(!$this->userhasPermission('menu-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
  		if($this->userhasRole('admin')){
  			$local = Local::where('uuid',$request->local)->first();
  		}else{
  			$local = Local::where('uuid',$request->local)->where('comercio_id',$this->userComercio())->first();
  		}
  		if(!$local)
  			return $this->ajaxError('No se encontró el local');
  		if(!$menu  = Menu::where('uuid',$request->menu)->first())
  			return $this->ajaxError('No se encontró el menú');
  		$type = strtolower($request->type);
  		$list = [];
  		foreach ($local->menu as $menulocal) {
  			foreach ($menulocal as $menutype => $value) {
  				if($type == strtolower($menutype)){
  					$menulocal[$menutype] = $menu->uuid;
  				}
  				$list[] = $menulocal;
  			}
  		}
  		$local->menu = $list;
  		$local->save();
  		$menus = $this->comercioMenus($local->comercio_id);
  		$local = Local::find($local->id);
  		$local = $this->getItem($local,'App\Http\Transformers\LocalTransformer');
			$local['cols'] = json_encode($this->tableCols($local,$menus));
			return $this->ajaxData($local);
  	}else{
   		return $this->ajaxError('Acción no permitida');
   	}
  }

  /**
   * elimina un Menú de un Local
   * @param  Request $request
   * @return json
   */
  public function menudelete(Request $request){
  	if($request->ajax()){
  		if(!$this->userhasPermission('nomenu-local'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    	if($this->userhasRole('admin')){
    		$local = Local::where('uuid',$request->local)->first();
    	}else{
    		$local = Local::where('uuid',$request->local)->where('comercio_id',$this->userComercio())->first();
    	}
  		if(!$local)
  			return $this->ajaxError('No se encontró el local');
  		$type = strtolower($request->type);
  		$list = [];
  		foreach ($local->menu as $menulocal) {
  			foreach ($menulocal as $menutype => $value) {
  				if($type == strtolower($menutype)){
  					$menulocal[$menutype] = '';
  				}
  				$list[] = $menulocal;
  			}
  		}
  		$local->menu = $list;
  		$local->save();
  		$menus = $this->comercioMenus($local->comercio_id);
  		$local = Local::find($local->id);
  		$local = $this->getItem($local,'App\Http\Transformers\LocalTransformer');
			$local['cols'] = json_encode($this->tableCols($local,$menus));
			return $this->ajaxData($local);
  	}else{
   		return $this->ajaxError('Acción no permitida');
   	}
  }

  /**
   * retorna la lista de Menús de un Comercio
   * @param  integer $id
   * @return array
   */
  private function comercioMenus($id){
  	if(!$comercio = Comercio::where('id',$id)->first())
  		return [];
  	$menus = $this->getCollection($comercio->menus->toArray(),'App\Menu','App\Http\Transformers\MenucomercioTransformer');
    $menus = collect($menus)->groupBy('type')->toArray();
    return $menus;
  }
}
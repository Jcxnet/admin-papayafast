<span class="hide">{{$role['name']}}</span>
<div class="products-list">
	<div class="product-img">
    <img src="{{$role['avatar']}}" alt="" class="img-circle" />
  </div>
	<div class="product-info">
    <a href="#" class="product-title" data-title="{{$role['displayname']}}">{{$role['displayname']}}</a>
     <span class="product-description" data-description="{{$role['description']}}">
       {{$role['description']}}
     </span>
  </div>
</div>
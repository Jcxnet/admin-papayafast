<div class="box box-primary box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-info-circle" aria-hidden="true"></i>
&nbsp;{{$menu['title']}}</h3>
    </div>
    <div class="box-body">

    	<div class="row">
    		<div class="col-sm-12">
    			<div class="info-box bg-teal-active">
			      <span class="info-box-icon"><i class="fa fa-info"></i></span>
			      <div class="info-box-content">

			      	<div class="row no-margin">
                <div class="col-sm-3 border-right">
                  <div class="description-block">
                    <span class="description-text">Menú</span>
                    <h5 class="description-header">{{$menu['title']}}</h5>
                  </div><!-- /.description-block -->
                </div><!-- /.col -->
                <div class="col-sm-3 border-right">
                  <div class="description-block">
                    <span class="description-text">Comercio</span>
                    <h5 class="description-header">{{$menu['comercio']}}</h5>
                  </div><!-- /.description-block -->
                </div><!-- /.col -->
                <div class="col-sm-3 border-right">
                  <div class="description-block">
                    <span class="description-text">Tipo</span>
                    <h5 class="description-header">{{$menu['type']}}</h5>
                  </div><!-- /.description-block -->
                </div><!-- /.col -->
                <div class="col-sm-3">
                  <div class="description-block">
                    <span class="description-text">Versión</span>
                    <h5 class="description-header">{{$menu['version']}}</h5>
                  </div><!-- /.description-block -->
                </div><!-- /.col -->
              </div>
			        <div class="progress">
			          <div class="progress-bar" style="width: 100%"></div>
			        </div>
			        <span class="progress-description">
			          Creado: {{$menu['created']}} - Última actualización: {{$menu['updated']}}
			        </span>
			      </div>
			    </div>
    		</div>
    	</div>

      <div class="row">
      	<div class="col-sm-3">
		  		@include('app.editor.item.resume',['title'=>'Página principal','text'=>$menu['pagetitle'],'bgclass'=>'bg-orange','icon'=>'fa-dot-circle-o'])
		  	</div>
	  		<div class="col-sm-3">
		  		@include('app.editor.item.resume',['title'=>'Páginas','text'=>$menu['total']['page'],'bgclass'=>'bg-blue','icon'=>'fa-file-text-o'])
		  	</div>
		  	<div class="col-sm-3">
		  		@include('app.editor.item.resume',['title'=>'Botones','text'=>$menu['total']['button'],'bgclass'=>'bg-aqua','icon'=>'fa-external-link-square'])
		  	</div>
		  	<div class="col-sm-3">
		  		@include('app.editor.item.resume',['title'=>'Productos','text'=>$menu['total']['product'],'bgclass'=>'bg-green','icon'=>'fa-tasks'])
		  	</div>
	  	</div>

    </div>
  </div>


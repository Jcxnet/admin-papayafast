<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return Redirect::to('dashboard');
});
*/
/*Route::group(['middleware' => 'web'], function () {

});*/


	Route::auth();
  Route::get('/',['as'=>'users.dashboard','uses'=>'DashboardController@index']);
  Route::get('dashboard',['as'=>'users.dashboard','uses'=>'DashboardController@index']);

  Route::group(['prefix'=>'roles'],function(){
  	Route::get 	('/',				['as' => 	'roles.all', 		'uses' =>	'RolesController@index' , 'middleware' => ['role:admin','permission:list-rol']	]);
  	Route::post ('add',			['as'	=>	'roles.add',		'uses' =>	'RolesController@add'		, 'middleware' => ['role:admin','permission:add-rol']	]);
  	Route::post ('edit',		['as'	=>	'roles.edit',		'uses' =>	'RolesController@add'		, 'middleware' => ['role:admin','permission:edit-rol']	]);
  	Route::post ('delete',	['as'	=>	'roles.delete',	'uses' =>	'RolesController@delete', 'middleware' => ['role:admin','permission:delete-rol']	]);
  });

  Route::group(['prefix'=>'permissions'],function(){
  	Route::get 	('/',				['as' => 	'permissions.all', 		'uses' =>	'PermissionsController@index', 	 'middleware' => ['role:admin','permission:list-permission'] ]);
  	Route::post ('restore',	['as' => 	'permissions.restore','uses' =>	'PermissionsController@restore', 'middleware' => ['role:admin','permission:restore-permission'] ]);
  	Route::post ('allow',		['as' => 	'permissions.allow',	'uses' =>	'PermissionsController@allow', 	 'middleware' => ['role:admin','permission:allow-permission'] ]);
  	Route::post ('block',		['as' => 	'permissions.block',	'uses' =>	'PermissionsController@block', 	 'middleware' => ['role:admin','permission:block-permission'] ]);
  });

	Route::group(['prefix'=>'roles/users'],function(){
  	Route::get 	('/',				['as' => 	'rolesusers.all',			'uses' =>	'RoleusersController@index', 'middleware' => ['permission:list-user'] ]);
  	Route::post ('add',			['as'	=>	'rolesusers.add',			'uses' =>	'RoleusersController@add',   'middleware' => ['permission:add-user'] ]);
  	Route::post ('edit',		['as'	=>	'rolesusers.edit',		'uses' =>	'RoleusersController@add',   'middleware' => ['permission:edit-user'] ]);
  	Route::post ('delete',	['as'	=>	'rolesusers.delete',	'uses' =>	'RoleusersController@delete', 'middleware' => ['permission:delete-user'] ]);
  	Route::post ('assign',	['as'	=>	'rolesusers.assign',	'uses' =>	'RoleusersController@assign', 'middleware' => ['permission:addrol-user'] ]);
  	Route::post ('remove',	['as'	=>	'rolesusers.remove',	'uses' =>	'RoleusersController@remove', 'middleware' => ['permission:deleterol-user'] ]);
  });

  Route::group(['prefix'=>'comercios'],function(){
  	Route::get 	('/',				['as' => 	'comercios.all', 		'uses' =>	'ComerciosController@index',  'middleware' => ['permission:list-comercio']  ]);
  	Route::post ('add',			['as'	=>	'comercios.add',		'uses' =>	'ComerciosController@add'  ,  'middleware' => ['permission:add-comercio']   ]);
  	Route::post ('edit',		['as'	=>	'comercios.edit',		'uses' =>	'ComerciosController@add'  ,  'middleware' => ['permission:edit-comercio']   ]);
  	Route::post ('delete',	['as'	=>	'comercios.delete',	'uses' =>	'ComerciosController@delete', 'middleware' => ['permission:delete-comercio'] ]);
  	Route::post ('locals',	['as'	=>	'comercios.locals',	'uses' =>	'ComerciosController@locals', 'middleware' => ['permission:locals-comercio'] ]);
  });

  Route::group(['prefix'=>'apikeys'],function(){
  	Route::get 	('/',				['as' => 	'apikeys.all', 			'uses' =>	'ApikeysController@index', 	 'middleware' => ['role:admin','permission:list-apikey'] ]);
  	Route::post	('values',	['as' => 	'apikeys.values', 	'uses' =>	'ApikeysController@values',	 'middleware' => ['role:admin','permission:generate-apikey'] ]);
  	Route::post	('suspend',	['as' => 	'apikeys.suspend', 	'uses' =>	'ApikeysController@suspend', 'middleware' => ['role:admin','permission:deactivate-apikey'] ]);
  	Route::post	('activate',['as' => 	'apikeys.activate',	'uses' =>	'ApikeysController@activate','middleware' => ['role:admin','permission:activate-apikey'] ]);
  });

  Route::group(['prefix'=>'locals'],function(){
  	Route::get 	('/',				 		['as' =>	'locals.all', 				'uses' =>	'LocalsController@index',  'middleware' => ['permission:list-local'] ]);
  	Route::post	('locals', 	 		['as' =>	'locals.format', 			'uses' =>	'LocalsController@locals', 'middleware' => ['permission:format-local'] ]);
  	Route::post	('info', 		 		['as' =>	'locals.info', 				'uses' =>	'LocalsController@info',   'middleware' => ['permission:info-local'] ]);
  	Route::post ('add',			 		['as' =>	'locals.add',					'uses' =>	'LocalsController@add', 	 'middleware' => ['permission:add-local'] ]);
  	Route::post ('edit',		 		['as' =>	'locals.edit',				'uses' =>	'LocalsController@add', 	 'middleware' => ['permission:edit-local'] ]);
  	Route::post ('delete',	 		['as' =>	'locals.delete',			'uses' =>	'LocalsController@delete', 'middleware' => ['permission:delete-local'] ]	);
  	Route::post ('delivery', 		['as' =>	'locals.delivery',		'uses' =>	'LocalsController@delivery','middleware' => ['permission:deactivate-local','permission:activate-local']  ]);
  	Route::post ('menu', 		 		['as' =>	'locals.menu',				'uses' =>	'LocalsController@menu', 		'middleware' => ['permission:menu-local'] ]);
  	Route::post ('menu/delete', ['as' =>	'locals.menu.delete',	'uses' =>	'LocalsController@menudelete', 'middleware' => ['permission:nomenu-local'] ]);
  });

  Route::group(['prefix'=>'menus'],function(){
  	Route::get 	('/',								['as' => 	'menus.all', 							'uses' =>	'MenusController@index', 'middleware' => ['permission:list-menu'] ]);
  	Route::post	('add',							['as' => 	'menus.add', 							'uses' =>	'MenusController@add',  'middleware' => ['permission:add-menu' ] ]);
  	Route::post	('edit',						['as' => 	'menus.edit', 						'uses' =>	'MenusController@add',  'middleware' => ['permission:edit-menu'] ]);
  	Route::post	('delete',					['as' => 	'menus.delete', 					'uses' =>	'MenusController@delete',  'middleware' => ['permission:delete-menu'] ]);
  	Route::post	('duplicate',				['as' => 	'menus.duplicate',				'uses' =>	'MenusController@duplicate', 'middleware' => ['permission:clone-menu'] ]);
  	Route::post	('preview',					['as' => 	'menus.preview',					'uses' =>	'MenusController@preview', 'middleware' => ['permission:preview-menu'] ]);
  	Route::post	('page/preview',		['as' => 	'menus.preview.page',			'uses' =>	'MenusController@previewPage', 'middleware' => ['permission:preview-menu'] ]);
  	Route::post	('activate',				['as' => 	'menus.activate',					'uses' =>	'MenusController@activate', 'middleware' => ['permission:activate-menu','permission:deactivate-menu'] ]);
  	Route::post	('comercio',				['as' => 	'menus.comercio', 				'uses' =>	'MenusController@comercio']);
  	Route::post	('local/add',				['as' => 	'menus.local.add',				'uses' =>	'MenusController@addLocal', 'middleware' => ['permission:addlocal-menu'] ]);
  	Route::post	('local/delete',		['as' => 	'menus.local.delete',			'uses' =>	'MenusController@deleteLocal', 'middleware' => ['permission:deletelocal-menu'] ]);
  	Route::post	('local/delete/all',['as' => 	'menus.local.delete.all',	'uses' =>	'MenusController@deleteAllLocals', 'middleware' => ['permission:deletelocal-menu'] ]);

  });

  Route::group(['prefix'=>'editor'],function(){
  	Route::get 	('/{id}',					['as' => 	'editor.main',					'uses' =>	'EditorController@index', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('resume',				['as' => 	'editor.menu.resume',		'uses' =>	'EditorController@resume', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('add/form',			['as' => 	'editor.add.form',			'uses' =>	'EditorController@addForm', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('page/add',			['as' => 	'editor.page.add',			'uses' =>	'EditorController@addPage', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('menu/save',			['as' => 	'editor.menu.save',			'uses' =>	'EditorController@saveMenu', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('sidebar/create',['as' => 	'editor.sidebar.create','uses' =>	'EditorController@createSidebar', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('items/order',		['as' => 	'editor.items.order',		'uses' =>	'EditorController@orderList', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('item/html',			['as' => 	'editor.items.html',		'uses' =>	'EditorController@itemHtml', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('button/add',		['as' => 	'editor.button.add',		'uses' =>	'EditorController@addButton', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('product/add',		['as' => 	'editor.product.add',		'uses' =>	'EditorController@addProduct', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('property/add',	['as' => 	'editor.property.add',	'uses' =>	'EditorController@addProperty', 'middleware' => ['permission:editor-menu'] ]);
  	Route::post	('group/add',			['as' => 	'editor.group.add',			'uses' =>	'EditorController@addGroup', 'middleware' => ['permission:editor-menu'] ]);
  });

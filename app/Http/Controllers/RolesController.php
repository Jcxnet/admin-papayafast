<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Role;
use Validator;


class RolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show all roles in database
     * @return View
     */
    public function index(){
    	if(!$this->userhasRole('admin') || !$this->userhasPermission('list-rol'))
    		return $this->ajaxError('No tiene permiso para realizar esta acción');
      $roles = Role::get();
      $list = [];
      foreach($roles as $role){
      	$permissions = $this->getCollection($role->perms()->get()->toArray(),'App\Permission','App\Http\Transformers\PermissionTransformer');
      	$permissions = collect($permissions)->sortBy('displayname')->groupBy('action')->toArray();
      	$users = $role->users()->get();
      	$role = $this->getItem($role,'App\Http\Transformers\RoleTransformer');
      	/*$avatar = $this->imageType($role['id'],'ROLE','roles');
      	if($avatar['exists'] == true)
      		$role['avatar'] = $avatar['data']['images']['conversions']['img200x200']['url'];*/
      	$list[] = ['role'=>$role, 'permissions'=>$permissions, 'users' => $users->toArray()];
      }
      return view('app.roles.index',['roles'=>$list]);
    }

    /**
     * agrega / edita un Rol
     * @param Request $request
     * @return json
     */
    public function add(Request $request){
    	if($request->ajax()){

    		if(!$this->userhasRole('admin'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$this->userhasPermission('add-rol') && !$this->userhasPermission('edit-rol'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');

    		$data = $request->only(['display_name','description']);
    		$id = isset($request->id)?$request->id:false;
    		if($id){
	    		if(!$id = Role::select('id')->where('uuid',$id)->first())
	    			return $this->ajaxError('No se pudo encontrar el rol');
	    		$id = $id->id;
	    	}
    		$data['name'] = str_slug($data['display_name']);
    		if($id){
    			$validator = Validator::make($data, [
			      'display_name'	=> 'required|string|min:4|max:200|unique:roles,display_name,'.$id,
			      'description' 	=> 'required|string|min:4|max:200',
			      'name'					=> 'required|string|min:4|max:200|unique:roles,name,'.$id,
			    ]);
    		}else{
    			$validator = Validator::make($data, [
			      'display_name'	=> 'required|string|min:4|max:200|unique:roles,display_name,NULL,id,deleted_at,NULL',
			      'description' 	=> 'required|string|min:4|max:200',
			      'name'					=> 'required|string|min:4|max:200|unique:roles,name,NULL,id,deleted_at,NULL',
			    ]);
    		}
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    try{
		    	if($id)
		    		$role = Role::find($id);
		    	else
		    		$role = new Role();
		    	$role->name         = $data['name'];
					$role->display_name = $data['display_name'];
					$role->description  = $data['description'];
					$role->save();

					if(!$id)
						$role = Role::find($role->id);

					$permissions = $this->getCollection($role->perms()->get()->toArray(),'App\Permission','App\Http\Transformers\PermissionTransformer');
      		$permissions = collect($permissions)->sortBy('displayname')->groupBy('action')->toArray();
      		$users = $role->users()->get();

			    $role = $this->getItem($role,'App\Http\Transformers\RoleTransformer');

			    if($request->hasFile('image'))
			    	$role['image'] = $this->imageUpload($request->file('image'),$role['id'],'ROLE','roles');
			    else
			    	$role['image'] = $this->imageType($role['id'],'ROLE','roles');

	      	if($role['image']['exists'] == true)
	      		$role['avatar'] = $role['image']['data']['images']['conversions']['img200x200']['url'];

			    $role['cols'] = $this->tableCols($role,$permissions,$users);
					return $this->ajaxData($role);
		    }catch(Exception $e){
		    	return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
		    }
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * formatea la información de un Rol
     * @param  array $role
     * @return array
     */
    private function tableCols($role,$permissions,$users){
    	$cols =[];
    	$cols['role'] 				= view('app.roles.item.role',['role'=>$role])->render();
		  $cols['permissions'] 	= view('app.roles.item.permissions',['permissions'=>$permissions])->render();
		  $cols['users'] 				= view('app.roles.item.users',['users'=>$users])->render();
		  $cols['actions'] 			= view('app.roles.item.actions',['id'=>$role['id']])->render();
    	return json_encode($cols);
    }

    /**
     * elimina un Rol
     * @param  Request $request
     * @return json
     */
    public function delete(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasRole('admin') || !$this->userhasPermission('delete-rol'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($id = $request->id){
    			if($role = Role::where('uuid',$id)->first()){
    				$role->name .= sprintf("[deleted %s]",date('Y-m-j H:i:s'));
    				$role->save();
						if($role->delete())
	    				return $this->ajaxData([],'ok','Rol eliminado');
	    			else
	    				return $this->ajaxError('No se pudo eliminar el rol');
    			}else{
    				return $this->ajaxError('No se encontró el rol');
    			}
	   		}else{
    			return $this->ajaxError('Se necesita el identificador del rol');
    		}
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

}
@foreach($contact as $item)
<span class="text-left clearfix">
	<span class="text-left">{{$item['name']}}</span><br/>
	@if(is_array($item['phone']) AND count($item['phone'])>0)
	<i class="fa fa-phone margin-r-5"></i><span class="data-{{$item['name']}}-phone" data-{{$item['name']}}-phone="{{$item['phone'][0]}}">
		{{$item['phone'][0]}}
	</span>
	@endif
	@if(is_array($item['email']) AND count($item['email'])>0)
	<i class="fa fa-at margin-r-5"></i><span class="data-{{$item['name']}}-email" data-{{$item['name']}}-email="{{$item['email'][0]}}">
		{{$item['email'][0]}}
	</span>
	@endif
</span>
@endforeach
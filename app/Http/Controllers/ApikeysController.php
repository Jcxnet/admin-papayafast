<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Apikey;
use App\Comercio;
use Validator;


class ApikeysController extends Controller
{
    private $orders = [
    	'InforestPardos'=>[
    				'detalle'	=> 'Pedidos para Inforest Pardos',
    				'version' => '1.0.0'
    	],
    	'OtroErp'=>[
    				'detalle' => 'Pedidos para otro ERP',
    				'version'	=> '1.0.0'
    	]
    ];

    private $payments = [
    	'Creditcards' => [
    				'detalle' => 'Pasarela de pagos Papaya',
    				'version' => '1.5.0'
    	]
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show all comercios in database
     * @return View
     */
    public function index(){
    	if(!$this->userhasRole('admin') || !$this->userhasPermission('list-apikey'))
    		return $this->ajaxError('No tiene permiso para realizar esta acción');
      $comercios = Comercio::orderBy('name')->get();
     	$comercios = $this->getCollection($comercios->toArray(),'App\Comercio','App\Http\Transformers\ComercioApikeyTransformer');
      return view('app.apikeys.index',['comercios'=>$comercios,'orderClass'=>$this->orders, 'paymentClass'=>$this->payments]);
    }

    /**
     * genera/actualiza una API key para un Comercio
     * @param  Request $request
     * @return json
     */
    public function values(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasRole('admin') || !$this->userhasPermission('generate-apikey'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$data = $request->only(['id','orders','payments','apikey']);
    		if(!$id = Comercio::where('uuid',$data['id'])->first())
    			return $this->ajaxError('No se pudo encontrar el comercio');
    		$name = $id->name; $id = $id->id;

  			$validator = Validator::make($data, [
		      'orders'		=> 'required|string',
		      'payments'	=> 'required|string',
		      'apikey' 		=> 'required|string|size:32',
		    ]);

		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    try{
	    		if(!$apikey = Apikey::where('comercio_id',$id)->first())
	    			$apikey = new Apikey();
		    	$apikey->comercio_id  = $id;
					$apikey->name 				= $name;
					$apikey->version  		= '1.0.0';
					$apikey->token 				= $data['apikey'];
					$apikey->processors   = ['orders'=>$data['orders'],'payments'=>$data['payments']];
					$apikey->unguard();
					$apikey->save();
					$apikey->reguard();
					$comercio = Comercio::find($id);
					$comercio = $this->getItem($comercio,'App\Http\Transformers\ComercioApikeyTransformer');
					$comercio['cols'] = $this->tableCols($comercio);
					return $this->ajaxData($comercio);
		    }catch(Exception $e){
		    	return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
		    }
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * formatea la información de una API key
     * @param  Comercio $comercio
     * @return json
     */
    private function tableCols($comercio){
    	$cols =[];
    	$cols['name'] 		= view('app.apikeys.item.name',['name'=>$comercio['name'],'version'=>$comercio['apikey']['version'],'logo'=>$comercio['logo']])->render();
		  $cols['clases'] 	= view('app.apikeys.item.clases',['apikey'=>$comercio['apikey']])->render();
		  $cols['apikey'] 	= view('app.apikeys.item.apikey',['apikey'=>$comercio['apikey']])->render();
		  $cols['actions'] 	= view('app.apikeys.item.actions',['id'=>$comercio['id'],'version'=>$comercio['apikey']['version'],'suspended'=>$comercio['apikey']['suspended']])->render();
    	return json_encode($cols);
    }

    /**
     * suspende la validez de una API key
     * @param  Request $request
     * @return json
     */
    public function suspend(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasRole('admin') || !$this->userhasPermission('deactivate-apikey'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$id = $request->id)
    			return $this->ajaxError('Se necesita el identificador del comercio');
    		if(!$comercio = Comercio::where('uuid',$id)->first())
    			return $this->ajaxError('No se encontró el comercio');
    		$comercio->apikey->suspended = true;
    		$comercio->apikey->save();
    		return $this->ajaxData([],'ok','API key inactiva');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * activa una API key suspendida
     * @param  Request $request
     * @return json
     */
    public function activate(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasRole('admin') || !$this->userhasPermission('activate-apikey'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$id = $request->id)
    			return $this->ajaxError('Se necesita el identificador del comercio');
    		if(!$comercio = Comercio::where('uuid',$id)->first())
    			return $this->ajaxError('No se encontró el comercio');
    		$comercio->apikey->suspended = false;
    		$comercio->apikey->save();
    		return $this->ajaxData([],'ok','API key activa');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }
}
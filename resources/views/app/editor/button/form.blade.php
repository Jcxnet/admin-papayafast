<div class="box box-primary box-solid">
  	<div class="box-header with-border bg-aqua">
      <h3 class="box-title"><i class="fa fa-external-link-square" aria-hidden="true"></i>&nbsp;Botón</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool remove-form" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>

		<div class="box-body">
  		<form class="form">
  			<input type="hidden" id="txtid" name="txtid" />
        <div class="form-group">
          <label for="txttitle" class="control-label">Título</label>
          <input type="text" class="form-control" id="txttitle" placeholder="Título">
        </div>
        <div class="form-group">
        	<div class="row">
          	<div class="col-xs-4">
          		<label class="control-label">Estilo de botón</label>
          	</div>
          	<div class="col-xs-4">
          		<div class="radio">
		            <label>
		              <input type="radio" name="optbuttontype" id="optbuttontype1" value="square" checked>
		              Rectangular
		            </label>
		          </div>
          	</div>
          	<div class="col-xs-4">
          		<div class="radio">
		            <label>
		              <input type="radio" name="optbuttontype" id="optbuttontype2" value="circle">
		              Circular
		            </label>
		          </div>
          	</div>
          </div>
        </div>
        <div class="form-group">
          @include ('app.editor.button.container',['items'=>[]])
        </div>
      </form>
    </div>

    <div class="box-footer">
      <button class="btn btn-default remove-form">Cancelar</button>
      <button class="btn btn-primary pull-right" id="btnButtonSubmit">Guardar cambios</button>
    </div><!-- /.box-footer -->


</div>

@include ('app.editor.button.functions')
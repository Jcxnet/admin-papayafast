<script>

  $(function (){

    initTable();
    actionSelect();
    actionButtons();
    formButtons();
    tableEvents();

  });

  function actionSelect(){
  	$('#btnAddLocal').hide();
  	$('#selectComercio').on('change',function(){
  		var id = $('#selectComercio').val();
  		$('#txtcomercioid').val(id);
  		getLocals(id);
  	});
  	$('#local_tabs_main a').on('shown.bs.tab', function(e){
		    var target = $(e.target).attr('data-info') // activated tab
  			if(target == 'tab2'){
  				refreshMap(map);
  			}
  			if(target == 'tab5'){
  				refreshMap(mapzone);
  			}

		});
  }

  function getLocals(id){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('comercios.locals') }}",
      data: {id:id},
      beforeSend:function(){
        showAlert('Lista de locales','Enviando y obteniendo datos...','info');
        $('#btnAddLocal').hide();
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: formatLocals(data.data);
          							break;
          case 'empty': showMessage('Aviso',data.meta.message,'warning');
          							$('#btnAddLocal').show();
          							clearTable();
          							break;
          case 'error': showMessage('Error','Error al intentar obtener la lista de locales:<br><b>'+data.data.message+'</b>','danger');
          							clearTable();
          							console.log('why errors!!');
          							break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar obtener la lista de locales, intente nuevamente','warning');
      	clearTable();
      }
    });
  }

  function formatLocals(data){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('locals.format') }}",
      data: {locals: JSON.stringify(data.locals), menus: JSON.stringify(data.menus)},
      beforeSend:function(){
        showAlert('Lista de locales','preparando datos...','info');
        $('#btnAddLocal').hide();
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showToast('Lista de locales','Información obtenida','success');
          							showLocals(data.data);
          							actionButtons();
          							$('#btnAddLocal').show();
          							break;
          case 'error': showMessage('Error','Error al intentar obtener la lista de locales:<br><b>'+data.data.message+'</b>','danger');
          							clearTable();
          							break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar obtener la lista de locales, intente nuevamente','warning');
      	clearTable();
      }
    });
  }

  function showLocals(data){
  	$('#dataTable').DataTable().destroy();
  	$('#dataTable').DataTable( {
        "processing": true,
        "data": data,
        "paging": true,
	      "lengthChange": false,
	      "searching": false,
	      "ordering": true,
	      "autoWidth": true,
	      "responsive": {
	        "details": false
	    	},
        "columns": [
            { "data": "name", 	 "orderable": true  },
            { "data": "address", "orderable": false },
            { "data": "contact", "orderable": false },
            { "data": "menu", 	 "orderable": false },
            { "data": "actions", "orderable": false },
        ],
        rowCallback: function ( row, data, index ) {
        	$(row).attr('rowid',data.id);
        }
    } );
  }


  function formButtons(){

  	$('#btnGeolocation').on('click',function(e){
  		e.preventDefault();
  		geoLocation(map,marker,'#txtaddress');
  		refreshMap(map);
  		return false;
  	});

  	$('#btnSearchAddress').on('click',function(e){
  		e.preventDefault();
  		searchAddress($('#txtaddress').val(),map,marker,'#txtaddress');
  		refreshMap(map);
  		return false;
  	});

  	$('#btnAddLocal').on('click',function(e){
    	e.preventDefault();
    	clearForm();
    	$('#lblcomercio').html($("#selectComercio option:selected").text());
    	$('#btnAddThisLocal').html('Agregar Local');
    	$('#frmAddLocal').find('.modal-title').html('Agregar Local');
    	$('#frmAddLocal').removeClass('edit-local').addClass('new-local');
    	$('#frmAddLocal').modal({backdrop:'static',show: true});
    	$('#local_tabs_main a:first').tab('show');
    	timetableInputs();
    	showAddressMap();
    	showZoneMap();
    	return false;
    });

    $('#btnAddThisLocal').on('click',function(e){
    	e.preventDefault();
    	if( $('#frmAddLocal').hasClass('new-local') )
    		addNewLocal();
    	if( $('#frmAddLocal').hasClass('edit-local') )
    		editLocal();
    	return false;
    });
  }

  function initTable(){
  	$('#dataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "autoWidth": true,
      "responsive": {
        "details": false
    	},
      "columns": [
			    { "orderable": true  },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false }
			  ]
    });
  }

  function clearTable(){
  	var table = $('#dataTable').DataTable();
  	table.clear().draw();
  }


  function addNewLocal(){
  	var formData = new FormData();
  	formData.append('info', JSON.stringify(dataInfoLocal()));
  	formData.append('address', JSON.stringify(dataAddressLocal()));
  	formData.append('contact', JSON.stringify(dataContactLocal()));
  	formData.append('timetable', JSON.stringify(dataTimetableLocal()));
  	formData.append('zones', JSON.stringify(dataZonesLocal()));

  	var file = document.getElementById("image").files[0]; //fetch file
    formData.append('image', file); //append file to formData object

  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('locals.add') }}",
      data: formData, //{ info: dataInfoLocal(), address: dataAddressLocal(), contact: dataContactLocal(), timetable: dataTimetableLocal(), zones: dataZonesLocal() },
      cache: false,
      contentType: false,
			processData: false,
      beforeSend:function(){
        showAlert('Agregar local','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showToast('Nuevo','Local creado','success');
          							$('#frmAddLocal').modal('hide');
          							showLocal(data.data.cols,data.data.id);
          							actionButtons();
          							clearForm();
          							break;
          case 'error': showMessage('Error','Error al crear el local:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar agregar el local, intente nuevamente','warning');
      }
    });
  }

  function editLocal(){
  	var formData = new FormData();
  	formData.append('info', JSON.stringify(dataInfoLocal()));
  	formData.append('address', JSON.stringify(dataAddressLocal()));
  	formData.append('contact', JSON.stringify(dataContactLocal()));
  	formData.append('timetable', JSON.stringify(dataTimetableLocal()));
  	formData.append('zones', JSON.stringify(dataZonesLocal()));

  	var file = document.getElementById("image").files[0]; //fetch file
    formData.append('image', file); //append file to formData object

  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('locals.edit') }}",
      data: formData,// { info: dataInfoLocal(), address: dataAddressLocal(), contact: dataContactLocal(), timetable: dataTimetableLocal(), zones: dataZonesLocal() },
      cache: false,
      contentType: false,
			processData: false,
      beforeSend:function(){
        showAlert('Editar local','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showToast('Editar','Local editado','success');
          							$('#frmAddLocal').modal('hide');
          							clearForm();
          							updateLocal(data.data);
          							break;
          case 'error': showMessage('Error','Error al editar el local:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar editar el local, intente nuevamente','warning');
      }
    });
  }

  function showLocal(cols,id){
  	cols = JSON.parse(cols);
  	var table = $('#dataTable').DataTable();
		var node  = table.row.add( {name: cols.name, address: cols.address, contact: cols.contact, menu: cols.menu, actions: cols.actions} ).node();
		table.column(0).data().sort().draw();
		$(node).attr('rowid',id);
  }

  function updateLocal(data){
  	var table = $('#dataTable').DataTable();
  	var row = $("tr[rowid='"+data.id+"']");
  	row.addClass('update');
  	table.row('.update').remove().draw( false );
  	showLocal(data.cols,data.id);
  }

  function actionButtons(){
  	$('.btnDelete, .btnEdit, .btnDelivery, .menu-local-selection, .menu-local-selected, .menu-local-delete').unbind();
  	$('.btnDelete').on('click',function(e){
  		e.preventDefault();
  		deleteLocal($(this).attr('itemid'));
  		return false;
  	});
  	$('.btnEdit').on('click',function(e){
  		e.preventDefault();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		$('#frmAddLocal').find('.modal-title').html('Editar Local');
  		$('#btnAddThisLocal').html('Editar Local');
  		$('#txtid').val(id);
  		getLocalInfo(id);
    	return false;
  	});
  	$('.btnDelivery').on('click',function(e){
  		e.preventDefault();
  		deliveryLocal($(this).attr('itemid'));
  		return false;
  	});

  	$('.menu-local-selection').on('click',function(e){
  		e.preventDefault();
  		menuLocal($(this),false);
  		return false;
  	});

  	$('.menu-local-delete').on('click',function(e){
  		e.preventDefault();
  		menuLocal($(this),true);
  		return false;
  	});

  	$('.menu-local-selected').on('click',function(e){
  		e.preventDefault();
  		return false;
  	});
  }

  function menuLocal(link,isdelete){
  	type 	= $(link).attr('data-menu-type');
  	title = $(link).attr('data-menu-title');
  	local = $(link).attr('data-local-id');
  	if(!isdelete){
  		menu 	= $(link).attr('data-menu-id');
  		updateMenuLocal(type,menu,local,title);
  	}else
  		deleteMenuLocal(type,local,title);
  }

  function getLocalInfo(id){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('locals.info') }}",
	    data: {id: id},
	    success:function(data){
	    	switch(data.meta.status){
	        case 'ok'		: showToast('Local','información obtenida','success');
	        							localTextinputs(data.data);
	        							$('#frmAddLocal').removeClass('new-local').addClass('edit-local');
									    	$('#frmAddLocal').modal({backdrop:'static',show: true});
									    	$('#local_tabs_main a:first').tab('show');
									    	timetableInputs();
									    	showAddressMap();
									    	showZoneMap();
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al intntar obtener la información del local:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	showMessage('Aviso','Se produjo un error al intentar obtener la información del local, intente nuevamente','warning');
	    }
	  });
  }

  function localTextinputs(data){
  	$('#txtname').val(data.name);
  	$('#txtcode').val(data.code);
  	$('#gmaplat').val(data.address.point.lat);
  	$('#gmaplng').val(data.address.point.lng);
  	$('#txtaddress').val(data.address.address);
  	for(i=0;i<data.contact.length;i++){
  		$('#txt'+data.contact[i].name+'email').val(data.contact[i].email[0]);
  		$('#txt'+data.contact[i].name+'phone').val(data.contact[i].phone[0]);
  	}
  	for(i=0;i<data.timetable.length;i++){
  		$('#day-'+i+'-open').val(data.timetable[i].open);
  		$('#day-'+i+'-close').val(data.timetable[i].close);
  	}
  	for(i=0;i<data.zones.length;i++){
  		$('#zone-'+(i+1)+'-points').val(data.zones[i].point.join());
  	}
  }

  function clearForm(){
  	$('#txtname,#txtcode,#gmaplat,#gmaplng,#txtaddress').val('');
  	var contact = ["reservas","delivery"];
  	for(i=0;i<contact.length;i++){
  		$('#txt'+contact[i]+'email').val('');
  		$('#txt'+contact[i]+'phone').val('');
  	}
  	for(i=0;i<7;i++){
  		$('#day-'+i+'-open').val('12:00');
  		$('#day-'+i+'-close').val('23:00');
  	}
  	for(i=0;i<3;i++){
  		$('#zone-'+(i+1)+'-points').val('');
  	}
  }

  function tableEvents(){
  	$('#dataTable').on( 'order.dt', function () {
    	actionButtons();
		});

		$('#dataTable').on( 'page.dt', function () {
	    actionButtons();
		});
  }

  function deleteLocal(id){
  	var row = $("tr[rowid='"+id+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Eliminar Local',
      message: '&iquest;Desea eliminar el local <b>'+title+'</b>?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: 'fa fa-trash',
          label: 'Eliminar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            deleteLocalAction(id,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function deleteLocalAction(id,title,dialog,$button,row){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('locals.delete') }}",
	    data: {id: id},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							row.addClass('delete');
	        							var table = $('#dataTable').DataTable();
	        							table.row('.delete').remove().draw( false );
	        							showToast('Local eliminado','Se eliminó el local <b>'+title+'</b>','success');
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al eliminar el local:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar eliminar el local, intente nuevamente','warning');
	    }
	  });
  }

  function deliveryLocal(id){
  	var row 			= $("tr[rowid='"+id+"']");
  	var title 		= row.find('.product-title').html();
  	var delivery 	= row.find('.btnDelivery').hasClass('btn-success');

  	BootstrapDialog.show({
  		type: (delivery)?BootstrapDialog.TYPE_DANGER:BootstrapDialog.TYPE_SUCCESS,
      title: (delivery)?'Suspender delivery':'Activar delivery',
      message: '&iquest;Desea '+((delivery)?'<b>suspender</b> el servicio de delivery':'<b>activar</b> el servicio de delivery')+' en el local '+title+'?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-delivery-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delivery-action',
          icon: 'fa fa-truck '+((delivery)?'':'fa-flip-horizontal'),
          label: (delivery)?'Suspender':'Activar',
          cssClass: (delivery)?'btn-danger':'btn-success',
          action: function(dialog) {
          	title = 'Se '+((delivery)?'suspendió el servicio de delivery':'activó el servicio de delivery')+' en el local '+title;
            deliveryLocalAction(id,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function deliveryLocalAction(id,title,dialog,$button,row){
  	$('#btn-delivery-action, #btn-delivery-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-delivery-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('locals.delivery') }}",
	    data: {id: id},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delivery-action, #btn-delivery-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							showToast('Servicio de delivery',title,'success');
	        							updateLocal(data.data);
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delivery-action, #btn-delivery-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error, intente nuevamente','warning');
	    }
	  });
	}

  function updateMenuLocal(type,menu,local, menutitle){
  	var row 	= $("tr[rowid='"+local+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_WARNING,
      title: 'Cambiar menú',
      message: '&iquest;Desea cambiar el menú <b>'+menutitle+'</b> en el local '+title+'?<br/>Si hay un menú activo y se están realizando pedidos, este cambio puede causar errores.',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-menu-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-menu-action',
          icon: 'fa fa-check',
          label: 'Cambiar',
          cssClass: 'btn-warning',
          action: function(dialog) {
          	title = 'Se cambió el menú <b>'+menutitle+'</b> para el local '+title;
            updateMenuLocalAction(local,menu,type,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function updateMenuLocalAction(local,menu,type,title,dialog,$button,row){
  	$('#btn-menu-action, #btn-menu-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-menu-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('locals.menu') }}",
	    data: {local: local, menu: menu, type: type},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-menu-action, #btn-menu-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							showToast('Cambiar menú',title,'success');
	        							updateLocal(data.data);
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-menu-action, #btn-menu-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error, intente nuevamente','warning');
	    }
	  });
  }

function deleteMenuLocal(type,local,menutitle){
  	var row 			= $("tr[rowid='"+local+"']");
  	var title 		= row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Borrar menú',
      message: '&iquest;Desea borrar el menú <b>'+menutitle+'</b> en el local '+title+'?<br/>Este cambio puede causar errores.',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-menu-delete-close',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-menu-delete-action',
          icon: 'fa fa-trash-o',
          label: 'Borrar',
          cssClass: 'btn-danger',
          action: function(dialog) {
          	title = 'Se eliminó el menú <b>'+menutitle+'</b> para el local '+title;
            deleteMenuLocalAction(local,type,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function deleteMenuLocalAction(local,type,title,dialog,$button,row){
  	$('#btn-menu-delete-action, #btn-menu-delete-close').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-menu-delete-close').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('locals.menu.delete') }}",
	    data: {local: local, type: type},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-menu-delete-action, #btn-menu-delete-close').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							showToast('Cambiar menú',title,'success');
	        							updateLocal(data.data);
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-menu-delete-action, #btn-menu-delete-close').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error, intente nuevamente','warning');
	    }
	  });
  }
</script>
<tr rowid="{{$comercio['id']}}">
  <td >
  	@include('app.apikeys.item.name',['name'=>$comercio['name'],'version'=>$comercio['apikey']['version'],'logo'=>$comercio['logo']])
  </td>
  <td>
  	@include('app.apikeys.item.clases',['apikey'=>$comercio['apikey']])
  </td>
  <td>
  	@include('app.apikeys.item.apikey',['apikey'=>$comercio['apikey']])
  </td>
  <td>
 		@include('app.apikeys.item.actions',['id'=>$comercio['id'],'version'=>$comercio['apikey']['version'],'suspended'=>$comercio['apikey']['suspended']])
  </td>
</tr>
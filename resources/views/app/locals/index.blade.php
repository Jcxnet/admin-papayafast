@extends('layouts.app')

@section('htmlheader_title')
	Locales
@endsection


@section('main-content')
	 <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de Locales por Comercio</h3>
          <div class="form-group">
		        <label for="selectComercio" class="col-sm-2 control-label">Comercio</label>
		        <div class="col-sm-8">
		        	<input type="hidden" name="txtcomercioid" id="txtcomercioid" value="" />
		          <select class="form-control" id="selectComercio">
                <option value=''>Selecciona un comercio</option>
                @foreach($comercios as $comercio)
                	<option value="{{$comercio['id']}}"> {{$comercio['name']}}</option>
                @endforeach
              </select>
		        </div>
						<div class="col-sm-2">
							@permission('add-local')
		        		<a href="#" id="btnAddLocal" class="btn btn-primary btn-flat pull-right">Agregar Local</a>
		        	@endpermission
		        </div>
		      </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-striped ">
            <thead>
            <tr>
              <th>Local</th>
              <th>Dirección</th>
              <th>Contacto</th>
              <th>Menú</th>
              <th></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>Local</th>
              <th>Dirección</th>
              <th>Contacto</th>
              <th>Menú</th>
              <th></th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

  @include('app.locals.form')

@endsection

@push('pagescript')
	<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

	<script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDsAn3nOIS1Q7_RrMcLIzoPJT-Np3PyXkU'></script>
	<script type="text/javascript" src="{{asset('js/maps.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/gmaps.min.js')}}"></script>

	@include('app.locals.functions')

@endpush
@foreach($menu as $item)
	@if(is_array($item))
		@foreach($item as $name => $id)
			<span class="text-left margin-r5">
				<?php
					switch(strtolower($name)){
						case 'delivery'	: $class='btn-success'; $menuTitle = 'Delivery'; break;
						case 'takein'		: $class='btn-info'; 		$menuTitle = 'En local'; break;
						case 'takeout'	: $class='btn-warning'; $menuTitle = 'Para llevar'; break;
						default: $class='btn-default'; $menuTitle = 'No definido'; break;
					}
					$disabled = '';
					if($id == ''){
						$class = 'btn-default text-muted';
					}
					$menuType = [];
					if(array_key_exists(strtoupper($name),$menus)){
						$menuType = $menus[strtoupper($name)];
					}
					$hasOne = false;
				?>
				@if(count($menuType)>0)
				<div class="btn-group">
          <button type="button" class="btn btn-flat btn-sm btn-menu-local {{$class}}">{{$menuTitle}}</button>
          <button type="button" class="btn btn-flat btn-sm {{$class}} dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only">&nbsp;</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            @permission('menu-local')
	            @foreach($menuType as $menuItem)
	            	<li>
	            		<?php $classLink = ($menuItem['id'] == $id)?'menu-local-selected':'menu-local-selection'; ?>
	            		<a href="#" data-menu-id="{{$menuItem['id']}}" data-local-id="{{$localid}}" data-menu-type="{{strtoupper($name)}}" data-menu-title="{{$menuTitle}}" class="{{$classLink}}">
	            			@if($menuItem['id'] == $id)
	            				<i class="fa fa-check"></i>
	            				<?php $hasOne = true;?>
	            			@endif
	            			{{$menuItem['title']}}
	            		</a>
	            	</li>
	            @endforeach
            @endpermission
            @permission('nomenu-local')
	            @if($hasOne)
		            <li class="divider">&nbsp;</li>
		            <li>
		            	<a href="#" class="menu-local-delete" data-local-id="{{$localid}}" data-menu-type="{{strtoupper($name)}}" data-menu-title="{{$menuTitle}}">
		            		<i class="fa fa-trash-o"></i> Borrar
		            	</a>
		            </li>
	            @endif
	          @endpermission
          </ul>
        </div>
        @else
        	<button type="button" class="btn btn-flat btn-sm btn-menu-local {{$class}}">{{$menuTitle}}</button>
        @endif
			</span>
		@endforeach
	@endif
@endforeach
	<li class="item-{{$type}}" id="item-{{$type}}-{{$item['order']}}" sort-id="{{$type}}-{{$item['order']}}" item-type="{{$type}}" item-id="{{$item['id']}}" item-order="{{$item['order']}}" item-title="{{$item['title']}}">
		<div class="box box-warning box-solid">
	    <div class="box-header with-border">
	      <h3 class="box-title"><span class="drag fa fa-arrows" aria-hidden="true"></span>&nbsp;{{$item['title']}}
	      	<span data-toggle="tooltip" title="Total de propiedades" id="list_group_{{$item['order']}}_total" class="badge bg-orange"></span>
	      </h3>
	      <div class="box-tools pull-right">
	        <button class="btn btn-box-tool item-menu-delete" item-type="{{$type}}" item-order="{{$item['order']}}" item-id="{{$item['id']}}"><i class="fa fa-times"></i></button>
	        <button class="btn btn-box-tool item-menu-edit" item-type="{{$type}}" item-order="{{$item['order']}}" item-id="{{$item['id']}}"><i class="fa fa-pencil"></i></button>
	      </div>
	    </div>
	    <div class="box-body">
	      <ul class="list-container container-border list-height-auto" id="list_group_{{$item['order']}}"></ul>
	    </div>
	  </div>
  </li>
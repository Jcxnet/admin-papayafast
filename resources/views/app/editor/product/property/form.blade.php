  		<div class="box box-success box-solid">
  			<div class="box-header with-border">
          <h3 class="box-title">Propiedad</h3>
        </div>
	  		<form class="form-horizontal">
	  			<div class="box-body text-black">
		  			<input type="hidden" id="txtpropertyid" name="txtpropertyid" />
		  			<input type="hidden" id="txtpropertyorder" name="txtpropertyorder" />
		        <div class="form-group">
		          <label for="txtpropertytitle" class="col-sm-2 control-label">Título</label>
		          <div class="col-sm-10">
		          	<input type="text" class="form-control" id="txtpropertytitle" placeholder="Título">
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="txtpropertyprice" class="col-sm-2 control-label">Precio</label>
		          <div class="col-sm-4">
		          	<input type="text" class="form-control item-price" id="txtpropertyprice" placeholder="0.00" value="0.00" maxlength="10">
		          </div>
		          <label for="txtpropertycode" class="col-sm-2 control-label">Código</label>
		          <div class="col-sm-4">
		          	<input type="text" class="form-control" id="txtpropertycode" placeholder="Código">
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="txtpropertyquantity" class="col-sm-2 control-label">Cantidad</label>
		          <div class="col-sm-4">
		          	<input type="text" class="form-control item-quantity" id="txtpropertyquantity" placeholder="1" maxlength="3" value="1">
		          </div>
		          <label for="txtpropertyavailable" class="col-sm-2 control-label">Disponible</label>
		          <div class="col-sm-4">
		          	<input type="checkbox" class="item-available" name="txtpropertyavailable" id="txtpropertyavailable" checked>
		          </div>
		        </div>
	        </div>
	        <div class="box-footer">
            <a class="btn btn-default" id="btn-property-cancel" href="#">Cancelar</a>
            <a class="btn btn-success pull-right" id="btn-property-submit" href="#">Guardar cambios</a>
          </div>
	      </form>
      </div>

<script type="text/javascript">
	$(function(){
		$('#btn-property-cancel, #btn-property-submit').unbind();

		$('#btn-property-cancel').on('click',function(){
			closePropertyForm();
		});

		$('#btn-property-submit').on('click',function(e){
			e.preventDefault();
			validateProperty();
			return false;
		});

		var prprice = new Cleave('#txtpropertyprice', {
			    numeral: true,
			    numeralThousandsGroupStyle: 'thousand',
			    numeralDecimalScale: 2
			});

		var prquantity = new Cleave('#txtpropertyquantity', {
		    numeral: true,
		    numeralThousandsGroupStyle: 'thousand',
		    numeralDecimalScale: 0
		});

	});


	function showPropertyForm(){
		$('#box-properties').addClass('direct-chat-contacts-open');
		$('html, body').animate({
      scrollTop: $("#box-properties").offset().top-100
  	}, 700);
	}

	function closePropertyForm(){
		$('#box-properties').removeClass('direct-chat-contacts-open');
		$('#btn-add-property').fadeIn(500);
		return false;
	}

	function getPropertyData(){
		var data = {
			id 					: $('#txtpropertyid').val(),
			title 			: $('#txtpropertytitle').val(),
			price 			: $('#txtpropertyprice').val(),
			code 				: $('#txtpropertycode').val(),
			order 			: $('#txtpropertyorder').val(),//getLastOrder('#list_properties_zone')+1,
			cantidad		: $('#txtpropertyquantity').val(),
			available 	: $('#txtpropertyavailable').is(":checked"),
			group 			: 0,
			grouptitle	: '',
			grouporder	: 0
		};
		return JSON.stringify(data);
	}

	function validateProperty(){
		var dataForm = getPropertyData();
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('editor.property.add') }}",
      data: dataForm,
      beforeSend:function(){
        overlayContent('#editor-content');
      },
      success:function(data){
      	removeOverlay('#editor-content');
        switch(data.meta.status){
          case 'ok'		: closePropertyForm();
          							updateProductList('list_properties_zone','property',data.data.order, data.data.title, data.data.propertyitem);
          							updateTotalContainer('#list_properties_zone','#list_properties_total');
          							sortPropertiesList();
          							showToast('Propiedad','los datos se actualizaron correctamente','success');
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-content');
      	showMessage('Aviso','Se produjo un error al intentar agregar la propiedad, intente nuevamente','warning');
      }
    });
	}

	function editProperty(order){
		var item = '#item-property-'+order;
		if(!$(item).length){
			showMessage('Aviso','no se encontró la propiedad, intente nuevamente','warning');
			return false;
		}
		$('#txtpropertyid').val( $(item).attr('item-id') );
		$('#txtpropertyorder').val( $(item).attr('item-order') );
		$('#txtpropertytitle').val( $(item).attr('item-title') );
		$('#txtpropertyprice').val( $(item).attr('item-price') );
		$('#txtpropertycode').val( $(item).attr('item-code') );
		$('#txtpropertyquantity').val( $(item).attr('item-quantity') );
		var state = (parseInt($(item).attr('item-available'))==1);
		if(state)
			$('#txtpropertyavailable').attr('checked', true );
		else
			$('#txtpropertyavailable').removeAttr('checked');
		$('#txtpropertyavailable').bootstrapSwitch('state',state);
		$('#btn-add-property').fadeOut(500);
		showPropertyForm();
	}

	function deleteProperty(order){
		var item = '#item-property-'+order;
		if(!$(item).length){
			showMessage('Aviso','no se encontró la propiedad, intente nuevamente','warning');
			return false;
		}

		var dialog = BootstrapDialog.confirm({
        title:  'Eliminar propiedad',
        message: '&iquest; desea eliminar la propiedad <b>'+$(item).attr('item-title')+'</b>?',
        type: BootstrapDialog.TYPE_DANGER,
        closable: false,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Eliminar',
        btnOKClass: 'btn-danger',
        callback: function(result) {
        		dialog.close();
        		if(result){
        			$(item).addClass('removed-item').remove();
        			showToast('Eliminar','se eliminó la propiedad','success');
        			updateTotalContainer('#list_properties_zone','#list_properties_total');
        		}
        }
    });
	}

</script>
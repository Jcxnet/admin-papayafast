<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Menu;
use App\Comercio;
use Validator;


class EditorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show editor dashboard
     * @return View
     */
    public function index(Request $request){
    	if(!$this->userhasPermission('editor-menu'))
    		return $this->ajaxError('No tiene permiso para realizar esta acción');
    	if(!$menu = Menu::where('uuid',$request->id)->first())
    		return view('app.editor.nomenu',['message'=>'No se encontró el menú seleccionado']);
    	if(!$comercio = Comercio::where('id',$menu->comercio_id)->first())
    		return view('app.editor.nomenu',['message'=>'No se pudo encontrar el Comercio asociado al Menú']);
    	if(!$menu = $this->getMenuInfo($menu,$comercio))
    		return view('app.editor.nomenu',['message'=>'No se pudo obtener la información del menú seleccionado']);

    	$menu['pagetitle'] = ($menu['page'] == false || $menu['page'] == 'false')?'Por definir':$menu['pages'][(integer)$menu['page']]['title'];
      return view('app.editor.index',['menu'=>$menu]);
    }

    /**
     * extrae la información del menú y la formatea para el editor
     * @param  Menu $menu
     * @param  Comercio $comercio
     * @return array
     */
    private function getMenuInfo($menu,$comercio){
    	$item = ['menus'=>[$menu->uuid],'edit'=>true];
  		$item = $this->sendData( config('apiendpoints.API_MENUS_LIST'),$item,[],"POST",$comercio->apikey->token);
  		if($item['meta']['status'] != 'ok')
  			return false;

  		$menu = $item['data'][0];
  		$menu['comercio'] 	= $comercio->name;
  		$menu['type']				= $this->enumMenuType($menu['type']);

  		$menu['pages'] 			= collect($menu['pages'])->sortBy('title')->toArray();
  		$menu['buttons'] 		= collect($menu['buttons'])->sortBy('title')->toArray();
  		$menu['products'] 	= collect($menu['products'])->sortBy('title')->toArray();
  		$menu['properties'] = collect($menu['properties'])->sortBy('title')->toArray();

  		$menu['max']['page'] 			= $this->getMaxKey($menu['pages'],'id');
  		$menu['max']['button'] 		= $this->getMaxKey($menu['buttons'],'id');
  		$menu['max']['product']		= $this->getMaxKey($menu['products'],'id');
  		$menu['max']['property']	= $this->getMaxKey($menu['properties'],'id');

  		$menu['total']['page'] 			= count($menu['pages']);
  		$menu['total']['button'] 		= count($menu['buttons']);
  		$menu['total']['product']		= count($menu['products']);
  		$menu['total']['property']	= count($menu['properties']);

  		return $menu;
    }

    /**
     * recibe información del menú y retorna la vista resumen en html
     * @param  Request $request
     * @return json
     */
    public function resume(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$menu = $request->all();
    		$view =  view('app.editor.sections.resume',['menu'=>$menu]);
    		return $this->ajaxData(['content'=>$view->render()],'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * retorna el formulario para agregar/editar un elemento del menú
     * @param Request $request
     * @return json
     */
    public function addForm(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$type = $request->type)
    			return $this->ajaxError('No se especificó el tipo de elemento');
    		switch(strtolower($type)){
    			case 'page'			: $view =  view('app.editor.page.form'); break;
    			case 'button'		: $view =  view('app.editor.button.form'); break;
    			case 'product'	: $view =  view('app.editor.product.form'); break;
    			default 				: return $this->ajaxError('El tipo de elemento no existe'); break;
    		}
    		return $this->ajaxData(['content'=>$view->render()],'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * retorna un elemento del menú en formato html
     * @param  Request $request
     * @return json
     */
    public function itemHtml(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$request->merge(json_decode($request->getContent(),true));
    		if(!$item = $request->item)
    			return $this->ajaxError('Debe proporcionar el ítem');
    		$view = view('app.editor.item.item',['item'=>$item,'type'=>$item['type']]);
    		return $this->ajaxData(['item'=>$item,'content'=>$view->render()],'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * valida los datos para una página del menú
     * @param Request $request
     * @return json
     */
    public function addPage(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$request->merge(json_decode($request->getContent(),true));
    		$data = $request->only(['id','title']);
  			$validator = Validator::make($data, [
		      'id'		=> 'required|integer|min:0',
		      'title'	=> 'required|string|min:3|max:200|regex:/^[0-9\pL\s\.]+$/u', //regex:/^[0-9\pL\s\.]+$/u -> alpha num + dot
		    ]);
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    $view = view('app.editor.item.item',['item'=>$data,'type'=>'page']);
		    $data['pageitem'] = $view->render();
		    return $this->ajaxData($data,'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * guarda el menú en la database
     * @param  Request $request
     * @return json
     */
    public function saveMenu(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$request->merge(json_decode($request->getContent(),true));
    		if(!$menu = Menu::where('uuid',$request->id)->first())
    			return $this->ajaxError('No se encontró el menú');
    		if(!$comercio = Comercio::where('id',$menu->comercio_id)->first())
    			return $this->ajaxError('No se encontró el comercio');

    		$data = $request->only(['page','pages','buttons','products','properties']);
    		$menu->menu = $this->prepareMenuData($data);
    		$menu->version = date("YmjHi00");
    		$menu->save();
    		if(!$menu = $this->getMenuInfo($menu,$comercio))
    			return $this->ajaxError('No se pudo obtener la información del menú');
		    return $this->ajaxData(['menu'=>$menu],'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * crea un sidebar para un grupo de elementos del menú
     * @param  Request $request
     * @return json
     */
    public function createSidebar(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$type = $request->type)
    			return $this->ajaxError('Debe especificar el tipo de elemento');
    		$type = strtolower($type);
    		$item = ['id' => $request->id, 'title' => $request->title];
    		switch($type){
    			case 'page'		:	$view = view('app.editor.sections.sidebarone',['type'=>'page','bgcolor'=>'bg-blue','item'=>$item, 'typename'=>'página']); break;
    			case 'button'	:	$view = view('app.editor.sections.sidebarone',['type'=>'button','bgcolor'=>'bg-aqua','item'=>$item, 'typename'=>'botón']); break;
    			case 'product':	$view = view('app.editor.sections.sidebarone',['type'=>'product','bgcolor'=>'bg-green','item'=>$item, 'typename'=>'producto']); break;
    			default 			: return $this->ajaxError('El tipo de elemento es desconocido');
    		}
    		return $this->ajaxData(['sidebar' => $view->render()],'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * ordena una lista de elementos de un contenedor
     * @param  Request $request
     * @return json
     */
    public function orderList(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$request->merge(json_decode($request->getContent(),true));
    		if(!$list = $request->items)
    			return $this->ajaxError('hace falta la lista de elementos');
    		$list = collect($list)->sortBy('order')->values()->toArray();
    		$content = '';
    		foreach($list as $item){
    			if($item['type']=='property')
    				$view = view('app.editor.product.property.item',['type'=>$item['type'],'item'=>$item]);
    			elseif($item['type'] == 'group')
    				$view = view('app.editor.product.group.item',['type'=>$item['type'],'item'=>$item]);
    			else
    				$view = view('app.editor.item.item',['type'=>$item['type'],'item'=>$item]);
    			$content .= $view->render();
    		}
    		return $this->ajaxData(['list'=>$list, 'content'=>$content],'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * valida los datos de un botón del menú
     * @param Request $request
     * @return json
     */
    public function addButton(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$request->merge(json_decode($request->getContent(),true));
    		$data = $request->only(['id','title']);
  			$validator = Validator::make($data, [
		      'id'		=> 'required|integer|min:0',
		      'title'	=> 'required|string|min:3|max:200|regex:/^[0-9\pL\s\.]+$/u', //regex:/^[0-9\pL\s\.]+$/u -> alpha num + dot
		    ]);
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    $view = view('app.editor.item.item',['item'=>$data,'type'=>'button']);
		    $data['buttonitem'] = $view->render();
		    return $this->ajaxData($data,'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * valida los datos de un producto del menú
     * @param Request $request
     * @return json
     */
    public function addProduct(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		/*dd('data received->',$request->all());
    		$request->merge(json_decode($request->getContent(),true));*/
    		$data = $request->only(['id','title','price','code','cantidad','available','detail','order']);
  			$validator = Validator::make($data, [
		      'id'				=> 'required|integer|min:0',
		      'title'			=> 'required|string|min:3|max:200|regex:/^[0-9\pL\s\.]+$/u', //regex:/^[0-9\pL\s\.]+$/u -> alpha num + dot
		      'detail'		=> 'string|min:3|max:200|regex:/^[0-9\pL\s\.]+$/u', //regex:/^[0-9\pL\s\.]+$/u -> alpha num + dot
		      'price' 		=> 'required|numeric|min:1',
		      'code'			=> 'required',
		      'cantidad' 	=> 'required|integer|min:1',
		      'available' => 'required|boolean',
		      'order' 		=> 'required|integer|min:0',
		    ]);
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    $data['price'] = sprintf('%.2f',$data['price']);
		    /*$view = view('app.editor.item.item',['item'=>$data,'type'=>'product']);
		    $data['productitem'] = $view->render();*/
		    $data['properties'] =[];
			$data['groups'] = [];
			$data['sizes'] =[];
			$data['extras'] = [];

		    if($request->hasFile('image'))
				$data['image'] = $this->imageUpload($request->file('image'),$data['code'],'PRODUCT','products');
			else
				$data['image'] = $this->imageType($data['code'],'PRODUCT','products');

	      	if($data['image']['exists'] == true){
	      		$data['thumbnail'] = $data['image']['data']['images']['conversions']['img400x200']['url'];
	      		$data['image'] = [$data['image']['data']['id']];
	      	}else{
	      		$data['image'] = [];
	      	}

		    return $this->ajaxData($data,'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * valida los datos de un propiedad de un producto del menú
     * @param Request $request
     * @return json
     */
    public function addProperty(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$request->merge(json_decode($request->getContent(),true));
    		$data = $request->only(['id','title','price','code','cantidad','available','order','group','grouptitle','grouporder']);
  			$validator = Validator::make($data, [
		      'id'				=> 'required|integer|min:0',
		      'title'			=> 'required|string|min:3|max:200|regex:/^[0-9\pL\s\.]+$/u', //regex:/^[0-9\pL\s\.]+$/u -> alpha num + dot
		      'price' 		=> 'required|numeric|min:0',
		      'code'			=> 'required',
		      'cantidad' 	=> 'required|integer|min:1',
		      'available' => 'required|boolean',
		      'order' 		=> 'required|integer|min:0',
		    ]);
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    $data['price'] = sprintf('%.2f',$data['price']);
		    $view = view('app.editor.product.property.item',['item'=>$data,'type'=>'property']);
		    $data['propertyitem'] = $view->render();
		    return $this->ajaxData($data,'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * valida los datos de un grupo de un producto del menú
     * @param Request $request
     * @return json
     */
    public function addGroup(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('editor-menu'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$request->merge(json_decode($request->getContent(),true));
    		$data = $request->only(['id','title','order']);
  			$validator = Validator::make($data, [
		      'id'				=> 'required|integer|min:0',
		      'title'			=> 'required|string|min:3|max:200|regex:/^[0-9\pL\s\.]+$/u', //regex:/^[0-9\pL\s\.]+$/u -> alpha num + dot
		      'order' 		=> 'required|integer|min:0',
		    ]);
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    $view = view('app.editor.product.group.item',['item'=>$data,'type'=>'group']);
		    $data['groupitem'] = $view->render();
		    return $this->ajaxData($data,'ok');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * prepara los datos del menú antes de almacenarlos en la database
     * @param  array $data
     * @return array
     */
    private function prepareMenuData($data){
    	$data['menu']			= ['page'=>$data['page']];
    	$data['pages'] 		= $this->noOrder($data['pages']);
    	$data['pages'] 		= $this->formatImages($data['pages']);
    	$data['buttons'] 	= $this->formatImages($data['buttons']);
    	$data['products'] = $this->formatImages($data['products']);
    	$data['products'] = $this->formatProperties($data['products']);
    	$data['properties'] = $this->noNUllItems($data['properties']);
    	$menu[] = [
    		'menu' 			=> $data['menu'],
    		'pages' 		=> $data['pages'],
    		'buttons' 	=> $data['buttons'],
    		'products' 	=> $data['products'],
    		'properties'=> $data['properties']
    	];//$data;
    	return $menu;
    }

    /**
     * retira el item 'order' de una lista de elementos
     * @param  array $array
     * @return array
     */
    private function noOrder($array){
    	$list = [];
    	foreach ($array as $key => $item) {
    		if(is_array($item)){
    			if(array_key_exists('order',$item)){
	    			if(is_array($item['order']))
	    				unset($item['order']);
	    		}
	    		$list[$key] = $item;
    		}
    	}
    	return $list;
    }

    /**
     * formatea las propiedades de un producto antes de ser enviado a la database
     * @param  array $array
     * @return array
     */
    private function formatProperties($array){
    	$list = [];
    	foreach ($array as $key => $item) {
    		if(array_key_exists('properties', $item))
    			$item['properties'] = $this->extractProperties([],$item['properties']);
    		if(array_key_exists('groups',$item)){
    			foreach ($item['groups'] as $group) {
    				if(array_key_exists('properties',$group))
    					$item['properties'] = $this->extractProperties($item['properties'],$group['properties']);
    			}
    		}
    		$item['groups'] = [];
    		$list[$key] = $item;
    	}
    	return $list;
    }

    /**
     * extrae las propiedades de un grupo de un producto
     * @param  array $list
     * @param  array $properties
     * @return array
     */
    private function extractProperties($list,$properties){
    	foreach ($properties as $property) {
    		if(is_array($property))
    			$list[] = $property['id'];
    		else
    			$list[] = $property;
    	}
    	return $list;
    }

    /**
     * filtra los elementos nulos de una lista
     * @param  array $array
     * @return array
     */
    private function noNUllItems($array){
    	$list = [];
    	foreach ($array as $key => $item) {
    		if($key != null && $item != null){
    			$list[$key] = $item;
    		}
    	}
    	return $list;
    }

    /**
     * formatea los valores de las imágenes de un elemento
     * @param  array $array
     * @return array
     */
    private function formatImages($array){
    	$list = [];
    	foreach($array as $key => $item){
    		if(is_array($item)){
    			if(array_key_exists('imageids',$item)){
	    			$item['image'] = $item['imageids'];
	    			unset($item['imageids']);
	    		}
	    		$list[$key] = $item;
    		}
    	}
    	return $list;
    }

    /**
     * obtiene el valor máximo (key) de una lista
     * @param  array 	$array
     * @param  string $key
     * @return integer
     */
    private function getMaxKey($array,$key){
    	$max = collect($array)->max($key);
    	if(is_null($max))
    		return 0;
    	return $max;
    }


}

<div class="form-group">
  <div class="col-sm-12">
    <div class="input-group">
      <div class="input-group-btn">
        <button type="button" class="btn btn-warning" id="btnGeolocation"><i class="fa fa-crosshairs" aria-hidden="true"></i>&nbsp;Dirección</button>
      </div>
      <input type="text" class="form-control" id="txtaddress" placeholder="Dirección del local">
      <div class="input-group-btn">
        <button type="button" class="btn btn-info" id="btnSearchAddress"><i class="fa fa-search" aria-hidden="true"></i></button>
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-12">
  	<div id="gmap_div"></div>
  	@include ('app.maps.address',['divmap'=>'#gmap_div','inputaddress'=>'#txtaddress'])
  </div>
</div>

@push('pagescript')
<script type="text/javascript">
	function dataAddressLocal(){
		var data = {
			lat  : $('#gmaplat').val(),
			lng  : $('#gmaplng').val(),
			address : $('#txtaddress').val(),
		};
		return data;
	}
</script>
@endpush
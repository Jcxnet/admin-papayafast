<div class="btn-group text-center">
  @permission('addrol-user')
  	<button type="button" class="btn btn-success btn-flat btnRole" title="Roles" itemid="{{$id}}"><i class="fa fa-bookmark"></i></button>&nbsp;
  @endpermission
  @permission('edit-user')
  	<button type="button" class="btn btn-info 	 btn-flat btnEdit" title="Editar" 		itemid="{{$id}}"><i class="fa fa-pencil"></i></button>&nbsp;
  @endpermission
  @permission('delete-user')
  @if($id != $me)
  	<button type="button" class="btn btn-danger  btn-flat btnDelete" title="Eliminar" itemid="{{$id}}"><i class="fa fa-trash"></i></button>
  @endif
  @endpermission
</div>
 	<input type="hidden" name="gmaplat" id="gmaplat" value="" />
	<input type="hidden" name="gmaplng" id="gmaplng" value="" />
	<input type="hidden" name="gmapaddress" id="gmapaddress" value="" />


@push('pagescript')
	<script type="text/javascript">

	var map;
	var marker;

	function showAddressMap() {
			var lat = parseFloat($('#gmaplat').val());
			var lng = parseFloat($('#gmaplng').val());
			var geolocate = false;

			if (lat=='' || lng == '' || isNaN(lat) || isNaN(lng) )
				geolocate = true;

			lat = (lat=='' || isNaN(lat))?-12.04644:lat;
			lng = (lng=='' || isNaN(lng))?-77.04277:lng;

			map = new GMaps({
				  div : '{{$divmap}}',
				  height: '300px',
				  width: '100%',
			  	lat: lat,
     			lng: lng,
      		zoom: 16,
		      streetViewControl: false,
		      mapTypeControl: false,
		      click: function(point){
		      	map.setCenter(point.latLng.lat(), point.latLng.lng());
		      	marker.setPosition(new google.maps.LatLng(point.latLng.lat(), point.latLng.lng()));
		      	updateLatLng(point.latLng.lat(), point.latLng.lng());
		      	getAddress(point.latLng.lat(), point.latLng.lng(),'{{$inputaddress}}');
		      	//refreshMap(map);
		      },
			});

			refreshMap(map);

			marker = map.addMarker({
			  lat: lat,
      	lng: lng,
			  draggable: true,
	  		animation: google.maps.Animation.DROP,
			});

			google.maps.event.addListener(marker, 'dragend', function(e){
				map.setCenter(marker.position.lat(),marker.position.lng());
				updateLatLng(marker.position.lat(),marker.position.lng());
				getAddress(marker.position.lat(), marker.position.lng(),'{{$inputaddress}}');
      	//refreshMap(map);
			});

			if(geolocate){
				geoLocation(map,marker,'{{$inputaddress}}');
				map.setCenter(lat, lng);
			}
	}

	</script>
@endpush
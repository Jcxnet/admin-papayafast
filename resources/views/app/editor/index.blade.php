@extends('layouts.editor')

@section('menutitle'){{$menu['title']}}@endsection

@section('menuid'){{$menu['id']}}@endsection

@section('editor-sidebar')
	@include('app.editor.sections.sidebar',['menu'=>$menu])
@endsection

@section('editor-content')
	@include('app.editor.sections.resume',['menu'=>$menu])
@endsection


@push('pagescript')
	<script type="text/javascript" src="{{ asset('plugins/bootstrap-multiselect/multiselect.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/store2.min.js')}}"></script>
	@include ('app.editor.functions',['menu'=>$menu])
	@include ('app.menus.preview')
@endpush

@push('pagecss')
	<link rel="stylesheet" href="{{asset('plugins/bootstrap-multiselect/multiselect.css')}}" type="text/css"/>
	<link rel="stylesheet" href="{{asset('css/preview.css')}}" type="text/css"/>
@endpush
  		<div class="box box-warning box-solid">
  			<div class="box-header with-border">
          <h3 class="box-title">Grupo</h3>
        </div>
	  		<form class="form">
	  			<div class="box-body text-black">
		  			<input type="hidden" id="txtgroupid" name="txtgroupid" />
		  			<input type="hidden" id="txtgrouporder" name="txtgrouporder" />
		        <div class="form-group">
		          <label for="txtgrouptitle" class="col-sm-2 control-label">Título</label>
		          <div class="col-sm-10">
		          	<input type="text" class="form-control" id="txtgrouptitle" placeholder="Título">
		          </div>
		        </div>
	        </div>
	        <div class="box-footer">
            <a href="#" class="btn btn-default" id="btn-group-cancel">Cancelar</a>
            <a href="#" class="btn btn-warning pull-right" id="btn-group-submit">Guardar cambios</a>
          </div>
	      </form>
      </div>

<script type="text/javascript">
	$(function(){
		$('#btn-group-cancel, #btn-group-submit').unbind();
		$('#btn-group-cancel').on('click',function(){
			closeGroupForm();
			return false;
		});
		$('#btn-group-submit').on('click',function(e){
			e.preventDefault();
			validateGroup();
			return false;
		});
	});

	function showGroupForm(){
		$('#box-groups').addClass('direct-chat-contacts-open');
		$('html, body').animate({
      scrollTop: $("#box-groups").offset().top-100
  	}, 700);
	}
	function closeGroupForm(){
		$('#box-groups').removeClass('direct-chat-contacts-open');
		$('#btn-add-group').fadeIn(500);
	}

	function getGroupData(){
		var data = {
			id 					: $('#txtgroupid').val(),
			title 			: $('#txtgrouptitle').val(),
			order 			: $('#txtgrouporder').val(),//getLastOrder('#list_groups_zone')+1,
		};
		return JSON.stringify(data);
	}

	function validateGroup(){
		var dataForm = getGroupData();
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('editor.group.add') }}",
      data: dataForm,
      beforeSend:function(){
        overlayContent('#editor-content');
      },
      success:function(data){
      	removeOverlay('#editor-content');
        switch(data.meta.status){
          case 'ok'		: closeGroupForm();
          							updateProductList('list_groups_zone','group',data.data.order, data.data.title, data.data.groupitem);
          							updateTotalContainer('#list_groups_zone','#list_groups_total','item-group');
          							sortAllProductLists();
          							showToast('Grupo','los datos se actualizaron correctamente','success');
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-content');
      	showMessage('Aviso','Se produjo un error al intentar agregar el grupo, intente nuevamente','warning');
      }
    });
	}

	function editGroup(order){
		var item = '#item-group-'+order;
		if(!$(item).length){
			showMessage('Aviso','no se encontró el grupo, intente nuevamente','warning');
			return false;
		}
		$('#txtgroupid').val( $(item).attr('item-id') );
		$('#txtgrouporder').val( $(item).attr('item-order') );
		$('#txtgrouptitle').val( $(item).attr('item-title') );

		$('#btn-add-group').fadeOut(500);
		showGroupForm();
	}

	function deleteGroup(order){
		var item = '#item-group-'+order;
		if(!$(item).length){
			showMessage('Aviso','no se encontró el grupo, intente nuevamente','warning');
			return false;
		}

		var dialog = BootstrapDialog.confirm({
        title:  'Eliminar grupo',
        message: '&iquest; desea eliminar el grupo <b>'+$(item).attr('item-title')+'</b>?',
        type: BootstrapDialog.TYPE_DANGER,
        closable: false,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Eliminar',
        btnOKClass: 'btn-danger',
        callback: function(result) {
        		dialog.close();
        		if(result){
        			$(item).find('.item-property').appendTo('#list_properties_zone');
        			updateTotalContainer('#list_properties_zone','#list_properties_total');
        			$(item).addClass('removed-item').remove();
        			showToast('Eliminar','se eliminó el grupo','success');
        			updateTotalContainer('#list_groups_zone','#list_groups_total','item-group');
        		}
        }
    });
	}

</script>


<span class="hide">{{$local['name']}}</span>
<div class="products-list">
	<div class="product-img">
    <img src="{{$local['images']['thumbnail']}}" alt="" class="img-circle" />
  </div>
	<div class="product-info">
    <a href="#" class="product-title" data-title="{{$local['name']}}">{{$local['name']}}</a>
     <span class="product-description" data-description="{{$local['code']}}">
       id: {{$local['code']}}
     </span>
  </div>
</div>
<span class="hide">{{$menu['title']}}</span>
<div class="products-list">
	<div class="product-img">
    <img src="" alt="" class="img-circle" />
  </div>
	<div class="product-info">
    <a href="#" class="product-title" data-title="{{$menu['title']}}">{{$menu['title']}}</a>
     <span class="product-description" data-type="{{$menu['type']}}">
       version: {{$menu['version']}}
     </span>
  </div>
</div>
@extends('layouts.app')

@section('htmlheader_title')
	Permisos
@endsection


@section('main-content')
<div class="row">
  <div class="col-sm-12">
		<div class="box" id="permissionsbox">
		    <div class="box-header">
		      <h3 class="box-title">Permisos por Rol</h3>
		      <a href="#" id="btnDefault" class="btn btn-warning btn-flat pull-right"><i class="fa fa-exclamation-triangle"></i>&nbsp;restaurar Roles y Permisos</a>
		    </div><!-- /.box-header -->
		    <div class="box-body no-padding">
		      @include('app.permissions.table',['roles'=>$roles,'actions'=>$actions])
		    </div><!-- /.box-body -->
		  </div><!-- /.box -->
	</div>
</div>

@endsection

@push('pagescript')
<script type="text/javascript">

$(function(){
	resetButton();
	actionButtons();
});

	function resetButton(){
		$('#btnDefault').unbind();
		$('#btnDefault').on('click', function(e){
			e.preventDefault();
			resetAction();
			return false;
		});
	}

	function actionButtons(){
		$('.allow-action, .block-action').unbind();
		$('.allow-action').on('click',function(e){
			e.preventDefault();
			allowAction($(this).attr('item-action-id'),$(this).attr('item-role-id'),$(this),$(this).parent());
			return false;
		});
		$('.block-action').on('click',function(e){
			e.preventDefault();
			blockAction($(this).attr('item-action-id'),$(this).attr('item-role-id'),$(this),$(this).parent());
			return false;
		});
	}

	function allowAction(action,role,item,parent){
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('permissions.allow') }}",
      data: {role: role, action: action},
      beforeSend:function(){
      	overlayContent('#permissionsbox');
      },
      success:function(data){
      	removeOverlay('#permissionsbox');
        switch(data.meta.status){
          case 'ok'		: showToast('Permitir acción','se activó la acción para el Rol','success');
												$(parent).html(data.data.content);
												$(item).remove();
												actionButtons();
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#permissionsbox');
      	showMessage('Aviso','Se produjo un error al intentar activar la acción, intente nuevamente','warning');
      }
    });
	}

	function blockAction(action,role,item,parent){
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('permissions.block') }}",
      data: {role: role, action: action},
      beforeSend:function(){
      	overlayContent('#permissionsbox');
      },
      success:function(data){
      	removeOverlay('#permissionsbox');
        switch(data.meta.status){
          case 'ok'		: showToast('Bloquear acción','se bloqueó la acción para el Rol','success');
												$(parent).html(data.data.content);
												$(item).remove();
												actionButtons();
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#permissionsbox');
      	showMessage('Aviso','Se produjo un error al intentar bloquear la acción, intente nuevamente','warning');
      }
    });
	}


	function resetAction(){
		var dialog = BootstrapDialog.confirm({
        title:  'Roles y Permisos',
        message: '&iquest; Desea restaurar los <b>Roles</b> y <b>Permisos</b> por defecto del sistema?<br/>Esta acción eliminará todos los permisos asignados actualmente y restaurará los Roles y Permisos por defecto en el sistema.',
        type: BootstrapDialog.TYPE_WARNING,
        closable: false,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Restaurar',
        btnOKClass: 'btn-warning',
        callback: function(result) {
        		dialog.close();
        		if(result){
        			resetRoles();
        		}
        }
    });
	}

	function resetRoles(){
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('permissions.restore') }}",
      beforeSend:function(){
        overlayContent('#permissionsbox');
        $('#btnDefault').hide();
      },
      success:function(data){
      	removeOverlay('#permissionsbox');
      	$('#btnDefault').show();
        switch(data.meta.status){
        	case 'ok'		: showMessage('Roles y Permisos','se restablecieron los Roles y Permisos','success');
        								$('#permissionsbox').find('.box-body').html(data.data.content);
        								actionButtons();
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#permissionsbox');
      	$('#btnDefault').show();
      	showMessage('Aviso','Se produjo un error al intentar agregar el elemento, intente nuevamente','warning');
      }
    });
	}

</script>
@endpush
  <div class="btn-group">
	  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
	  </button>
	  <ul class="dropdown-menu" role="menu">
	    @permission('edit-menu')
	    	<li><a href="#" class="btnEdit" title="Editar" itemid="{{$id}}"><i class="fa fa-pencil"></i> Editar</a></li>
	    @endpermission
	    @permission('clone-menu')
  			<li><a href="#" class="btnClone" title="Duplicar" itemid="{{$id}}"><i class="fa fa-files-o"></i> Duplicar</a></li>
  		@endpermission
  		@permission('delete-menu')
  			<li><a href="#" class="btnDelete" title="Eliminar" itemid="{{$id}}"><i class="fa fa-trash"></i> Eliminar</a></li>
  		@endpermission
  		<li class="divider"></li>
  		@permission('editor-menu')
	  		<li><a href="{{route('editor.main',['id'=>$id])}}" class="btnBuilder" title="Editor" itemid="{{$id}}"><i class="fa fa-cubes"></i> Editor</a></li>
	  	@endpermission
	  	@permission('preview-menu')
  			<li><a href="#" class="btnPreview" title="Vista previa" itemid="{{$id}}"><i class="fa fa-eye"></i> Vista previa</a></li>
  		@endpermission
  		<li class="divider"></li>
		  @permission('activate-menu|deactivate-menu')
			  @if($active)
			  	<li><a href="#" class="btnActive menu-active" title="Desactivar menú" itemid="{{$id}}"><i class="fa fa-pause"></i> Desactivar menú</a></li>
			  @else
			  	<li><a href="#" class="btnActive menu-inactive" title="Activar menú" itemid="{{$id}}"><i class="fa fa-play"></i> Activar menú</a></li>
			  @endif
			@endpermission
	  </ul>
	</div>
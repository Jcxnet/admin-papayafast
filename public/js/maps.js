
	function geoLocation(map,marker,input){
	 	GMaps.geolocate({
      success: function(position){
        map.setCenter(position.coords.latitude, position.coords.longitude);
        marker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
        updateLatLng(position.coords.latitude,position.coords.longitude);
        showToast('Google maps','Geolocalización correcta','success');
        getAddress(position.coords.latitude,position.coords.longitude,input);
        return true;
      },
      error: function(error){
        showToast('Google maps','La geolocalización falló:'+error.message,'error');
      },
      not_supported: function(){
        showToast('Google maps','Su navegador no soporta geolocalización','warning');
      },
    });
    return false;
	 }

	function getAddress(lat, lng, input){
		var latlng = {lat: lat, lng:lng};
  	var geocoder = new google.maps.Geocoder;
  	geocoder.geocode({"location": latlng}, function(results, status){
  		if (status === google.maps.GeocoderStatus.OK) {
      	if (results[0]) {
	        updateLatLng(lat, lng);
	        var data = {address: results[0].formatted_address, lat: lat, lng: lng};
	        $(input).val(data.address);
	        return true;
	      } else {
	      	showToast('Google maps','No encontramos esa drección, intenta nuevamente','warning');
	      	return false;
	      }
	    } else {
	      showToast('Google maps','El servicio de ubicación ha fallado, intenta nuevamente','error');
	      return false;
	    }
  	});
	}

  function searchAddress(address,map,marker,input) {
		if (address == '') return false;
		var geocoder = new google.maps.Geocoder;
    geocoder.geocode( { 'address': address,componentRestrictions: {country: 'PE'}}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      		var location = results[0].geometry.location;
        	map.setCenter(location.lat(),location.lng());
        	marker.setPosition(new google.maps.LatLng(location.lat(), location.lng()));
        	var latlng = {lat: location.lat(), lng: location.lng()};
        	geocoder.geocode({"location": latlng}, function(results, status){
        		if (status === google.maps.GeocoderStatus.OK) {
      				if (results[0]) {
	        			updateLatLng(location.lat(),location.lng());
	        			var data = {address: results[0].formatted_address, lat:location.lat(), lng:location.lng()};
	        			$(input).val(data.address);
	        			return data;
	        		}
	        	}else{
	        		return false;
	        	}
        	});
      } else {
      	showToast('Google maps','El servicio de ubicación ha fallado, intenta nuevamente','error');
      	return false;
      }
    });
  }

	function updateLatLng(lat,lng){
		$('#gmaplat').val(lat);
		$('#gmaplng').val(lng);
	}

	function refreshMap(mapVar){
	 	var intervalR = setInterval(function () {
	 			mapVar.refresh();
	 			google.maps.event.trigger(mapVar, 'resize');
        clearInterval(intervalR);
	  }, 500);
	}
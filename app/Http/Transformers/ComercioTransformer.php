<?php

namespace App\Http\Transformers;

use App\Comercio;
use League\Fractal\TransformerAbstract;

class ComercioTransformer extends TransformerAbstract{

	public function transform(Comercio $comercio){
		return [
			'id'	 				=> $comercio->uuid,
      'name'  			=> $comercio->name,
      'description' => $comercio->description,
      'email'				=> $comercio->email,
      'phone'				=> $comercio->phone,
      'ruc'					=> $comercio->ruc,
      'menus'				=> $comercio->menus->toArray(),
      'logo'				=> $this->comercioImage($comercio->uuid),
		];
	}

	private function comercioImage($id){
		$logo = \App\Http\Controllers\Controller::getImageItem($id,'LOGO','logos');
    if($logo['exists'] == true)
    	return $logo['data']['images']['conversions']['img300x300']['url'];
    return asset('img/icon/comercio.png');
	}


}

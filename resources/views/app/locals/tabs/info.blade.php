<div class="form-group">
  <label for="txtname" class="col-sm-2 control-label">Local</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="txtname" placeholder="Nombre del local">
  </div>
</div>
<div class="form-group">
  <label for="txtcode" class="col-sm-2 control-label">Código</label>
  <div class="col-sm-10">
    <input type="text" class="form-control" id="txtcode" placeholder="Código del local">
  </div>
</div>

<div class="form-group">
  <div class="col-sm-2">
  	<label for="image" class="control-label">Imagen</label>
  	<div class="products-list">
			<div class="product-img">
		    <img src="" alt="" id="localimage" class="img-circle" />
		  </div>
		</div>
  </div>
  <div class="col-sm-10">
    <input type="file" id="image" name="image">
    <p class="help-block">Seleccione una imagen para el Local.</p>
  </div>
</div>

@push('pagescript')
<script type="text/javascript">
	function dataInfoLocal(){
		var data = {
			name 			: $('#txtname').val(),
			code 			: $('#txtcode').val(),
			comercio 	: $('#txtcomercioid').val(),
			local 		: $('#txtid').val()
		};
		return data;
	}
</script>
@endpush
<tr rowid="{{$role['role']['id']}}">
  <td >
  	@include('app.roles.item.role',['role'=>$role['role']])
  </td>
  <td>
  	@include('app.roles.item.permissions',['permissions'=>$role['permissions']])
  </td>
  <td>
  	@include('app.roles.item.users',['users'=>$role['users']])
  </td>
  <td>
  	@include('app.roles.item.actions',['id'=>$role['role']['id']])
  </td>
</tr>
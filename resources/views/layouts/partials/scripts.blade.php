<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>
<!-- Bootstrap dialogs -->
<script src="{{ asset('plugins/bootstrap-dialog/js/bootstrap-dialog.min.js') }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/fastclick/fastclick.min.js') }}" type="text/javascript"></script>
<!-- Scroll -->
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- Toast -->
<script src="{{ asset('plugins/toast/jquery.toast.min.js') }}" type="text/javascript"></script>
<!--Common -->
<script src="{{ asset('/js/common.js') }}" type="text/javascript"></script>

<div class="info-box">
  <span class="info-box-icon {{$bgclass}}"><i class="fa {{$icon}}"></i></span>
  <div class="info-box-content">
    <span class="info-box-text">{{$title}}</span>
    <span class="info-box-number">{{$text}}</span>
  </div>
</div>
<tr><td colspan="{{(count($roles)+1)}}"><h4 class="text-black text-bold">{{$action}}</h4></td></tr>
@foreach($actions as $action)
<tr item-action="{{$action['name']}}" item-action-id="{{$action['id']}}">
  <td>
  	<i class="fa fa-circle-o text-light-blue"></i>&nbsp;<b>{{$action['displayname']}}</b><br/>
  	<span class="text-light-blue">{{$action['description']}}</span>
  </td>
  @foreach($roles as $role)
  	@if(array_key_exists($action['id'], $role['permissions']))
  		<td class="text-center" >@include('app.permissions.buttonallow',['roleid'=>$role['role']['id'],'actionid'=>$action['id']])</td>
  	@else
  		<td class="text-center" >@include('app.permissions.buttonblock',['roleid'=>$role['role']['id'],'actionid'=>$action['id']])</td>
  	@endif
  @endforeach
</tr>
@endforeach
<script type="text/javascript">

	function previewMenu(menu){
		var dialog = new BootstrapDialog({
    title: 'Vista previa',
    size: BootstrapDialog.SIZE_WIDE,
    closable: true,
		closeByBackdrop: false,
    closeByKeyboard: true,
    draggable: true,
    message: function(dialogRef) {
        var $message = $('<div>Cargando vista previa del menú...</div>');
        $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
        $.ajax({
            url: "{{route('menus.preview')}}",
            method: "POST",
            data: {id: menu},
            context: {
                thisDialog: dialogRef
            },
            success: function(data) {
            	if(data.meta.status == 'ok'){
            		saveMenuPreview(data.data.menu);
                $('.bootstrap-dialog-message').html(data.data.content);
                actionButtonLink();
                this.thisDialog.setTitle ( data.data.title+' v:'+data.data.version+' ['+data.data.comercio+']' );
            	}
            	else{
            		this.thisDialog.close();
  							showMessage('Aviso','Se produjo un error:<br/>'+data.data.message,'warning');
            	}
            },
            error:function (){
            	this.thisDialog.close();
  						showMessage('Aviso','Se produjo un error al intentar obtener el menú, intente nuevamente','warning');
            }
        });

        return $message;
	    }
		});
		dialog.open();
  }

  function saveMenuPreview(menu){
  	preview = store.namespace('preview');
		preview.clearAll();
		store.preview.set('menu',menu);
  }

  function actionButtonLink(){
  	$('.button-link,.btn-preview-back').unbind();
  	$('.button-link').on('click',function(e){
  		e.preventDefault();
  		var id 			= $(this).attr('item-id');
  		var type 		= $(this).attr('item-type');
  		if(type == 'button'){
  			var menu = store.preview.get('menu');
				if(menu['buttons']  === undefined || menu['buttons'] == null || menu['buttons'] == ''){
					showMessage('Aviso','no encontramos la información para mostrar el menú','warning');
					return false;
				}
				var button = menu['buttons'][id];
  			if(parseInt(button['page'])>0){
  				var page = menu['pages'][parseInt(button['page'])];
  				page['buttons']  = getItemsMenu( menu['buttons'],  page['buttons'],  'button');
    			page['products'] = getItemsMenu( menu['products'], page['products'], 'product');
  				showPagePreview(page);
  			}
  		}
  		return false;
  	});

  	$('.btn-preview-back').on('click',function(e){
  		e.preventDefault();
  		var menu 		= store.preview.get('menu');
  		var backid 	= parseInt($(this).attr('back-id'));
  		var page 		= menu['pages'][backid];
			page['buttons']  = getItemsMenu( menu['buttons'],  page['buttons'],  'button');
			page['products'] = getItemsMenu( menu['products'], page['products'], 'product');
			showPagePreview(page);
  		return false;
  	});
  }

  function getItemsMenu(items, list, type){
  	if(list.length == 0  || items.length == 0)
  		return [];
  	var selection = [];
  	var item = undefined;
  	for(i=0;i<list.length; i++){
  		item = items[parseInt(list[i])];
  		if( item != undefined){
  			item['type'] = type;
  			selection.push(item);
  		}
  	}
  	return selection;
  }

  function showPagePreview(page){
  	$.ajax({
      url: "{{route('menus.preview.page')}}",
      method: "POST",
      data: {page: JSON.stringify(page)},
      beforeSend: function(){
      	var overlay = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
				$(overlay).insertAfter( $('.box-body') );
      },
      success: function(data) {
      	$('.overlay').remove();
      	if(data.meta.status == 'ok'){
          $('.bootstrap-dialog-message').html(data.data.content);
          actionButtonLink();
      	}
      	else{
      		this.thisDialog.close();
					showMessage('Aviso','Se produjo un error:<br/>'+data.data.message,'warning');
      	}
      },
      error:function (){
      	$('.overlay').remove();
				showMessage('Aviso','Se produjo un error al intentar obtener el menú, intente nuevamente','warning');
      }
    });
  }

</script>

function dialogType(type){
	switch(type){
	 	case 'info'		: type = BootstrapDialog.TYPE_INFO; 		break;
	 	case 'primary': type = BootstrapDialog.TYPE_PRIMARY; 	break;
	 	case 'success': type = BootstrapDialog.TYPE_SUCCESS; 	break;
	 	case 'warning': type = BootstrapDialog.TYPE_WARNING; 	break;
	 	case 'danger'	: type = BootstrapDialog.TYPE_DANGER; 	break;
	 	default 			: type = BootstrapDialog.TYPE_DEFAULT;
	 }
	 return type;
}

function showMessage(title,message,type){

  BootstrapDialog.show({
      type: dialogType(type),
      title: title,
      message: message,
      buttons: [{
          label: 'Ok',
          action: function(dialogRef){
          	dialogRef.close();
          }
      }]
  });
}

function showAlert(title, message, type){
	BootstrapDialog.show({
      type: dialogType(type),
      title: title,
      message: message,
  });
}

function closeAllDialogs(){
	$.each(BootstrapDialog.dialogs, function(id, dialog){
      dialog.close();
  });
}

function showToast(title, message, type, time){
	if(typeof(time) == 'undefined')
		time = 2;
	$.toast({
    heading   : title,
    text      : message,
    hideAfter : time * 1000,
    position	: 'bottom-center',
    icon			: type,
    loader		: false,
    showHideTransition	: 'slide',
    allowToastClose			: true,
  });
}

function getTimestamp(){
	if (!Date.now) {
  	Date.now = function() { return new Date().getTime(); }
	}
	return parseInt(Date.now()/1000);
}

function isArray(obj){
  return !!obj && Array === obj.constructor;
}

function overlayContent(div){
	var overlay = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
	$(overlay).insertAfter( $(div).find('.box-body').first() );
}

function removeOverlay(div){
	$(div).find('.overlay').remove();
}
<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Comercio;
use App\Local;
use Validator;


class ComerciosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show all comercios in database
     * @return View
     */
    public function index(){
    	if(!$this->userhasPermission('list-comercio'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    	if($this->userhasRole('admin'))
    		$comercios = Comercio::orderBy('name')->get();
    	else
    		$comercios = Comercio::where('id',$this->userComercio())->orderBy('name')->get();
     	$comercios = $this->getCollection($comercios->toArray(),'App\Comercio','App\Http\Transformers\ComercioTransformer');

     	/*for($i=0;$i<count($comercios);$i++){
     		$logo = $this->imageType($comercios[$i]['id'],'LOGO','logos');
      	if($logo['exists'] == true)
      		$comercios[$i]['logo'] = $logo['data']['images']['conversions']['img300x300']['url'];
     	}*/

      return view('app.comercios.index',['comercios'=>$comercios]);
    }

    /**
     * agrega / edita un Comercio
     * @param Request $request [description]
     * @return json
     */
    public function add(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('add-comercio') && !$this->userhasPermission('edit-comercio'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$data = $request->only(['name','description','email','phone','ruc']);
    		$id = isset($request->id)?$request->id:false;
    		if($id){
	    		if(!$id = Comercio::select('id')->where('uuid',$id)->first())
	    			return $this->ajaxError('No se pudo encontrar el comercio');
	    		$id = $id->id;
	    		if(!$this->userhasRole('admin')){
	    			if($id != $this->userComercio())
	    				return $this->ajaxError('No tiene permiso para editar este Comercio');
	    		}

	    	}
    		if($id){
    			$validator = Validator::make($data, [
			      'name'					=> 'required|string|min:4|max:100|unique:comercios,name,'.$id,
			      'email'					=> 'email|unique:comercios,email,'.$id,
			      'description' 	=> 'string|min:4|max:250',
			      'phone' 				=> 'string|min:4|max:20',
			      'ruc' 					=> 'string|size:11',
			    ]);
    		}else{
    			$validator = Validator::make($data, [
			      'name'					=> 'required|string|min:4|max:100|unique:comercios,name',
			      'email'					=> 'email|unique:comercios,email',
			      'description' 	=> 'string|min:4|max:250',
			      'phone' 				=> 'string|min:4|max:20',
			      'ruc' 					=> 'string|size:11',
			    ]);
    		}
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    try{
		    	if($id)
		    		$comercio = Comercio::find($id);
		    	else
		    		$comercio = new Comercio();
		    	$comercio->name   = $data['name'];
					$comercio->email 	= $data['email'];
					$comercio->phone  = $data['phone'];
					$comercio->ruc  	= $data['ruc'];
					$comercio->description  = $data['description'];
					$comercio->save();
					if(!$id)
						$comercio = Comercio::find($comercio->id);
					$comercio = $this->getItem($comercio,'App\Http\Transformers\ComercioTransformer');

					if($request->hasFile('image'))
			    	$comercio['image'] = $this->imageUpload($request->file('image'),$comercio['id'],'LOGO','logos');
			    else
			    	$comercio['image'] = $this->imageType($comercio['id'],'LOGO','logos');

	      	if($comercio['image']['exists'] == true)
	      		$comercio['logo'] = $comercio['image']['data']['images']['conversions']['img300x300']['url'];

					$comercio['cols'] = $this->tableCols($comercio);
					return $this->ajaxData($comercio);
		    }catch(Exception $e){
		    	return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
		    }
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * formatea la información de un Comercio
     * @param  Comercio $comercio
     * @return json
     */
    private function tableCols($comercio){
    	$cols =[];
    	$cols['name'] 		= view('app.comercios.item.name',['name'=>$comercio['name'],'description'=>$comercio['description'],'logo'=>$comercio['logo']])->render();
		  $cols['contact'] 	= view('app.comercios.item.contact',['email'=>$comercio['email'],'phone'=>$comercio['phone']])->render();
		  $cols['ruc'] 			= view('app.comercios.item.ruc',['ruc'=>$comercio['ruc']])->render();
		  $cols['actions'] 	= view('app.comercios.item.actions',['id'=>$comercio['id']])->render();
    	return json_encode($cols);
    }

    /**
     * elimina un Comercio
     * @param  Request $request
     * @return json
     */
    public function delete(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('delete-comercio'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($id = $request->id){
    			if($comercio = Comercio::where('uuid',$id)->first()){
    				$comercio->name .= sprintf("[deleted %s]",date('Y-m-j H:i:s'));
    				$comercio->save();
						if($comercio->delete())
	    				return $this->ajaxData([],'ok','Comercio eliminado');
	    			else
	    				return $this->ajaxError('No se pudo eliminar el comercio');
    			}else{
    				return $this->ajaxError('No se encontró el comercio');
    			}
	   		}else{
    			return $this->ajaxError('Se necesita el identificador del comercio');
    		}
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * retorna la lista de Locales y sus Menús que pertenecen a un Comercio
     * @param  Request $request
     * @return json
     */
    public function locals(Request $request){
	   	if($request->ajax()){
	   		if(!$this->userhasPermission('locals-comercio'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
	   		if(!$comercio = Comercio::where('uuid',$request->id)->first())
	   			return $this->ajaxError('No se pudo encontrar el comercio');
	   		if(!$locals = Local::where('comercio_id',$comercio->id)->orderBy('name')->get())
	   			return $this->ajaxError('No se encontraron locales');
	   		$locals = $this->getCollection($locals->toArray(),'App\Local','App\Http\Transformers\LocalTransformer');
	   		$menus = $this->getCollection($comercio->menus->toArray(),'App\Menu','App\Http\Transformers\MenucomercioTransformer');
     		$menus = collect($menus)->groupBy('type')->toArray();
	   		if(count($locals) > 0)
	   			return $this->ajaxData(['locals'=>$locals,'menus'=>$menus]);
	   		return $this->ajaxData(['locals'=>[],'menus'=>[]],'empty','Debe agregar locales');
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }


}
@extends('layouts.app')

@section('htmlheader_title')
	Menús
@endsection


@section('main-content')
	 <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de Menús por Comercio</h3>
          <div class="form-group">
		 	<label for="selectComercio" class="col-sm-2 control-label">Comercio</label>
		        <div class="col-sm-8">
		        	<input type="hidden" name="txtcomercioid" id="txtcomercioid" value="" />
		          <select class="form-control" id="selectComercio">
                <option value=''>Selecciona un comercio</option>
                @foreach($comercios as $comercio)
                	<option value="{{$comercio['id']}}"> {{$comercio['name']}}</option>
                @endforeach
              </select>
		        </div>
						<div class="col-sm-2">
		        	@permission('add-menu')
		        		<a href="#" id="btnAddMenu" class="btn btn-primary btn-flat pull-right">Agregar Menú</a>
		        	@endpermission
		        </div>
		      </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-striped ">
            <thead>
            <tr>
              <th>Menú</th>
              <th>Tipo</th>
              <th>Detalles</th>
              <th>Locales</th>
              <th></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>Menú</th>
              <th>Tipo</th>
              <th>Detalles</th>
              <th>Locales</th>
              <th></th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

  @include('app.menus.form')

@endsection

@push('pagescript')
	<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

	<script src="{{ asset('plugins/bootstrap-multiselect/multiselect.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/store2.min.js')}}"></script>

	@include('app.menus.functions')
	@include('app.menus.preview')

@endpush

@push('pagecss')
	<link rel="stylesheet" href="{{asset('plugins/bootstrap-multiselect/multiselect.css')}}" type="text/css"/>
	<link rel="stylesheet" href="{{asset('css/preview.css')}}" type="text/css"/>
@endpush
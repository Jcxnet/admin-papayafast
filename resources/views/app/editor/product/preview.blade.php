
<div class="col-sm-6">
	<figure class="preview-product">
	  @if(array_key_exists(0,$product['image']))
	  	<img src="{{$product['image'][0]}}" alt=""/>
	  @else
	  	<h3>image->{{json_encode($product['image'])}}</h3>
	  @endif
	  <figcaption>
	    <h3>{{$product['title']}}</h3>
	    <p>{{$product['detail']}}</p>
	    <p>
	    	<ul>
					@foreach($product['properties'] as $property)
						@include ('app.editor.product.property.preview',['property' => $property])
					@endforeach
				</ul>
				@foreach($product['groups'] as $group)
					@include ('app.editor.product.group.preview',['group' => $group])
				@endforeach
	    </p>
	  </figcaption>
	  <span class="preview-product-price">{{sprintf("%.2f",$product['price'])}}</span>
	</figure>
</div>

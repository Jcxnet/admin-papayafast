
@push('pagescript')
	<script type="text/javascript">

	var {{$zonevar}};
	var zonesInMap = [];

	function showZoneMap() {
		zonesInMap = [];
		 	{{$zonevar}} = new GMaps({
				  div : '{{$divmap}}',
				  height: '300px',
				  width: '100%',
				  lat: {{isset($lat)?$lat:-12.04644}},
      		lng: {{isset($lng)?$lng:-77.04277}},
      		zoom: 14,
		      streetViewControl: false,
		      mapTypeControl: false
			});
		refreshMap({{$zonevar}});
	}

	function drawZoneMap(points, line, bgcolor, id){
		var points = JSON.parse("[" + points + "]");
		if( zonesInMap[id] == undefined ){ //add zone to collection
	  	if(points.length<2) return false;
	  	var path = Array();
	  	for(var i=0;i<=points.length-1;i+=2){
	  		path.push([points[i],points[i+1]]);
	  	}
  		zonesInMap[id] = [path,true,null,getPolygonCenter(path)];
  	}else{
  		zonesInMap[id][1] = !(zonesInMap[id][1]);
  	}
  	if(zonesInMap[id][1]){
  		zonesInMap[id][2] = {{$zonevar}}.drawPolygon({
		    paths: zonesInMap[id][0],
		    strokeColor: line,
		    strokeOpacity: 0.8,
		    strokeWeight: 2,
		    fillColor: bgcolor,
		    fillOpacity: 0.25
		  });
		  var point = zonesInMap[id][3];
		  {{$zonevar}}.setCenter(point.lat(), point.lng());
  	}else{
  		{{$zonevar}}.removePolygon(zonesInMap[id][2]);
  	}
  	refreshMap({{$zonevar}});
	}


	function getPolygonCenter(pathArray){
		var bounds = new google.maps.LatLngBounds();
		var i;
		var polygonCoords = [];
		var point;

		for(i=0;i<pathArray.length;i++){
			point = pathArray[i];
			polygonCoords.push(new google.maps.LatLng(point[0], point[1]));
		}

		for (i = 0; i < polygonCoords.length; i++) {
		  bounds.extend(polygonCoords[i]);
		}
		return bounds.getCenter();
	}

	</script>
@endpush
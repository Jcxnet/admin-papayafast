<?php

namespace App\Http\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalTransformer extends TransformerAbstract{

	public function transform(Local $local){
		return [
			'id'	 				=> $local->uuid,
      'name'  			=> $local->name,
      'address' 		=> $local->address,
      'code'				=> $local->code,
      'contact'			=> $local->contact,
      'comercio'		=> $local->comercio->name,
      'menu'				=> $local->menu,
      'delivery'		=> $local->delivery,
			'images'			=> $this->LocalImage($local->uuid),
		];
	}

	private function localImage($id){
		$image = \App\Http\Controllers\Controller::getImageItem($id,'LOCAL','locals');
    if($image['exists'] == true){
    	return [
    		'image' 		=> array_key_exists('img800x200',$image['data']['images']['conversions'])?$image['data']['images']['conversions']['img800x200']['url']    										:asset('img/icon/comercio.png'),
    		'thumbnail' => array_key_exists('img200x200',$image['data']['images']['conversions'])?$image['data']['images']['conversions']['img200x200']['url']    										:asset('img/icon/comercio.png')
    	];
    }
    return [
    	'image' 		=> asset('img/icon/comercio.png'),
    	'thumbnail' => asset('img/icon/comercio.png')
    ];
	}

}

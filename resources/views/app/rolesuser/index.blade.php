@extends('layouts.app')

@section('htmlheader_title')
	Roles por usuario
@endsection


@section('main-content')
	 <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de usuarios</h3>
          @permission('add-user')
          	<a href="#" id="btnAddUser" class="btn btn-primary btn-flat pull-right">Agregar Usuario</a>
          @endpermission
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-striped ">
            <thead>
            <tr>
              <th>Usuario</th>
              <th>Roles</th>
              <th>Permisos</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            	@include ('app.rolesuser.item.item',['user'=>$user, 'me'=>$me])
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>Usuario</th>
              <th>Roles</th>
              <th>Permisos</th>
              <th></th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>


<div class="modal fade" tabindex="-1" role="dialog" id="frmAddUser">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-user" aria-hidden="true"></i> Agregar Usuario</h4>
      </div>
      <div class="modal-body">

				  <form class="form-horizontal" id="formUser" enctype="multipart/form-data" method="post">
				  	<input type="hidden" id="txtid" name="id" value="" />
			      <div class="form-group">
			        <label for="txtname" class="col-sm-2 control-label">Usuario</label>

			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtname" name="name" placeholder="Nombre de usuario">
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="txtemail" class="col-sm-2 control-label">Email</label>

			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtemail" name="email" placeholder="Email">
			        </div>
			      </div>
			      <div class="form-group form-new-data">
			        <label for="txtpassword" class="col-sm-2 control-label">Contraseña</label>

			        <div class="col-sm-10">
			          <input type="password" class="form-control" id="txtpassword" name="password" data-toggle="password"><br/>
			          <a href="#" id="generatePass">Generar contraseña</a>
			        </div>
			      </div>

			      <div class="form-group form-new-data">
              <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="sendmail" id="sendmail">Enviar email al usuario
                  </label>
                </div>
              </div>
            </div>
				  </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAddThisUser">Agregar Usuario</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="frmRoleUser">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-bookmark" aria-hidden="true"></i> Asignar rol</h4>
      </div>
      <div class="modal-body">

				  <form class="form-horizontal">
				  	<input type="hidden" id="txtid" value="" />
				  	<div class="form-group">
				  		<p class="text-blue text-center" id="roleUserActual">
				  			<b>Username</b> -	<span class="label label-primary text-sm">Role</span>
				  			<button type="button" class="btn btn-warning" id="btnNoRolesUser">Cancelar Rol</button>
				  		</p>
				  	</div>
				  	<div class="form-group">
			        <label for="selectRole" class="col-sm-2 control-label">Rol</label>

			        <div class="col-sm-10">
			          <select class="form-control" id="selectRole">
                  <option value=''>Selecciona un rol</option>
                  @foreach($roles as $role)
                  	<option value="{{$role['id']}}"> {{$role['displayname']}} - ({{$role['description']}})</option>
                  @endforeach
                </select>
			        </div>
			      </div>

			      <div class="form-group">
			        <label for="selectComercio" class="col-sm-2 control-label">Comercio</label>

			        <div class="col-sm-10">
			          <select class="form-control" id="selectComercio">
                  <option value=''>Selecciona un comercio</option>
                  @foreach($comercios as $comercio)
                  	<option value="{{$comercio['uuid']}}">{{$comercio['name']}}</option>
                  @endforeach
                </select>
			        </div>
			      </div>

				  </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnRoleThisUser">Asignar Rol</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@push('pagescript')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/password/password.generator.min.js') }}"></script>
<script src="{{ asset('plugins/password/show.password.min.js') }}"></script>
<script>

  $(function () {

    initTable();
    actionButtons();
    formButtons();
    tableEvents();

  });

  function formButtons(){
  	$('#btnAddUser').on('click',function(e){
    	e.preventDefault();
    	clearForm();
    	$('#btnAddThisUser').html('Agregar Usuario');
    	$('#frmAddUser').find('.modal-title').html('Agregar Usuario');
    	$('#frmAddUser').removeClass('edit-user').addClass('new-user');
    	$('#frmAddUser').modal({backdrop:'static',show: true});
    	$('#txtpassword').password('val', '');
    	$('#frmAddUser').find('.form-new-data').show();
    	return false;
    });

    $('#btnAddThisUser').on('click',function(e){
    	e.preventDefault();
    	if( $('#frmAddUser').hasClass('new-user') )
    		addNewUser();
    	if( $('#frmAddUser').hasClass('edit-user') )
    		editUser();
    	return false;
    });

    $('#generatePass').on('click',function(e){
    	e.preventDefault()
    	var pass = $.passGen({'length' : 10, 'numeric' : true, 'lowercase' : true, 'uppercase' : true, 'special' : false});
    	$('#txtpassword').password('val', pass);
    	$('#txtpassword').focus();
    	return false;
    });

    $('#btnRoleThisUser').on('click',function(e){
    	e.preventDefault();
    	roleUser();
    	return false;
    });
    $('#btnNoRolesUser').on('click',function(e){
    	e.preventDefault();
    	cancelRoleUser();
    	return false;
    });
  }

  function initTable(){
  	$('#dataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "autoWidth": true,
      "responsive": {
        "details": false
    	},
      "columns": [
			    { "orderable": true  },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false }
			  ]
    });
  }

  function addNewUser(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('rolesusers.add') }}",
      data: {name:$('#txtname').val(), email: $('#txtemail').val(), password: $('#txtpassword').val(), sendmail: $('#sendmail').is(':checked')},
      beforeSend:function(){
        showAlert('Agregar usuario','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Nuevo','Usuario creado','success');
          							showToast('Nuevo','Usuario creado','success');
          							$('#frmAddUser').modal('hide');
          							showUser(data.data.cols,data.data.id);
          							actionButtons();
          							clearForm();
          							break;
          case 'error': showMessage('Error','Error al crear el usuario:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar agregar el usuario, intente nuevamente','warning');
      }
    });
  }

  function editUser(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('rolesusers.add') }}",
      data: {id: $('#txtid').val(),name:$('#txtname').val(), email: $('#txtemail').val()},
      beforeSend:function(){
        showAlert('Editar usuario','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Editar','Usuario editado','success');
          							showToast('Editar','Usuario editado','success');
          							$('#frmAddUser').modal('hide');
          							clearForm();
          							updateRole(data.data);
          							break;
          case 'error': showMessage('Error','Error al editar el usuario:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar editar el usuario, intente nuevamente','warning');
      }
    });
  }

  function roleUser(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('rolesusers.assign') }}",
      data: {id: $('#txtid').val(), role: $('#selectRole').val(), comercio: $('#selectComercio').val()},
      beforeSend:function(){
        showAlert('Asignar rol','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Asignar rol','Rol asignado','success');
          							showToast('Asignar rol','Rol asignado','success');
          							$('#frmRoleUser').modal('hide');
          							clearForm();
          							updateRole(data.data);
          							break;
          case 'error': showMessage('Error','Error al asignar el rol al usuario:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar asignar el rol al usuario, intente nuevamente','warning');
      }
    });
  }

  function cancelRoleUser(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('rolesusers.remove') }}",
      data: {id: $('#txtid').val()},
      beforeSend:function(){
        showAlert('Cancelar rol','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Cancelar rol','Rol cancelado','success');
          							showToast('Cancelar rol','Rol cancelado','success');
          							$('#frmRoleUser').modal('hide');
          							clearForm();
          							updateRole(data.data);
          							break;
          case 'error': showMessage('Error','Error al cancelar el rol al usuario:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar cancelar el rol al usuario, intente nuevamente','warning');
      }
    });
  }

  function showUser(cols,id){
  	cols = JSON.parse(cols);
  	var table = $('#dataTable').DataTable();
		var node  = table.row.add( [cols.user, cols.roles, cols.permissions, cols.actions] ).node();
		table.column(0).data().sort().draw();
		$(node).attr('rowid',id);
  }

  function actionButtons(){
  	$('.btnDelete, .btnEdit, .btnRole').unbind();
  	$('.btnDelete').on('click',function(e){
  		e.preventDefault();
  		deleteUser($(this).attr('itemid'));
  		return false;
  	});
  	$('.btnEdit').on('click',function(e){
  		e.preventDefault();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		$('#frmAddUser').find('.modal-title').html('Editar Usuario');
  		$('#btnAddThisUser').html('Editar Usuario');
  		$('#txtid').val(id);
  		$('#txtname').val(row.find('.product-title').attr('data-title'));
  		$('#txtemail').val(row.find('.product-description').attr('data-description'));
    	$('#frmAddUser').removeClass('new-user').addClass('edit-user');
    	$('#frmAddUser').modal({backdrop:'static',show: true});
    	$('#frmAddUser').find('.form-new-data').hide();
    	return false;
  	});
  	$('.btnRole').on('click',function(e){
  		e.preventDefault();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		$('#txtid').val(id);
  		$('#roleUserActual').find('b').html(row.find('.product-title').attr('data-title'));
  		$('#roleUserActual').find('span').html(row.find('.role-title').attr('data-role'));
  		$('#frmRoleUser').modal({backdrop:'static',show: true});
  		return false;
  	});
  }

  function clearForm(){
  	$('#txtname,#txtemail,#txtid,#txtpassword').val('');
  }

  function tableEvents(){
  	$('#dataTable').on( 'order.dt', function () {
    	actionButtons();
		});

		$('#dataTable').on( 'page.dt', function () {
	    actionButtons();
		});
  }

  function deleteUser(id){
  	var row = $("tr[rowid='"+id+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Eliminar Usuario',
      message: '&iquest;Desea eliminar el usuario <b>'+title+'</b>?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: 'fa fa-trash',
          label: 'Eliminar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            deleteUserAction(id,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function deleteUserAction(id,title,dialog,$button,row){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('rolesusers.delete') }}",
	    data: {id: id},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							row.addClass('delete');
	        							var table = $('#dataTable').DataTable();
	        							table.row('.delete').remove().draw( false );
	        							//showMessage('Usuario eliminado','Se eliminó el usuario <b>'+title+'</b>','success');
	        							showToast('Usuario eliminado','Se eliminó el usuario <b>'+title+'</b>','success');
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al eliminar el usuario:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar eliminar el usuario, intente nuevamente','warning');
	    }
	  });
  }

  function updateRole(data){
  	var table = $('#dataTable').DataTable();
  	var row = $("tr[rowid='"+data.id+"']");
  	row.addClass('delete');
  	table.row('.delete').remove().draw( false );
  	showUser(data.cols,data.id);
  }

</script>
@endpush
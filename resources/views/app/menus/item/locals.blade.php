<span class="text-left clearfix">
	<div id ="locals-buttons-list-{{$id}}">
	@permission('deletelocal-menu')
		@foreach($locals as $local)
			<button class="btn btn-primary btn-flat btn-xs margin-5 btnLocalDelete" data-local-button="{{$local['name']}}" data-local-id="{{$local['id']}}" data-menu-id="{{$id}}">
				{{$local['name']}} &nbsp; <i class="fa fa-times"></i>
			</button>
		@endforeach
	@endpermission
	</div>
	@if(count($locals)>0)
		@permission('deletelocal-menu')
			<button class="btn btn-danger btn-flat btn-sm margin-r5 btnDeleteAll" data-menu-id="{{$id}}"><i class="fa fa-ban"></i></button>
		@endpermission
	@endif
		@if(count($select)>0)
			@permission('addlocal-menu')
				<select id="select-local-menu-{{$id}}" class="multiselect-local" data-menu-id="{{$id}}" multiple="multiple">
				    @foreach($select as $local)
				    	<option value="{{$local['id']}}">{{$local['name']}}</option>
				    @endforeach
				</select>
			@endpermission
		@endif
</span>
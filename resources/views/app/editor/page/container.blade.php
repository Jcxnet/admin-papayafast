
	<div class="box box-primary direct-chat direct-chat-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Elementos <span class="direct-chat-timestamp">(arrastre <i class="fa fa-external-link-square"></i>&nbsp;Botones o <i class="fa fa-tasks"></i>&nbsp;Productos a la lista)</span>
      &nbsp;<span data-toggle="tooltip" title="Total de elementos" id="list_add_zone_total" class="badge bg-blue">{{count($items)}}</span>
      </h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<ul class="list-container direct-chat-messages container-border" id="list_add_zone"></ul>
    </div>
    <div class="box-footer">
    	<div class="small-box bg-red">
        <div class="drop-zone inner" id="list_drop_zone">
        </div>
        <div class="icon">
          <i class="fa fa-trash-o"></i>
        </div>
        <span class="small-box-footer">
          Arrastre los elementos para eliminar
        </span>
      </div>
    </div>
  </div>

@extends('layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.notaccess') }}
@endsection

@section('contentheader_title')
    {{ trans('adminlte_lang::message.403error') }}
@endsection

@section('$contentheader_description')
@endsection

@section('main-content')

<div class="error-page">
    <h2 class="headline text-red"> <i class="fa fa-eye-slash" aria-hidden="true"></i> 403</h2>
    <div class="error-content">
        <h3><i class="fa fa-lock text-red"></i> Oops! {{ trans('adminlte_lang::message.403error') }}.</h3>
        <p>
            {{ trans('adminlte_lang::message.notaccess') }}
            {{ trans('adminlte_lang::message.mainwhile') }} <a href="{{ url('dashboard') }}">{{ trans('adminlte_lang::message.returndashboard') }}</a> {{ trans('adminlte_lang::message.usingsearch') }}
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection
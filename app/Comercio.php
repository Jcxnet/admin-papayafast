<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comercio extends Model
{
	use SoftDeletes;
	protected $table = 'comercios';
	protected $dates = ['deleted_at'];

	public function users(){
		return $this->hasMany('App\User');
	}

	public function apikey(){
		return $this->hasOne('App\Apikey');
	}

	public function locals(){
		return $this->hasMany('App\Local');
	}

	public function menus(){
		return $this->hasMany('App\Menu');
	}
}

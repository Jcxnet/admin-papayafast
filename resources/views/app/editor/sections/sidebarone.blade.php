<a href="#" class="item-menu-add panel-body {{$bgcolor}}" item-type="{{$type}}" sort-id="{{$type}}-0" >Agregar {{$typename}}</a>
<ul class="accordion-list panel-body scrollable-list" id="list_origin_{{$type}}">
	@include ('app.editor.item.item',['item'=>$item, 'type'=>$type])
</ul>
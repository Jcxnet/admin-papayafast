@if ($button['style'] == 'square')

<div class="button-preview button-link" item-type="button" item-id="{{$button['id']}}" >
	<img class="button-preview-image" src="{{$button['image'][0]}}" alt="">
	<span class="button-preview-caption">
    <a href="" title="link to {{$button['page']}}"><span class="color-white">{{$button['title']}}</span></a>
  </span>
</div>

@endif

@if ($button['style'] == 'circle')

<div class="button-link button-preview-circular" item-type="button" item-id="{{$button['id']}}" >
	<div class="row">
		<div class="col-xs-2">
			<img class="button-preview-circular-image" src="{{$button['image'][0]}}" alt="">
		</div>
		<div class="col-xs-10">
			<h3><a href="" title="link to {{$button['page']}}">{{$button['title']}}</a></h3>
    </div>
  </div>
</div>

@endif

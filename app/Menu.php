<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;
		protected $table = 'menus';
		protected $dates = ['deleted_at'];
		protected $casts = ['active' => 'boolean', 'menu' => 'array'];

		public function comercio(){
			return $this->belongsTo('App\Comercio');
		}
}

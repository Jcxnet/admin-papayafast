<?php

namespace App\Http\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract{

	public function transform(User $user){
		return [
			'id'	  	=> $user->uuid,
      'name'    => $user->name,
      'email'		=> $user->email,
      'avatar'	=> asset('img/avatar/avatar-01.jpg'),
      'comercio'=> $this->comercio($user->comercio),
		];
	}

	private function comercio($comercio){
		if($comercio)
			return $comercio->name;
		return null;
	}
}

<div class="set" id="sidebar-{{$type}}-items">
  <a href="#" class="accordion-set-header">
    {!!$header!!} <i class="fa fa-plus accordion-icon-header"></i>
  </a>
  <div class="accordion-set-content">
    <div class="panel panel-default no-margin">
	    @if(count($items)>0)
	    	<a href="#" class="item-menu-add panel-body {{$bgcolor}}" item-type="{{$type}}" sort-id="{{$type}}-0" >Agregar {{$typename}}</a>
	    	<ul class="accordion-list panel-body scrollable-list" id="list_origin_{{$type}}">
	    	@foreach($items as $item)
	    		@include ('app.editor.item.item',['item'=>$item, 'type'=>$type, 'typename' =>$typename, 'main' => $main])
	    	@endforeach
	    	</ul>
	    @else
	    	<div class="small-box bg-yellow margin">
          <div class="inner">
            <h3>&nbsp;</h3>
            <p>No se encontraron {{$plural}}</p>
          </div>
          <div class="icon">
            <i class="ion ion-alert-circled"></i>
          </div>
          <a href="#" class="small-box-footer none-button-add" item-type="{{$type}}">
            Agregar {{$typename}} <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
	    @endif
    </div>
  </div>
</div>
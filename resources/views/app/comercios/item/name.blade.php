<span class="hide">{{$name}}</span>
<div class="products-list">
	<div class="product-img">
    <img src="{{$logo}}" alt="" class="img-circle" />
  </div>
	<div class="product-info">
    <a href="#" class="product-title" data-title="{{$name}}">{{$name}}</a>
     <span class="product-description" data-description="{{$description}}">
       {{$description}}
     </span>
  </div>
</div>
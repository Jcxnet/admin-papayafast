<?php

namespace App\Http\Transformers;

use App\Comercio;
use League\Fractal\TransformerAbstract;

class ComercioApikeyTransformer extends TransformerAbstract{

  public function transform(Comercio $comercio){

		return  [
			'id'	 				=> $comercio->uuid,
      'name'  			=> $comercio->name,
      'description' => $comercio->description,
      'email'				=> $comercio->email,
      'phone'				=> $comercio->phone,
      'ruc'					=> $comercio->ruc,
      'apikey'			=> $this->getApikey($comercio),
      'logo'				=> $this->comercioImage($comercio->uuid),
		];

	}

	private function getApikey(Comercio $comercio){
    if(is_null($comercio->apikey))
       return ['version'=>NULL,'name'=>NULL,'token'=>NULL,'processors'=>['orders'=>NULL,'payments'=>NULL],'suspended'=>true];
    return $comercio->apikey->toArray();
  }

  private function comercioImage($id){
		$logo = \App\Http\Controllers\Controller::getImageItem($id,'LOGO','logos');
    if($logo['exists'] == true)
    	return $logo['data']['images']['conversions']['img300x300']['url'];
    return asset('img/icon/comercio.png');
	}

}

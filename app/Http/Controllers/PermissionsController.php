<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Role;
use App\Permission;
use Validator;

use DB;

class PermissionsController extends Controller
{
    private  $roles_default = [
    	['name'=>'admin', 		'display_name' =>'Admin', 		'description' => 'Administrador del Sistema'],
    	['name'=>'comercio', 	'display_name' =>'Comercio', 	'description' => 'Administrador de un comercio'],
    	['name'=>'editor', 		'display_name' =>'Editor', 		'description' => 'Editor de un comercio'],
			['name'=>'owner', 		'display_name' =>'Owner', 		'description' => 'Dueño de un comercio'],
    ];

    private $permissions_default = [
    	/*Api keys*/
    	['action'=>'API keys','name'=>'list-apikey', 'display_name' =>'Lista de API keys', 		'description' => 'Lista de las API keys de los Comercios', 	'roles'=>['admin'], 'locked' => true],
    	['action'=>'API keys','name'=>'generate-apikey', 'display_name' =>'Generar API key', 		'description' => 'Generar claves API para un comercio', 	'roles'=>['admin'], 'locked' => true],
    	['action'=>'API keys','name'=>'deactivate-apikey','display_name' =>'Desactivar API key', 	'description' => 'Desactiva la clave API de un comercio', 'roles'=>['admin'], 'locked' => true],
    	['action'=>'API keys','name'=>'activate-apikey',	'display_name' =>'Activar API key', 		'description' => 'Activa la clave API de un comercio', 		'roles'=>['admin'], 'locked' => true],
    	/*Comercios*/
    	['action'=>'Comercios','name'=>'list-comercio',			'display_name' =>'Listado de Comercios',	'description' => 'Muestra la lista de comercios', 		'roles'=>['admin','comercio','owner'], 'locked' => false],
    	['action'=>'Comercios','name'=>'add-comercio',			'display_name' =>'Crea un Comercio', 			'description' => 'Agrega un Comercio al sistema', 		'roles'=>['admin'], 'locked' => false],
    	['action'=>'Comercios','name'=>'edit-comercio',			'display_name' =>'Edita un Comercio', 		'description' => 'Editar los datos de un Comercio',		'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Comercios','name'=>'delete-comercio',		'display_name' =>'Elimina un Comercio',		'description' => 'Elimina un Comercio del sistema',		'roles'=>['admin'], 'locked' => false],
    	['action'=>'Comercios','name'=>'locals-comercio',		'display_name' =>'Locales de un Comercio','description' => 'Lista de Locales de un Comercio',		'roles'=>['admin','comercio','owner', 'editor'], 'locked' => false],
    	/*Editor*/
    	['action'=>'Editor','name'=>'editor-menu',				'display_name' =>'Editar un Menú',				'description' => 'Edita el Menú de un Comercio',			'roles'=>['admin','comercio','editor'], 'locked' => false],
    	/*Locales*/
    	['action'=>'Locales','name'=>'list-local',				'display_name' =>'Listado de Locales',		'description' => 'Lista los locales de un Comercio',	'roles'=>['admin','comercio','editor','owner'], 'locked' => false],
    	['action'=>'Locales','name'=>'format-local',			'display_name' =>'Formato de Locales',		'description' => 'Formato de lista de Locales',	'roles'=>['admin','comercio','editor','owner'], 'locked' => false],
    	['action'=>'Locales','name'=>'info-local',				'display_name' =>'Información de Local',	'description' => 'Retorna Información de un Local',	'roles'=>['admin','comercio','editor','owner'], 'locked' => false],
    	['action'=>'Locales','name'=>'add-local',					'display_name' =>'Agregar un Local',			'description' => 'Agrega un Local para un Comercio',	'roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Locales','name'=>'edit-local',				'display_name' =>'Editar un Local',				'description' => 'Edita los datos de un Local',				'roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Locales','name'=>'delete-local',			'display_name' =>'Elimina un Local',			'description' => 'Elimina un Local de un Comercio',		'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Locales','name'=>'deactivate-local',	'display_name' =>'Desactivar un Local',		'description' => 'Pausa las ventas de un Local',			'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Locales','name'=>'activate-local',		'display_name' =>'Activar un Local',			'description' => 'Activa las ventas de un Local',			'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Locales','name'=>'menu-local',				'display_name' =>'Asignar un Menú',				'description' => 'Asigna un Menú para un Local',	'roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Locales','name'=>'nomenu-local',				'display_name' =>'Retirar un Menú',				'description' => 'Elimina un Menú para un Local',	'roles'=>['admin','comercio','editor'], 'locked' => false],
    	/*Menus*/
    	['action'=>'Menús','name'=>'list-menu',					'display_name' =>'Listado de Menús',		'description' => 'Lista los Menús de un Comercio',	'roles'=>['admin','comercio','editor', 'owner'], 'locked' => false],
    	['action'=>'Menús','name'=>'add-menu',					'display_name' =>'Agregar un Menú',			'description' => 'Agrega un Menú para un Comercio',	'roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Menús','name'=>'edit-menu',					'display_name' =>'Editar un Menú',			'description' => 'Edita los datos de un Menú',			'roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Menús','name'=>'clone-menu',				'display_name' =>'Duplica un Menú',			'description' => 'Duplica un Menú para un Comercio','roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Menús','name'=>'delete-menu',				'display_name' =>'Eliminar un Menú',		'description' => 'Elimina un Menú de un Comercio',	'roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Menús','name'=>'preview-menu',			'display_name' =>'Visualiza un Menú',		'description' => 'Viat previa de un Menú',					'roles'=>['admin','comercio','editor','owner'], 'locked' => false],
    	['action'=>'Menús','name'=>'addlocal-menu',			'display_name' =>'Agregar un Local',		'description' => 'Agrega un Local que usará el Menú',	'roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Menús','name'=>'deletelocal-menu',	'display_name' =>'Elimina un Local',		'description' => 'Elimina un Local que usaba un Menú','roles'=>['admin','comercio','editor'], 'locked' => false],
    	['action'=>'Menús','name'=>'deactivate-menu',		'display_name' =>'Desactivar un Menú',	'description' => 'Desactiva un Menú ',								'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Menús','name'=>'activate-menu',			'display_name' =>'Activar un Menú',			'description' => 'Activa un Menú ',										'roles'=>['admin','comercio'], 'locked' => false],
    	/*Roles*/
    	['action'=>'Roles','name'=>'list-rol', 					'display_name' =>'Listado de Roles',  	'description' => 'Lista los Roles del sistema', 		'roles'=>['admin'], 'locked' => true],
    	['action'=>'Roles','name'=>'add-rol', 					'display_name' =>'Agregar un Rol', 			'description' => 'Agrega un nuevo Rol al sistema', 	'roles'=>['admin'], 'locked' => true],
    	['action'=>'Roles','name'=>'edit-rol', 					'display_name' =>'Editar un Rol', 			'description' => 'Edita un Rol del sistema', 				'roles'=>['admin'], 'locked' => true],
    	['action'=>'Roles','name'=>'delete-rol',				'display_name' =>'Elimina un Rol', 			'description' => 'Eliminar un Rol del sistema',			'roles'=>['admin'], 'locked' => true],
    	/*Usuarios*/
    	['action'=>'Usuarios','name'=>'list-user',			'display_name' =>'Listado de usuarios',	'description' => 'Lista los Usuarios de un Comercio / Sistema',	'roles'=>['admin','comercio','owner'], 'locked' => false],
    	['action'=>'Usuarios','name'=>'add-user',				'display_name' =>'Agrega un Usuario',		'description' => 'Agrega un Usuario para el Comercio / Sistema','roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Usuarios','name'=>'edit-user',			'display_name' =>'Editar un Usuario',		'description' => 'Edita los datos de un Usuario',								'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Usuarios','name'=>'delete-user',		'display_name' =>'Elimina un Usuario',	'description' => 'Elimina un Usuario del Comercio / Sistema',		'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Usuarios','name'=>'addrol-user',		'display_name' =>'Agregar un Rol al Usuario',	'description' => 'Asigna el Rol del Usuario en el Sistema',		'roles'=>['admin','comercio'], 'locked' => false],
    	['action'=>'Usuarios','name'=>'deleterol-user',	'display_name' =>'Retira el Rol del Usuario',	'description' => 'Retira el Rol del Usuario en el Sistema',		'roles'=>['admin','comercio'], 'locked' => false],
    	/*Permisos*/
	  	['action'=>'Permisos','name'=>'list-permission', 	'display_name' =>'Listado de Permisos', 'description' => 'Lista los Permisos del sistema', 'roles'=>['admin'], 'locked' => true],
	  	['action'=>'Permisos','name'=>'allow-permission', 'display_name' =>'Activar un Permiso', 	'description' => 'Activa un Permiso para un Rol',  'roles'=>['admin'], 'locked' => true],
	  	['action'=>'Permisos','name'=>'block-permission', 'display_name' =>'Bloquear un Permiso', 'description' => 'Bloquea un Permiso para un Rol', 'roles'=>['admin'], 'locked' => true],
	  	['action'=>'Permisos','name'=>'restore-permission',	'display_name' =>'Restaurar los Permisos','description' => 'Restaura los Permisos del Sistema',	'roles'=>['admin'], 'locked' => true],
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show all roles vs permissions in database
     * @return View
     */
    public function index(){
    	if(!$this->userhasRole('admin') || !$this->userhasPermission('list-permission'))
    		return $this->ajaxError('No tiene permiso para realizar esta acción');
      $data = $this->withRolesActions();
      return view('app.permissions.index',$data);
    }

    /**
     * extrae la información de los roles y los permisos asociados
     * @return array
     */
    private function withRolesActions(){
    	$roles = Role::all();
      $list = [];	$actions = [];
      foreach($roles as $role){
      	$permissions = $this->getCollection($role->perms()->get()->toArray(),'App\Permission','App\Http\Transformers\PermissionTransformer');
      	$permissions = collect($permissions)->keyBy('id')->toArray();
      	$list[] = ['role'=>$this->getItem($role,'App\Http\Transformers\RoleTransformer'), 'permissions'=> $permissions ];
      }
			$actions = $this->getCollection(Permission::all()->toArray(),'App\Permission','App\Http\Transformers\PermissionTransformer');
      $actions = collect($actions)->sortBy('action')->sortBy('displayname')->groupBy('action')->toArray();
      return ['roles'=>$list, 'actions' => $actions];
    }

    /**
     * Restaura los roles y permisos del sistema
     * @param  Request $request
     * @return json
     */
    public function restore(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasRole('admin') || !$this->userhasPermission('restore-permission'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		DB::table('permission_role')->delete();
    		DB::table('permissions')->delete();
  			$this->defaultRoles();
  			$this->defaultPermissions();
  			$view = view('app.permissions.table',$this->withRolesActions());
  			return $this->ajaxData(['content'=>$view->render()]);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * agrega el permiso de una acción para un rol
     * @param  Request $request
     * @return json
     */
    public function allow(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasRole('admin') || !$this->userhasPermission('allow-permission'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$rol = Role::where('uuid',$request->role)->first())
    			return $this->ajaxError('No se encontró el rol especificado');
    		if(!$permission = Permission::where('uuid',$request->action)->first())
    			return $this->ajaxError('No se encontró la acción especificada');
    		$rol->attachPermission($permission);
    		$view = view('app.permissions.buttonallow',['roleid'=>$rol->uuid, 'actionid'=>$permission->uuid]);
    		return $this->ajaxData(['content'=>$view->render()]);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * retira el permiso de una acción para un rol
     * @param  Request $request
     * @return json
     */
    public function block(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasRole('admin') || !$this->userhasPermission('block-permission'))
	    		return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$rol = Role::where('uuid',$request->role)->first())
    			return $this->ajaxError('No se encontró el rol especificado');
    		if(!$permission = Permission::where('uuid',$request->action)->first())
    			return $this->ajaxError('No se encontró la acción especificada');
    		$rol->detachPermission($permission);
    		$view = view('app.permissions.buttonblock',['roleid'=>$rol->uuid, 'actionid'=>$permission->uuid]);
    		return $this->ajaxData(['content'=>$view->render()]);
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}
    }

    /**
     * Crear los permisos por defecto si no existen en el sistema
     * @return void
     */
    private function defaultPermissions(){
    	foreach ($this->permissions_default as $default) {
    		if(!$permission = Permission::where('name',$default['name'])->first()){
    			$permission = new Permission();
		    	$permission->name         = $default['name'];
					$permission->display_name = $default['display_name'];
					$permission->description  = $default['description'];
					$permission->action  			= $default['action'];
					$permission->save();
    		}
    		$this->addPermissionRole($default['roles'],$permission);
    	}
    }

    /**
     * asigna el permiso a la lista de roles enviada
     * @param array     	$roles
     * @param Permission  $permission
     * @return void
     */
    private function addPermissionRole($roles, Permission $permission){
    	foreach ($roles as $rolename) {
    		if($rol = Role::where('name',$rolename)->first()){
    			$rol->attachPermission($permission);
    		}
    	}
    }

    /**
     * Crea los roles por defecto si no existen en la database
     * @return void
     */
    private function defaultRoles(){
    	foreach ($this->roles_default as $role) {
    		if(!$rol = Role::where('name',$role['name'])->first()){
    			$rol = new Role();
		    	$rol->name         = $role['name'];
					$rol->display_name = $role['display_name'];
					$rol->description  = $role['description'];
					$rol->save();
    		}
    	}
    }

}
<?php
	$type=strtolower($type);
	switch ($type) {
		case 'delivery':
			$class = 'label-success'; $title = 'Delivery';
			break;
		case 'takein':
			$class = 'label-info'; $title = 'Local';
			break;
		case 'takeout':
			$class = 'label-warning'; $title = 'Llevar';
			break;
		default:
			$class = 'label-default'; $title = 'No definido';
			break;
	}
?>
<span class="text-left clearfix">
	<h4>
		<span class="data-type label {{$class}} margin-r5" data-type="{{$type}}">{{$title}}</span>
		@if(!$active)
			<span class="label label-danger margin-r5">Inactivo</span>
		@endif
	</h4>
</span>
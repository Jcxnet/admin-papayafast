@extends('layouts.app')

@section('htmlheader_title')
	Editor de menús
@endsection

@section('main-content')
	<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" style="width: 100%;"><i class="fa fa-cubes"></i> Editor de menús - <b>@yield('menutitle','')</b>
          <b><a href="#" class="pull-right menu-preview" item-id="@yield('menuid','0')"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;Vista previa</a></b></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
	         <div class="row">
					 	<div class="col-md-3" id="editor-sidebar">
					 		@yield('editor-sidebar')
					 	</div>
					  <div class="col-md-9" id="editor-content">
					    @yield('editor-content')
					  </div>
					</div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
@endsection

@push('pagescript')
	<script type="text/javascript" src="{{asset('js/sortable.js')}}"></script>
	<script type="text/javascript" src="{{asset('plugins/cleave/cleave.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
@endpush

@push('pagecss')
	<link rel="stylesheet" href="{{asset('plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css')}}" type="text/css"/>
	<link rel="stylesheet" href="{{asset('css/editor.css')}}" type="text/css"/>
@endpush
<div class="form-group">
  <label for="txtreservaemail" class="col-sm-2 control-label">Reservas</label>
  <div class="col-sm-5">
    <div class="input-group">
      <span class="input-group-addon">@</span>
      <input type="text" id="txtreservasemail" class="form-control" placeholder="Email">
    </div>
  </div>
  <div class="col-sm-5">
    <div class="input-group">
      <span class="input-group-addon">
      	<i class="fa fa-phone"></i>
      </span>
      <input type="text" id="txtreservasphone" class="form-control" placeholder="Teléfono">
    </div>
  </div>
</div>
<div class="form-group">
  <label for="txtdeliveryemail" class="col-sm-2 control-label">Delivery</label>
  <div class="col-sm-5">
    <div class="input-group">
      <span class="input-group-addon">@</span>
      <input type="text" id="txtdeliveryemail" class="form-control" placeholder="Email">
    </div>
  </div>
  <div class="col-sm-5">
    <div class="input-group">
      <span class="input-group-addon">
      	<i class="fa fa-phone"></i>
      </span>
      <input type="text" id="txtdeliveryphone" class="form-control" placeholder="Teléfono">
    </div>
  </div>
</div>

@push('pagescript')
<script type="text/javascript">
	function dataContactLocal(){
		var data = {
			reserva : { email:  $('#txtreservasemail').val(), phone:  $('#txtreservasphone').val()},
			delivery: { email:  $('#txtdeliveryemail').val(), phone:  $('#txtdeliveryphone').val()}
		};
		return data;
	}
</script>
@endpush
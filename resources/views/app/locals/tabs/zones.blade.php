<div class="row">
	<div class="col-sm-12">
  	<div id="gmap_zone"></div>
  	@include ('app.maps.zone',['divmap'=>'#gmap_zone','zonevar'=>'mapzone'])
	</div>
	<div class="nav-tabs-custom">
	  <ul class="nav nav-tabs">
	    <li class="active"><a href="#zone_1" data-toggle="tab" aria-expanded="true"><span class="text-success">Zona verde</span></a></li>
	    <li class=""><a href="#zone_2" data-toggle="tab" aria-expanded="false"><span class="text-warning">Zona naranja</span></a></li>
	    <li class=""><a href="#zone_3" data-toggle="tab" aria-expanded="false"><span class="text-danger">Zona roja</span></a></li>
	  </ul>
	  <div class="tab-content">
	    <div class="tab-pane active" id="zone_1">
	    	<div class="form-group">
	    		<div class="col-sm-12">
	    			<label for="zone-1-points">Coordenadas</label>
            <textarea name="zone-1-points" id="zone-1-points" class="form-control" rows="3" placeholder=""></textarea>
            <button type="button" class="btn btn-success clearfix margin btn-show-zone" data-zone="zone-1-points" data-line="#4E9E19" data-color="#6ED927" data-id="0"><i class="fa fa-flag-o" aria-hidden="true"></i>&nbsp;Mostrar / Ocultar</button>
	    		</div>
	      </div>
	    </div>
	    <div class="tab-pane" id="zone_2">
	    	<div class="form-group">
	    		<div class="col-sm-12">
	    			<label for="zone-2-points">Coordenadas</label>
            <textarea name="zone-2-points" id="zone-2-points" class="form-control" rows="3" placeholder=""></textarea>
            <button type="button" class="btn btn-warning clearfix margin btn-show-zone" data-zone="zone-2-points" data-line="#E68920" data-color="#F5AB56" data-id="1"><i class="fa fa-flag-o" aria-hidden="true"></i>&nbsp;Mostrar / Ocultar</button>
	    		</div>
		    </div>
	    </div>
	    <div class="tab-pane" id="zone_3">
	    	<div class="form-group">
	    		<div class="col-sm-12">
	    			<label for="zone-3-points">Coordenadas</label>
            <textarea name="zone-3-points" id="zone-3-points" class="form-control" rows="3" placeholder=""></textarea>
            <button type="button" class="btn btn-danger clearfix margin btn-show-zone" data-zone="zone-3-points" data-line="#A82525" data-color="#FF4747" data-id="2"><i class="fa fa-flag-o" aria-hidden="true"></i>&nbsp;Mostrar / Ocultar</button>
	    		</div>
		    </div>
	    </div>
	  </div>
	</div>
</div>

@push('pagescript')
	<script type="text/javascript">
		$('.btn-show-zone').on('click',function(){
			var zone_data 	= $(this).attr('data-zone');
			var zone_line 	= $(this).attr('data-line');
			var zone_color 	= $(this).attr('data-color');
			var zone_id 		= $(this).attr('data-id');
			drawZoneMap($('#'+zone_data).val(),zone_line, zone_color, zone_id);
		});

		function dataZonesLocal(){
			var zones =['green','orange','red'];
			var data = [];
			for(zone=1;zone<=3;zone++){
				data.push({ zone:zones[zone-1], points:$('#zone-'+zone+'-points').val()});
			}
			return data;
		}

	</script>
@endpush
<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Role;
use App\Permission;
use App\User;
use App\Comercio;

use Validator;

use Mail;
use Illuminate\Support\Facades\Password;

class RoleusersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware(['auth']);
    }

    /**
     * Show all users in database
     * @return View
     */
    public function index(){
    	if(!$this->userhasPermission('list-user'))
    		return $this->ajaxError('No tiene permiso para realizar esta acción');
    	if($this->userhasRole('admin'))
    		$users = User::get();
    	else{
    		$users = User::where('comercio_id',$this->userComercio())->where('comercio_id','<>','0')->get();
    	}
      $list = [];
      foreach($users as $user){
      	$permissions  = [];
      	$roleuser 		= [];
      	if($role = User::find($user->id)->roles->first()){
      		$permissions  = $this->getCollection($role->perms()->get()->toArray(),'App\Permission','App\Http\Transformers\PermissionTransformer');
      		$roleuser 		= $this->getItem($role,'App\Http\Transformers\RoleTransformer');
      		$avatar 			= $roleuser['avatar'];
      	}
      	$user = $this->getItem($user,'App\Http\Transformers\UserTransformer');
      	$user['avatar'] = ($avatar)?$avatar:'';
      	$list[] = [
      		'user' 				=> 	$user,
      		'roles'				=>	$roleuser,
      		'permissions'	=>	collect($permissions)->sortBy('action')->sortBy('displayname')->groupBy('action')->toArray()
      	];
      }
      $me = \Auth::user();
      if($this->userhasRole('admin')){
      	$comercios = Comercio:: orderBy('name')->get()->toArray();
      	$roles =$this->getCollection(Role::orderBy('name')->get()->toArray(),'App\Role','App\Http\Transformers\RoleTransformer');
      }
     	else{
     		$comercios = Comercio:: orderBy('name')->where('id',$this->userComercio())->get()->toArray();
     		$roles =$this->getCollection(Role::orderBy('name')->where('name','<>','admin')->get()->toArray(),'App\Role','App\Http\Transformers\RoleTransformer');
     	}
      return view('app.rolesuser.index',['users'=>$list,'roles' => $roles, 'comercios' =>$comercios,'me'=>$me->uuid]);
    }

    /**
     * Agrega / Edita un usuario al sistema
     * @param Request $request
     * @return json
     */
    public function add(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('add-user') && !$this->userhasPermission('edit-user'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		$data = $request->only(['name','email','password','sendmail']);
    		$id = isset($request->id)?$request->id:false;
    		if($id){
	    		if(!$id = User::select('id')->where('uuid',$id)->first())
	    			return $this->ajaxError('No se pudo encontrar el usuario');
	    		$id = $id->id;
	    	}
    		if($id){
    			$validator = Validator::make($data, [
			      'name'  => 'required|min:4|max:255',
        		'email' => 'required|email|max:255|unique:adminusers,email,'.$id,
			    ]);
    		}else{
    			$validator = Validator::make($data, [
			      'name'  		=> 'required|min:4|max:255',
        		'email' 		=> 'required|email|max:255|unique:adminusers,email',
        		'password' 	=> 'required|min:10|max:20',
			    ]);
    		}
		    if($validator->fails()) {
		        $errors = implode('<br/>',$validator->errors()->all());
		        return $this->ajaxError($errors);
		    }
		    try{
		    	if(!$id){
		    		$user = new User;
		    		$user->password = bcrypt($data['password']);
		    		if(!$this->userhasRole('admin'))
		    			$user->comercio_id = $this->userComercio();
		    	}else{
		    		$user = User::find($id);
		    	}
		    	$user->name  = $data['name'];
					$user->email = $data['email'];
					$user->save();
					$user = User::find($user->id);
					if((boolean) $data['sendmail']){
						$this->sendMailUser($user);
					}
					$role = $user->roles->first();
					$user = $this->getItem($user,'App\Http\Transformers\UserTransformer');
					if(!$role || is_null($role)){
						$user['cols'] = $this->tableCols($user);
					}else{
						$user['cols'] = $this->tableCols	(
					      $user,
					      $this->getItem($role,'App\Http\Transformers\RoleTransformer'),
					      Permission::with($role->name)->get()->toArray());
					}
					return $this->ajaxData($user);
		    }catch(Exception $e){
		    	return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
		    }
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * envía un email con los detalles para acceder al sistema
     * @param  User $user
     * @return void
     */
    private function sendMailUser($user){
    	$token = Password::createToken($user);
    	$emailData = [
        'user' 		=> 	$user->toArray(),
        'url'	 		=>  url('password/reset/'.$token),
        'appName' => 	env('MAIL_SYSTEM_NAME'),
      ];

      Mail::send('app.emails.newuser', $emailData, function ($message) use ($user) {
          $message->from(env('MAIL_SYSTEM_EMAIL'),env('MAIL_SYSTEM_NAME'));
          $message->to($user['email'], $user['name']);
          $message->subject('Bienvenido!');
      });
    }

    /**
     * formatea la información del usuario para mostrar en la lista
     * @param  array  $user
     * @param  array  $role
     * @param  array  $permissions
     * @return json
     */
    private function tableCols($user,$role=[],$permissions=[]){
    	$cols =[];
    	$cols['user'] 				= view('app.rolesuser.item.user',				['user'=>$user])->render();
    	$cols['roles'] 				= view('app.rolesuser.item.role',				['role'=>$role, 'comercio'=>$user['comercio']])->render();
		  $cols['permissions'] 	= view('app.rolesuser.item.permissions',['permissions'=>$permissions])->render();
		  $cols['actions'] 			= view('app.rolesuser.item.actions',		['id'=>$user['id'],'me'=>0])->render();
    	return json_encode($cols);
    }

    /**
     * elimina un Usuario del sistema
     * @param  Request $request
     * @return json
     */
    public function delete(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('delete-user'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($id = $request->id){
    			if($this->userhasRole('admin'))
    				$user = User::where('uuid',$id)->first();
    			else
    				$user = User::where('uuid',$id)->where('comercio_id',$this->userComercio())->first();
    			if($user){
    				$user->name .= sprintf("[deleted %s]",date('Y-m-j H:i:s'));
    				$user->save();
    				$user->detachRoles($user->roles);
    				$user->delete();
    				if($user->deleted_at != NULL)
	    				return $this->ajaxData([],'ok','Usuario eliminado');
	    			else
	    				return $this->ajaxError('No se pudo eliminar el usuario');
    			}else{
    				return $this->ajaxError('No se encontró el usuario');
    			}
	   		}else{
    			return $this->ajaxError('Se necesita el identificador del usuario');
    		}
    	}else{
     		return $this->ajaxError('Acción no permitida');
     	}
    }

    /**
     * asigna un Rol a un Usuario / asigna un Comercio a un Usuario
     * @param  Request $request
     * @return json
     */
    public function assign(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('addrol-user'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if(!$user = User::where('uuid',$request->id)->first())
    			return $this->ajaxError('No se encontró el usuario');
    		if(!$role = Role::where('uuid',$request->role)->first())
    			return $this->ajaxError('No se encontró el Rol que se intentó asignar');
    		if($this->userhasRole('admin'))
    			$comercio = Comercio::where('uuid',$request->comercio)->first();
    		else{
    			$comercio = Comercio::where('id',$this->userComercio())->first();
    			if(!$comercio){
    				return $this->ajaxError('No tiene permiso para realizar esta acción');
    			}
    		}
    		if(!$comercio)
    			$comercio = false;
    		//$comercio = $comercio->id;
    		try{
    			if(!$user->hasRole($role->name)){
    				$user->detachRoles();
    				$user->attachRole($role);
    			}
    			$user->comercio_id = (!$comercio)?0:$comercio->id;
    			$user->save();
    			$user->fresh();
    			$user->comercio = $comercio;
    			$user = $this->getItem($user,'App\Http\Transformers\UserTransformer');
    			$permissions  = $this->getCollection($role->perms()->get()->toArray(),'App\Permission','App\Http\Transformers\PermissionTransformer');
    			$user['cols'] = $this->tableCols	(
					      $user,
					      $this->getItem($role,'App\Http\Transformers\RoleTransformer'),
					      collect($permissions)->sortBy('action')->sortBy('displayname')->groupBy('action')->toArray());
    			return $this->ajaxData($user,'ok','Rol asignado');
    		}catch(Exception $e){
    			return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
    		}
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}

    }

    /**
     * elimina el Rol de un Usuario
     * @param  Request $request
     * @return json
     */
    public function remove(Request $request){
    	if($request->ajax()){
    		if(!$this->userhasPermission('deleterol-user'))
    			return $this->ajaxError('No tiene permiso para realizar esta acción');
    		if($this->userhasRole('admin'))
    			$user = User::where('uuid',$request->id)->first();
    		else
    			$user = User::where('uuid',$request->id)->where('comercio_id',$this->userComercio())->first();
    		if(!$user)
    			return $this->ajaxError('No se encontró el usuario');
    		try{
   				$user->detachRoles();
    			$user = $this->getItem($user,'App\Http\Transformers\UserTransformer');
    			$user['cols'] = $this->tableCols	($user);
    			return $this->ajaxData($user,'ok','Rol cancelado');
    		}catch(Exception $e){
    			return $this->ajaxError(sprintf("code:%d, %s",$e->getCode(),$e->getMessage()));
    		}
    	}else{
     		return $this->ajaxError('Acción no permitida',[],401);
     	}

    }

}
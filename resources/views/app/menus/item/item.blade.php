<tr rowid="{{$local['id']}}">
  <td >
  	@include('app.locals.item.name',['local'=>$local])
  </td>
  <td>
  	@include('app.locals.item.address',['address'=>$local['address']])
  </td>
  <td>
  	@include('app.locals.item.contact',['contact'=>$local['contact']])
  </td>
  <td>
  	@include('app.locals.item.actions',['id'=>$local['id']])
  </td>
</tr>
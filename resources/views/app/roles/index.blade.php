@extends('layouts.app')

@section('htmlheader_title')
	Roles
@endsection


@section('main-content')
	 <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de roles</h3>
          @permission('add-rol')
          	<a href="#" id="btnAddRole" class="btn btn-primary btn-flat pull-right">Agregar Rol</a>
          @endpermission
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-striped ">
            <thead>
            <tr>
              <th>Rol</th>
              <th>Permisos</th>
              <th>Usuarios</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $role)
            	@include ('app.roles.item.item',['role'=>$role])
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>Rol</th>
              <th>Permisos</th>
              <th>Usuarios</th>
              <th></th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>


<div class="modal fade" tabindex="-1" role="dialog" id="frmAddRole" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-plus-square" aria-hidden="true"></i> Agregar Rol</h4>
      </div>
      <div class="modal-body">

				  <form class="form-horizontal" id="formRole" enctype="multipart/form-data" method="post">
				  	<input type="hidden" id="txtid" name="id" value="" />
			      <div class="form-group">
			        <label for="txtrole" class="col-sm-2 control-label">Rol</label>

			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtrole" name="display_name" placeholder="Nombre del rol">
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="txtdescription" class="col-sm-2 control-label">Descripción</label>

			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtdescription" name="description" placeholder="Descripción del rol">
			        </div>
			      </div>

			      <div class="form-group">
              <div class="col-sm-2">
              	<label for="image" class="control-label">Imagen</label>
              	<div class="products-list">
									<div class="product-img">
								    <img src="" alt="" id="roleimage" class="img-circle" />
								  </div>
								</div>
              </div>
              <div class="col-sm-10">
	              <input type="file" id="image" name="image">
	              <p class="help-block">Seleccione una imagen para el Rol.</p>
              </div>
            </div>

				  </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAddThisRole">Agregar Rol</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@push('pagescript')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>

  $(function () {

    initTable();
    actionButtons();
    formButtons();
    tableEvents();

  });

  function formButtons(){
  	$('#btnAddRole').on('click',function(e){
    	e.preventDefault();
    	clearForm();
    	$('#btnAddThisRole').html('Agregar Rol');
    	$('#frmAddRole').find('.modal-title').html('Agregar Rol');
    	$('#frmAddRole').removeClass('edit-role').addClass('new-role');
    	$('#frmAddRole').modal({backdrop:'static',show: true});
    	return false;
    });

    $('#btnAddThisRole').on('click',function(e){
    	e.preventDefault();
    	if( $('#frmAddRole').hasClass('new-role') )
    		addNewRole();
    	if( $('#frmAddRole').hasClass('edit-role') )
    		editRole();
    	return false;
    })
  }

  function initTable(){
  	$('#dataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "autoWidth": true,
      "responsive": {
        "details": false
    	},
      "columns": [
			    { "orderable": true  },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false }
			  ]
    });
  }

  function addNewRole(){

		var formData = new FormData(document.getElementById('formRole'));
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('roles.add') }}",
      data: formData, //{display_name:$('#txtrole').val(), description: $('#txtdescription').val(),image:imageData},
      cache: false,
      contentType: false,
			processData: false,
      beforeSend:function(){
        showAlert('Agregar rol','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Nuevo','Rol creado','success');
          							showToast('Nuevo','Rol creado','success')
          							$('#frmAddRole').modal('hide');
          							showRole(data.data.cols,data.data.id);
          							actionButtons();
          							clearForm();
          							if(data.data.image.exists == false)
          								showMessage('Aviso',data.data.image.data.message,'warning');
          							break;
          case 'error': showMessage('Error','Error al crear el rol:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar agregar el rol, intente nuevamente','warning');
      }
    });
  }

  function editRole(){
  	var formData = new FormData(document.getElementById('formRole'));
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('roles.edit') }}",
      data: formData, //{id: $('#txtid').val(),display_name:$('#txtrole').val(), description: $('#txtdescription').val()},
      cache: false,
      contentType: false,
			processData: false,
      beforeSend:function(){
        showAlert('Editar rol','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Editar','Rol editado','success');
          							showToast('Editar','Rol editado','success');
          							$('#frmAddRole').modal('hide');
          							clearForm();
          							updateRole(data.data);
          							if(data.data.image.exists == false)
          								showMessage('Aviso',data.data.image.data.message,'warning');
          							break;
          case 'error': showMessage('Error','Error al editar el rol:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar editar el rol, intente nuevamente','warning');
      }
    });
  }

  function showRole(cols,id){
  	cols = JSON.parse(cols);
  	var table = $('#dataTable').DataTable();
		var node  = table.row.add( [cols.role, cols.permissions, cols.users, cols.actions] ).node();
		table.column(0).data().sort().draw();
		$(node).attr('rowid',id);
  }

  function actionButtons(){
  	$('.btnDelete, .btnEdit').unbind();
  	$('.btnDelete').on('click',function(e){
  		e.preventDefault();
  		deleteRole($(this).attr('itemid'));
  		return false;
  	});
  	$('.btnEdit').on('click',function(e){
  		e.preventDefault();
  		clearForm();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		$('#frmAddRole').find('.modal-title').html('Editar Rol');
  		$('#btnAddThisRole').html('Editar Rol');
  		$('#txtid').val(id);
  		$('#txtrole').val(row.find('.product-title').attr('data-title'));
  		$('#txtdescription').val(row.find('.product-description').attr('data-description'));
  		$('#roleimage').attr('src',row.find('.img-circle').attr('src'));
    	$('#frmAddRole').removeClass('new-role').addClass('edit-role');
    	$('#frmAddRole').modal({backdrop:'static',show: true});
    	return false;
  	});
  }

  function clearForm(){
  	$('#txtrole,#txtdescription,#txtid,#image').val('');
  	$('#roleimage').attr('src','');
  }

  function tableEvents(){
  	$('#dataTable').on( 'order.dt', function () {
    actionButtons();
		});

		$('#dataTable').on( 'page.dt', function () {
	    actionButtons();
		});
  }

  function deleteRole(id){
  	var row = $("tr[rowid='"+id+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Eliminar Rol',
      message: '&iquest;Desea eliminar el rol <b>'+title+'</b>?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: 'fa fa-trash',
          label: 'Eliminar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            deleteRoleAction(id,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function deleteRoleAction(id,title,dialog,$button,row){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('roles.delete') }}",
	    data: {id: id},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							row.addClass('delete');
	        							var table = $('#dataTable').DataTable();
	        							table.row('.delete').remove().draw( false );
	        							//showMessage('Rol eliminado','Se eliminó el rol <b>'+title+'</b>','success');
	        							showToast('Rol eliminado','Se eliminó el rol <b>'+title+'</b>','success');
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al eliminar el rol:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar eliminar el rol, intente nuevamente','warning');
	    }
	  });
  }

  function updateRole(data){
  	var table = $('#dataTable').DataTable();
  	var row = $("tr[rowid='"+data.id+"']");
  	row.addClass('update');
  	table.row('.update').remove().draw( false );
  	showRole(data.cols,data.id);
  }

</script>
@endpush
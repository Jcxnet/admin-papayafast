@extends('layouts.app')

@section('htmlheader_title')
	Editor de menús
@endsection


@section('main-content')
	 <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Editor de menús</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        		<div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-exclamation"></i></span>
              <div class="info-box-content">
                <span class="info-box-number">Error</span>
                <span class="info-box-text">{{$message}}</span>
              </div><!-- /.info-box-content -->
            </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>


@endsection

<div class="row">
	<div class="col-md-5">
		<ul>
			<li>{{$group['title']}}</li>
		</ul>
	</div>
	<div class="col-md-7">
		<select id="select-group-{{$group['id']}}" class="multiselect-group" item-title="{{$group['title']}}">
		  @foreach($group['properties'] as $property)
		  	@if((float)$property['price']>0)
		  		<option>{{$property['title']}} +{{sprintf('%.2f',$property['price'])}}</option>
		  	@else
		  		<option>{{$property['title']}}</option>
		  	@endif
			@endforeach
		</select>
	</div>
</div>
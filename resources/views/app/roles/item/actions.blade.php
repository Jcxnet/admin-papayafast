<div class="btn-group text-center">
	@permission('edit-rol')
  	<button type="button" class="btn btn-info 	btn-flat btnEdit" itemid="{{$id}}"><i class="fa fa-pencil"></i></button>
  @endpermission
  @permission('delete-rol')
  	<button type="button" class="btn btn-danger btn-flat btnDelete" itemid="{{$id}}"><i class="fa fa-trash"></i></button>
  @endpermission
</div>
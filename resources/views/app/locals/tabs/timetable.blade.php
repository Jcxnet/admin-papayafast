<?php
	$days = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
?>
<div class="row">
@for($day=0;$day<7;$day++)
 	<div class="col-sm-6">
		<div class="box box-info box-solid">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{$days[$day]}}</h3>
	    </div>
	    <div class="box-body">
	      <div class="row">
	      	<div class="col-sm-6">
	      		<div class="input-group">
					    <input type="text" class="form-control timetable-day-open" id="day-{{$day}}-open" name="day-{{$day}}-open" data-day="{{$day}}" value="12:00">
					    <span class="input-group-addon">
					    	<span class="glyphicon glyphicon-time"></span>
					    </span>
						</div>
	      	</div>
	      	<div class="col-sm-6">
	      		<div class="input-group">
					    <input type="text" class="form-control timetable-day-close" id="day-{{$day}}-close" name="day-{{$day}}-close" data-day="{{$day}}" value="23:00">
					    <span class="input-group-addon">
					    	<span class="glyphicon glyphicon-time"></span>
					    </span>
						</div>
	      	</div>
	      </div>
	    </div>
	  </div>
  </div>
@endfor
</div>

@push('pagescript')
	<link rel="stylesheet" href="{{asset('plugins/timedropper/timedropper.min.css')}}">
	<script src="{{asset('plugins/timedropper/timedropper.min.js')}}"></script>
	<script type="text/javascript">

		function timetableInputs(){
			$('.timetable-day-open, .timetable-day-close').unbind();
			$('.timetable-day-open, .timetable-day-close').timeDropper({
				format: 'HH:mm',
				setCurrentTime: false,
			});
		}

		function dataTimetableLocal(){
			var data = [];
			for(day=0;day<7;day++){
				data.push({ day : day, open : $('#day-'+day+'-open').val(), close : $('#day-'+day+'-close').val() });
			}
			return data;
		}

	</script>
@endpush
<table class="table table-striped">
  <tr>
    <th style="width:300px;">&nbsp;</th>
    @foreach($roles as $role)
    	@include('app.permissions.role',['role' => $role['role']])
    @endforeach
  </tr>
  @if(count($actions)>0)
    @foreach($actions as $action => $actions)
    	@include('app.permissions.action',['action' => $action, 'actions' => $actions, 'roles' => $roles])
    @endforeach
  @else
  	<tr><td colspan="{{(count($roles)+1)}}"><span class="text-bold text-danger"><i class="fa fa-exclamation-triangle"></i>&nbsp;No se encontraron acciones</span></td></tr>
  @endif
</table>
@foreach($permissions as $action => $list)
	<i class="fa fa-circle-o text-light-blue pull-left"></i>&nbsp;<b>{{$action}}</b>
	<ul>
	<?php $pos=1; ?>
	@foreach($list as $permission)
		@if($pos == 1)
			<div class="row">
		@endif
		<div class="col-md-3">
			<li>{{$permission['displayname']}}</li>
		</div>
		<?php $pos++; ?>
		@if($pos>4)
			</div>
			<?php $pos=1;?>
		@endif
	@endforeach
	@if(count($list)%4 != 0)
		</div>
	@endif
	</ul>
@endforeach

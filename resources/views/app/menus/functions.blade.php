<script type="text/javascript">

	var preview = null;

  $(function (){

    initTable();
    actionSelect();
    //actionButtons();
    formButtons();
    tableEvents();

  });

	function initTable(){
  	$('#dataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "autoWidth": true,
      "responsive": {
        "details": false
    	},
      "columns": [
			    { "orderable": true  },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false }
			  ]
    });
  }

  function clearTable(){
  	var table = $('#dataTable').DataTable();
  	table.clear().draw();
  }

  function actionButtons(){
  	$('.btnLocalDelete, .btnDeleteAll, .btnClone, .btnEdit, .btnDelete, .btnActive, .btnPreview').unbind();
  	$('.btnLocalDelete').on('click',function(e){
  		e.preventDefault();
  		deleteLocalMenu($(this).attr('data-menu-id'),$(this).attr('data-local-id'),$(this).attr('data-local-button'));
  		return false;
  	});
  	$('.btnDeleteAll').on('click',function(e){
  		e.preventDefault();
  		deleteAllLocalMenu($(this).attr('data-menu-id'));
  		return false;
  	});
  	$('.btnClone').on('click',function(e){
  		e.preventDefault();
  		cloneMenu($(this).attr('itemid'));
  		return false;
  	});
  	$('.btnEdit').on('click',function(e){
  		e.preventDefault();
  		beforeEditMenu($(this).attr('itemid'));
    	return false;
  	});
  	$('.btnDelete').on('click', function(e){
  		e.preventDefault();
  		deleteMenu($(this).attr('itemid'));
    	return false;
  	});
  	$('.btnActive').on('click', function(e){
  		e.preventDefault();
  		activateMenu($(this).attr('itemid'),$(this).hasClass('menu-active'));
    	return false;
  	});
  	$('.btnPreview').on('click',function(e){
  		e.preventDefault();
  		previewMenu($(this).attr('itemid'));
  		return false;
  	});
  }

  function formButtons(){
  	$('#btnAddMenu').on('click',function(e){
    	e.preventDefault();
    	clearForm();
    	beforeAddMenu();
    	return false;
    });
    $('#btnAddThisMenu').on('click',function(e){
    	e.preventDefault();
    	if( $('#frmAddMenu').hasClass('new-menu') )
    		addNewMenu();
    	if( $('#frmAddMenu').hasClass('edit-menu') )
    		editMenu();
    	return false;
    });
  }
  function beforeAddMenu(id){
  	$('#lblcomercio').html($("#selectComercio option:selected").text());
  	$('#btnAddThisMenu').html('Agregar Menú');
  	$('#frmAddMenu').find('.modal-title').html('Agregar Menú');
  	$('#frmAddMenu').removeClass('edit-menu').addClass('new-menu');
  	$('#frmAddMenu').modal({backdrop:'static',show: true});
  }

  function beforeEditMenu(id){
		var row = $("tr[rowid='"+id+"']");
		var title = row.find('.product-title').attr('data-title');
		var type = row.find('.product-description').attr('data-type');
		$('#frmAddMenu').find('.modal-title').html('Editar menú');
		$('#btnAddThisMenu').html('Editar Menú');
		$('#txtid').val(id);
		$('#txtname').val(title);
		$('#selecttype').val(type);
		$('#lblcomercio').html($("#selectComercio option:selected").text());
  	$('#frmAddMenu').removeClass('new-menu').addClass('edit-menu');
  	$('#frmAddMenu').modal({backdrop:'static',show: true});
  }

  function actionSelect(){
  	$('#btnAddMenu').hide();
  	$('#selectComercio').on('change',function(){
  		var id = $('#selectComercio').val();
  		$('#txtcomercioid').val(id);
  		getMenus(id);
  	});
  }

  function addNewMenu(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('menus.add') }}",
      data: { id: $('#txtid').val(), name: $('#txtname').val(), type:$('#selecttype').val(), comercio: $('#txtcomercioid').val() },
      beforeSend:function(){
        showAlert('Agregar menú','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showToast('Nuevo','Menú creado','success');
          							$('#frmAddMenu').modal('hide');
          							showMenu(data.data.cols,data.data.id);
          							actionButtons();
          							multiSelects();
          							clearForm();
          							break;
          case 'error': showMessage('Error','Error al crear el menú:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar agregar el menú, intente nuevamente','warning');
      }
    });
  }

  function editMenu(){
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('menus.add') }}",
      data: { id: $('#txtid').val(), name: $('#txtname').val(), type:$('#selecttype').val(), comercio: $('#txtcomercioid').val() },
      beforeSend:function(){
        showAlert('Editar menú','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showToast('Editar','Menú editado','success');
          							$('#frmAddMenu').modal('hide');
          							updateMenu(data.data);
          							actionButtons();
          							multiSelects();
          							clearForm();
          							break;
          case 'error': showMessage('Error','Error al editar el menú:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar editar el menú, intente nuevamente','warning');
      }
    });
  }

  function getMenus(id){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('menus.comercio') }}",
      data: {id:id},
      beforeSend:function(){
        showAlert('Lista de menús','Enviando y obteniendo datos...','info');
        $('#btnAddMenu').hide();
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showMenus(data.data);
          							actionButtons();
          							multiSelects();
          							$('#btnAddMenu').show();
          							break;
          case 'error': showMessage('Error','Error al intentar obtener la lista de menús:<br><b>'+data.data.message+'</b>','danger');
          							clearTable();
          							$('#btnAddMenu').hide();
          							break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar obtener la lista de menús, intente nuevamente','warning');
      	clearTable();
      	$('#btnAddMenu').hide();
      }
    });
  }

  function showMenus(data){
  	$('#dataTable').DataTable().destroy();
  	$('#dataTable').DataTable( {
        "processing": true,
        "data": data,
        "paging": true,
	      "lengthChange": false,
	      "searching": false,
	      "ordering": true,
	      "autoWidth": true,
	      "responsive": {
	        "details": false
	    	},
        "columns": [
            { "data": "menu", 	 	"orderable": true  },
            { "data": "type", 		"orderable": false },
            { "data": "totals", 	"orderable": false },
            { "data": "locals", 	"orderable": false },
            { "data": "actions", 	"orderable": false },
        ],
        rowCallback: function ( row, data, index ) {
        	$(row).attr('rowid',data.id);
        }
    } );
  }

  function multiSelects(){
  	$('.multiselect-local').unbind();
  	$('.multiselect-local').multiselect({
  			 enableFiltering: true,
         includeSelectAllOption: true,
         selectAllText: 'Todos!',
	       buttonText: function(options, select) {
              return 'Agregar local';
          },
         buttonTitle: function(options, select) {
              var labels = [];
              options.each(function () {
                  labels.push($(this).text());
              });
              return labels.join(' - ');
          },
        onInitialized: function(select, container){
        	$(this).attr('data-menu-id',select.attr('data-menu-id'));
        	$(this).attr('data-select-id','select-local-menu-'+select.attr('data-menu-id'));
        },
        onChange: function(element, checked) {
              if (checked === true) {
                addLocalButton($(this).attr('data-menu-id'), element.val(), element.text());
              }
              else if (checked === false) {
                removeLocalButton($(this).attr('data-menu-id'), element.val());
              }
          },
        onSelectAll: function() {
        	var selected = [];
          $('#'+$(this).attr('data-select-id')+' option:selected').each(function() {
              selected.push($(this).val());
          });
          if(selected.length>0){
	            addLocalMenu($(this).attr('data-menu-id'),selected);
          }
        },
        /*onDeselectAll: function() {},*/
        onDropdownHidden: function(event) {
        	 var selected = [];
            $('#'+$(this).attr('data-select-id')+' option:selected').each(function() {
                selected.push($(this).val());
            });
            if(selected.length>0){
 	            addLocalMenu($(this).attr('data-menu-id'),selected);
            }
        }
	  });
  }


  function addLocalButton(menu, local, name){
  	var div 		= $('#locals-buttons-list-'+menu);
  	var button 	= '<button class="btn btn-primary btn-flat btn-xs margin-r5" id="btn-'+menu+'-'+local+'">'+name+' &nbsp; <i class="fa fa-times"></i></button> ';
  	div.append(button);
  }

  function removeLocalButton(menu, local){
  	var button 	= $('#btn-'+menu+'-'+local);
  	button.remove();
  }

 function deleteLocalMenu(menu, local, button){
  	var row = $("tr[rowid='"+menu+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Eliminar menú de Local',
      message: '&iquest;Desea eliminar el menú <b>'+title+'</b> del local <b>'+button+'</b>?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: 'fa fa-trash',
          label: 'Eliminar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            deleteLocalMenuAction(menu,local,button,title,dialog,this);
          }
        }
      ]
  	});
  }

  function deleteLocalMenuAction(menu,local,button,title,dialog,$button){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('menus.local.delete') }}",
	    data: {menu: menu, local:local},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							updateMenu(data.data);
	        							multiSelects();
	        							showToast('Eliminar menú de Local','Se eliminó el menú <b>'+title+'</b> del local <b>'+button+'</b>','success');
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al eliminar el menú del local:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar eliminar el menú del local, intente nuevamente','warning');
	    }
	  });
  }

  function deleteAllLocalMenu(menu){
		var row = $("tr[rowid='"+menu+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Eliminar menú de los Locales',
      message: '&iquest;Desea eliminar el menú <b>'+title+'</b> de todos los locales?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: 'fa fa-trash',
          label: 'Eliminar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            deleteAllLocalMenuAction(menu,title,dialog,this);
          }
        }
      ]
  	});
  }

  function deleteAllLocalMenuAction(menu,title,dialog,$button){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('menus.local.delete.all') }}",
	    data: {menu: menu},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							updateMenu(data.data);
	        							multiSelects();
	        							showToast('Eliminar menú de los Locales','Se eliminó el menú <b>'+title+'</b> de todos los locales','success');
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al eliminar el menú:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar eliminar el menú, intente nuevamente','warning');
	    }
	  });
  }

  function addLocalMenu(menu, locals){
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('menus.local.add') }}",
	    data: {menu: menu, locals:locals},
	    success:function(data){
	    	switch(data.meta.status){
	        case 'ok'		: updateMenu(data.data);
	        							multiSelects();
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al intentar agregar el menú a los locales:<br><b>'+data.data.message+'</b>','danger'); 
	        							break;
	        default : break;
	      }
	    },
	    error: function(){
	    	showMessage('Aviso','Se produjo un error al intentar el menú a los locales, intente nuevamente','warning');
	    }
	  });
  }

  function cloneMenu(id){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('menus.duplicate') }}",
      data: {id:id},
      beforeSend:function(){
        showAlert('Duplicar menú','Enviando y obteniendo datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showMenu(data.data.cols, data.data.id);
          							multiSelects();
          							break;
          case 'error': showMessage('Error','Ocurrió un error al intentar duplicar el menú:<br><b>'+data.data.message+'</b>','danger');
          							break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar duplicar el menú, intente nuevamente','warning');
      }
    });
  }


  function deleteMenu(menu){
		var row = $("tr[rowid='"+menu+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Eliminar menú',
      message: '&iquest;Desea eliminar el menú <b>'+title+'</b>?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: 'fa fa-trash',
          label: 'Eliminar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            deleteMenuAction(menu,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function deleteMenuAction(menu,title,dialog,$button,row){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('menus.delete') }}",
	    data: {id: menu},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							row.addClass('delete');
	        							var table = $('#dataTable').DataTable();
	        							table.row('.delete').remove().draw( false );
	        							showToast('Menú eliminado','Se eliminó el menú <b>'+title+'</b>','success');
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al eliminar el menú:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar eliminar el menú, intente nuevamente','warning');
	    }
	  });
  }

function activateMenu(menu,isactive){
		var row = $("tr[rowid='"+menu+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: ((isactive)?BootstrapDialog.TYPE_WARNING:BootstrapDialog.TYPE_SUCCESS),
      title: ((isactive)?'Desactivar':'Activar')+' menú',
      message: '&iquest;Desea '+((isactive)?'desactivar':'activar')+' el menú <b>'+title+'</b>?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: ((isactive)?'fa fa-pause':'fa fa-play'),
          label: ((isactive)?'Desactivar':'Activar'),
          cssClass: ((isactive)?'btn-warning':'btn-success'),
          action: function(dialog) {
            activateMenuAction(menu,title,dialog,this,row,isactive);
          }
        }
      ]
  	});
  }

  function activateMenuAction(menu,title,dialog,$button,row,isactive){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('menus.activate') }}",
	    data: {id: menu},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	closeAllDialogs();
	    	switch(data.meta.status){
	        case 'ok'		: showToast(((isactive)?'Desactivar menú':'Activar menú'),'Menú '+((isactive)?'desactivado':'activado'),'success');
          							$('#frmAddMenu').modal('hide');
          							updateMenu(data.data);
          							actionButtons();
          							multiSelects();
          							break;
	        case 'error': showMessage('Error','Ocurrió un error al '+((isactive)?'desactivar':'activar')+' el menú:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar '+((isactive)?'desactivar':'activar')+' el menú, intente nuevamente','warning');
	    }
	  });
  }

  function showMenu(cols,id){
  	cols = JSON.parse(cols);
  	var table = $('#dataTable').DataTable();
		var node  = table.row.add( {menu: cols.menu, type: cols.type, totals: cols.totals, locals: cols.locals, actions: cols.actions} ).node();
		table.column(0).data().sort().draw();
		$(node).attr('rowid',id);
  }

  function updateMenu(data){
  	var table = $('#dataTable').DataTable();
  	var row = $("tr[rowid='"+data.id+"']");
  	row.addClass('update');
  	table.row('.update').remove().draw( false );
  	showMenu(data.cols,data.id);
  }

  function tableEvents(){
  	$('#dataTable').on( 'order.dt', function () {
    	actionButtons();
		});

		$('#dataTable').on( 'page.dt', function () {
	    actionButtons();
		});
  }

  function clearForm(){
  	$('#txtid,#lblcomercio,#txtname,#selecttype').val('');
  }

</script>
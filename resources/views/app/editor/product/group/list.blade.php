
	<div class="box box-warning direct-chat direct-chat-primary" id="box-groups">
    <div class="box-header with-border">
      <h3 class="box-title">Grupos <span class="direct-chat-timestamp">(opciones para seleccionar en el producto)</span>
      &nbsp;<span data-toggle="tooltip" title="Total de elementos" id="list_groups_total" class="badge bg-orange">{{count($items)}}</span>
      </h3>
      <div class="box-tools pull-right">
      	<a class="btn btn-box-tool bg-orange" href="#" id="btn-add-group"><i class="fa fa-plus-square"></i></a>
      	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
    	<ul class="list-container direct-chat-messages container-border" id="list_groups_zone"></ul>

    	<div class="direct-chat-contacts" id="form_property_container">
    		<div class="margin">
    			@include('app.editor.product.group.form')
    		</div>
	    </div>

    </div>
    <div class="box-footer">
    </div>
  </div>

<script type="text/javascript">
	$(function(){
		$('#btn-add-group').on('click',function(){
			$(this).fadeOut(500);
    	$('#txtgroupid').val($('#list_groups_zone li').length+1);
    	$('#txtgrouporder').val(getNextItemOrder('item-group'));
    	showGroupForm();
    	sortGroupsList();
			return false;
		});
	});

	function sortGroupsList(){
		if($('#list_groups_zone').length){
			Sortable.create(list_groups_zone, {
	  			group: {
	  				name: 'list_origin_groups',
	  				put: false,
	  			},
	  			animation: 100,
	  			sort: true,
	  			dataIdAttr: 'sort-id',
	  			onSort: function (evt) {
	        	updateTotalContainer('#list_groups_zone','#list_groups_total','item-group');
	    		},
			});
		}
	}

</script>
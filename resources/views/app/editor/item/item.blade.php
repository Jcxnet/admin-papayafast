<?php
	$typename = isset($typename)?$typename:'elemento';
	$main 		= isset($main)?$main:false;
?>

<li class="list_origin_item item-menu item-{{$type}}" id="item-{{$type}}-{{$item['id']}}" sort-id="{{$type}}-{{$item['id']}}" item-type="{{$type}}" item-id="{{$item['id']}}">
	<span class="drag fa fa-arrows" aria-hidden="true"></span>
	<span class="pull-right item-menu-edit" title="Editar {{$typename}}" item-type="{{$type}}" item-id="{{$item['id']}}"><i class="fa fa-edit"></i></span>
	&nbsp;<span class="item-menu-title">{{$item['title']}}</span>
	@if($type=='page')
		<span class="pull-right page-menu-main" title="Establecer como página principal" item-type="{{$type}}" item-id="{{$item['id']}}"><i class="fa fa-dot-circle-o"></i></span>
	@endif
	@if(isset($item['code']))
		<br/>
		<small class="item-menu-spec text-black"><i class="fa fa-hashtag"></i><span class="item-code">{{$item['code']}}</span></small>
	@endif
	@if(isset($item['price']))
		<small class="item-menu-spec text-black"><i class="fa fa-dollar"></i><span class="item-price">{{sprintf('%.2f',$item['price'])}}</span></small>
	@endif
</li>
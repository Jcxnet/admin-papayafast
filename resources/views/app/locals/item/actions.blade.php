<div class="btn-group text-center">
  @permission('edit-local')
  	<button type="button" class="btn btn-info 	btn-flat btnEdit" title="Editar" itemid="{{$id}}"><i class="fa fa-pencil"></i></button>
  @endpermission
  @permission('delete-local')
  	<button type="button" class="btn btn-danger btn-flat btnDelete" title="Eliminar" itemid="{{$id}}"><i class="fa fa-trash"></i></button>
  @endpermission
  @permission('deactivate-local|activate-local')
	  @if($delivery)
	  	<button type="button" class="btn btn-success btn-flat btnDelivery" title="Suspender delivery" itemid="{{$id}}"><i class="fa fa-truck fa-flip-horizontal"></i></button>
	  @else
	  	<button type="button" class="btn btn-danger btn-flat btnDelivery" title="Activar delivery" itemid="{{$id}}"><i class="fa fa-truck "></i></button>
	  @endif
	@endpermission
</div>
	<script type="text/javascript">

		$(function(){
			formButton();
		});

		function getPageData(){
			var data = {
				id 		: $('#txtid').val(),
				title : $('#txttitle').val(),
				image : [],
				buttons : getContainerType('#list_add_zone','button'),
				products: getContainerType('#list_add_zone','product'),
				order   : getContainerItems('#list_add_zone'),
			};
			return JSON.stringify(data);
		}

		function formButton(){
			$('#btnPageSubmit').unbind();
			$('#btnPageSubmit').on('click',function(e){
				e.preventDefault();
				validatePageForm();
				return false;
			});
		}

		function validatePageForm(){
			var dataForm = getPageData();
			$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('editor.page.add') }}",
	      data: dataForm,
	      beforeSend:function(){
	        overlayContent('#editor-content');
	      },
	      success:function(data){
	      	removeOverlay('#editor-content');
	        switch(data.meta.status){
	          case 'ok'		: savePage(dataForm);
	          							showToast('Página','los datos se actualizaron correctamente','success');
	          							saveMenuSystem();
	          							updateSidebar('list_origin_page','page',data.data.id, data.data.title, data.data.pageitem);
	          							break;
	          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
	          default : break;
	        }
	      },
	      error: function(){
	      	removeOverlay('#editor-content');
	      	showMessage('Aviso','Se produjo un error al intentar agregar el elemento, intente nuevamente','warning');
	      }
	    });
		}

		function savePage(data){
			var menu = getMenu();
			var page  = JSON.parse(data);

			menu['pages'][page.id] = page;
			//these values are updated after saveMenuSystem returns menu values
			/*menu['total']['page']  = Object.keys(menu['pages']).length;
			menu['max']['page']		 = getMaxId(menu['pages']);*/

			menu['buttons'] 	= reorderItems(menu['buttons'],  page.order, 'button');
			menu['products'] 	= reorderItems(menu['products'], page.order, 'product');

			editor('new',{'type':'page', 'id':page.id, 'saved':true, 'timestamp': getTimestamp()});

			saveMenu(menu);
		}

		function showPageValues(id){
			var pages = getMenuKey('pages');
			if(!Object.keys(pages).length){
				showMessage('Página','no se encontraron páginas, intente nuevamente','warning');
				return false;
			}
			if(pages[id] === undefined){
				showMessage('Página','la página no se encuentra, intente nuevamente','warning');
				return false;
			}
			showPageElements(pages[id]);
		}

		function showPageElements(page){
			setActualView('editpage');
			editor('new',{'type':'page', 'id':page.id, 'saved':false, 'timestamp': getTimestamp()});
			var itemButtons 	= getMenuItems('buttons',page.buttons);
			var itemProducts 	= getMenuItems('products',page.products);
			$('#txtid').val(page.id);
	    $('#txttitle').val(page.title);
	    addMenuItems(getPageItems(itemButtons,itemProducts));
		}

		function getPageItems(buttons, products){
			var list = [];
			for(i=0;i<buttons.length;i++){
				list.push({type:'button',id:buttons[i].id,title:buttons[i].title,order:buttons[i].order});
			}
			for(i=0;i<products.length;i++){
				list.push({type:'product',id:products[i].id,title:products[i].title,order:products[i].order});
			}
			return {items:list};
		}


	</script>
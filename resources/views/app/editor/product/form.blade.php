<div class="box box-primary box-solid">
  	<div class="box-header with-border bg-green">
      <h3 class="box-title"><i class="fa fa-tasks" aria-hidden="true"></i>&nbsp;Producto</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool remove-form" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>

		<div class="box-body">
  		<form class="form" enctype="multipart/form-data" id="formProduct">
  			<input type="hidden" id="txtid" name="txtid" />
        <div class="form-group">
          <label for="txttitle" class="control-label">Título</label>
          <input type="text" class="form-control" id="txttitle" placeholder="Título">
        </div>
        <div class="form-group">
        	<div class="row">
          	<div class="col-xs-6">
          		<div class="row">
          			<div class="col-xs-6">
          				<label for="txtcode" class="control-label pull-right">Código</label>
          			</div>
          			<div class="col-xs-6">
          				<input type="text" class="form-control" id="txtcode" placeholder="Código">
          			</div>
          		</div>
          	</div>
          	<div class="col-xs-6">
          		<div class="row">
          			<div class="col-xs-6">
          				<label for="txtprice" class="control-label pull-right">Precio</label>
          			</div>
          			<div class="col-xs-6">
          				<input type="text" class="form-control item-price" id="txtprice" placeholder="0.00" maxlength="10">
          			</div>
          		</div>
          	</div>
          </div>
        </div>
        <div class="form-group">
        	<div class="row">
          	<div class="col-xs-6">
          		<div class="row">
          			<div class="col-xs-6">
          				<label for="txtquantity" class="control-label pull-right">Cantidad</label>
          			</div>
          			<div class="col-xs-6">
          				<input type="text" class="form-control item-quantity" id="txtquantity" placeholder="1" maxlength="3" value="1">
          			</div>
          		</div>
          	</div>
          	<div class="col-xs-6">
          		<div class="row">
          			<div class="col-xs-6">
          				<label for="txtprice" class="control-label pull-right">Disponible</label>
          			</div>
          			<div class="col-xs-6">
          				<input type="checkbox" class="item-available" name="optavailable" id="optavailable" checked>
          			</div>
          		</div>
          	</div>
          </div>
        </div>

        <div class="form-group">
         <div class="row">
		  <div class="col-sm-3">
		  	<div class="pull-right">
		  	<label for="image" class="control-label">Imagen</label>
		  	<div class="products-list">
					<div class="product-img">
				    <img src="" alt="" id="productimage" class="img-circle" />
				  </div>
				</div>
			</div>
		  </div>
		  <div class="col-sm-9">
		    <div class="pull-left">
			    <input type="file" id="image" name="image">
			    <p class="help-block">Seleccione una imagen para el Producto.</p>
		    </div>
		  </div>
		  </div>
		</div>

        <div class="form-group">
          <label>Descripción</label>
          <textarea class="form-control" rows="3" placeholder="Descripción del producto" name="txtdescription" id="txtdescription"></textarea>
        </div>
        <div class="form-group">
          @include ('app.editor.product.property.list',['items'=>[]])
        </div>
        <div class="form-group">
          @include ('app.editor.product.group.list',['items'=>[]])
        </div>
      </form>
    </div>

    <div class="box-footer">
      <a class="btn btn-default remove-form">Cancelar</a>
      <a class="btn btn-primary pull-right" id="btnProductSubmit">Guardar cambios</a>
    </div><!-- /.box-footer -->


</div>

@include ('app.editor.product.functions')

	<script type="text/javascript">

		$(function(){
			formButton();
			inputFormat();
		});

		function getProductData(){
			/*var page = getContainerType('#list_add_zone','page');
			page = (page.length>0)?page[0]:null;
			var data = {
				id 			: $('#txtid').val(),
				title 		: $('#txttitle').val(),
				code  		: $('#txtcode').val(),
				price 		: ''+$('#txtprice').val(),
				cantidad	: $('#txtquantity').val(),
				available 	: $('#optavailable').is(":checked"),
				detail 		: $('#txtdescription').val(),
				properties 	: [],
				image 		: [],
				order 		: "1",
				groups 		: [],
				sizes 		: [],
				extras 		: []
			};
			return JSON.stringify(data);*/
			var formData = new FormData();
			formData.append('id', $('#txtid').val() );
			formData.append('title', $('#txttitle').val() );
			formData.append('code', $('#txtcode').val() );
			formData.append('price', ''+$('#txtprice').val() );
			formData.append('cantidad', $('#txtquantity').val() );
			formData.append('available', $('#optavailable').is(":checked")?1:0 );
			formData.append('detail', $('#txtdescription').val() );
			formData.append('properties', [] );
			formData.append('order', "1" );
			formData.append('groups', [] );
			formData.append('sizes', [] );
			formData.append('extras', [] );

			var file = document.getElementById("image").files[0]; //fetch file
			formData.append('image', file); //append file to formData object
			return formData;
		}

		function formButton(){
			$('#btnProductSubmit').unbind();
			$('#btnProductSubmit').on('click',function(e){
				e.preventDefault();
				if(validateGroups())
					validateProductForm();
				return false;
			});
		}

		function inputFormat(){
			$("[class='item-available']").bootstrapSwitch({size: 'small', onText: 'Sí', offText: 'No', onColor:'success', offColor:'danger'});
			var clvprice = new Cleave('.item-price', {
			    numeral: true,
			    numeralThousandsGroupStyle: 'thousand',
			    numeralDecimalScale: 2
			});
			var clvquantity = new Cleave('.item-quantity', {
			    numeral: true,
			    numeralThousandsGroupStyle: 'thousand',
			    numeralDecimalScale: 0
			});
		}

		function validateGroups(){
			var groups = $('#list_groups_zone').find('.item-group');
			var total  =  groups.length;
			if(total == 0)
				return true;
			for(i=0;i<total;i++){
				var totalgroup 	 = $(groups[i]).find('.item-property').length;
				var unavailables = $(groups[i]).find('.item-unavailable').length;
				var ngrupo 		 	 = $(groups[i]).attr('item-title');

				if (totalgroup < 2){
					showMessage('Aviso','el grupo <b>'+ngrupo+'</b> debe tener al menos dos propiedades','warning');
					return false;
				}
				if((totalgroup-unavailables)<2){
					showMessage('Aviso','el grupo <b>'+ngrupo+'</b> debe tener al menos dos propiedades disponibles','warning');
					return false;
				}
			}
			return true;
		}

		function validateProductForm(){
			var dataForm = getProductData();
			$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('editor.product.add') }}",
	      data: dataForm,
	      cache: false,
      	  contentType: false,
		  processData: false,
	      beforeSend:function(){
	        overlayContent('#editor-content');
	      },
	      success:function(data){
	      	removeOverlay('#editor-content');
	        switch(data.meta.status){
	          case 'ok'		: orderGroups(orderProperties());
	          				dataForm = data.data;
	          				dataForm = saveProperties(dataForm);
	          				saveProduct(dataForm);
	          				showToast('Producto','los datos se actualizaron correctamente','success');
	          				saveMenuSystem();
	          				updateSidebar('list_origin_product','product',data.data.id, data.data.title, data.data.productitem);
	          				break;
	          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
	          default : break;
	        }
	      },
	      error: function(){
	      	removeOverlay('#editor-content');
	      	showMessage('Aviso','Se produjo un error al intentar agregar el elemento, intente nuevamente','warning');
	      }
	    });
		}

		function orderProperties(){
			var properties = $('#list_properties_zone').find('.item-property');
			var total = $(properties).length;
			for(i=0;i<total;i++){
				$(properties[i]).attr('item-order',i+1);
			}
			return total+1;
		}

		function orderGroups(pos){
			var groups = $('#list_groups_zone').find('.item-group');
			var total = $(groups).length;
			for(i=0;i<total;i++){
				$(groups[i]).attr('item-order',i+1);
				var properties = $(groups[i]).find('.item-property');
				var max = properties.length;
				for(j=0;j<max;j++){
					$(properties[j]).attr('item-order',pos);
					pos++;
				}
			}
		}

		function newProperty(){
			return {id:0, title:'', price:0, code:'', order:'', cantidad:0, available:true, group:'', grouptitle:'', grouporder:''};
		}

		function saveProperties(dataForm){
			var properties = $('#list_properties_zone').find('.item-property');
			var total = $(properties).length;
			var list  = [];
			var menu  = getMenu();
			var nextId = getNextItemId('property');

			for(i=0;i<total;i++){
				var property 	 = newProperty();
				var itemId 		 = $(properties[i]).attr('item-id');
				property.title = $(properties[i]).attr('item-title');
				property.price = $(properties[i]).attr('item-price');
				property.code  = $(properties[i]).attr('item-code');
				property.order = ''+$(properties[i]).attr('item-order');
				property.cantidad  = $(properties[i]).attr('item-quantity');
				property.available = ($(properties[i]).attr('item-available')==1)?true:false;
				if(itemId == 0){
					property.id = nextId;
					itemId 			= nextId;
					$(properties[i]).attr('item-id',nextId);
					nextId++;
				}else{
					property.id = itemId;
				}
				menu['properties'][itemId] = property;
				list.push(itemId);
			}

			var groups = $('#list_groups_zone').find('.item-group');
			total = $(groups).length;
			for(i=0;i<total;i++){
				properties = $(groups[i]).find('.item-property');
				var max = properties.length;
				var title = $(groups[i]).attr('item-title');
				var order = $(groups[i]).attr('item-order');
				var group = $(groups[i]).attr('item-order');
				for(j=0;j<max;j++){
					var itemId 		 = $(properties[j]).attr('item-id');
					property 	 		 = newProperty();
					property.title = $(properties[j]).attr('item-title');
					property.price = $(properties[j]).attr('item-price');
					property.code  = $(properties[j]).attr('item-code');
					property.order = ''+$(properties[j]).attr('item-order');
					property.cantidad  	= $(properties[j]).attr('item-quantity');
					property.available 	= ($(properties[j]).attr('item-available')==1)?true:false;
					property.group 			= group;
					property.grouptitle = title;
					property.grouporder = order;
					if(itemId == 0){
						property.id = nextId;
						itemId 			= nextId;
						$(properties[j]).attr('item-id',nextId);
						nextId++;
					}else{
						property.id = itemId;
					}
					menu['properties'][itemId] = property;
					list.push(itemId);
				}
			}

			saveMenu(menu);

			//dataForm = JSON.parse(dataForm);
			if(list.length>0)
				dataForm.properties = list.filter(function(val) { return val !== null; });
			else
				dataForm.properties = [];
			return JSON.stringify(dataForm);
		}

		function saveProduct(data){
			var menu 		= getMenu();
			var product  	= JSON.parse(data);

			menu['products'][product.id] = product;
			editor('new',{'type':'product', 'id':product.id, 'saved':true, 'timestamp': getTimestamp()});
			saveMenu(menu);
		}


		function showProductValues(id){
			var products = getMenuKey('products');
			if(!Object.keys(products).length){
				showMessage('Producto','no se encontraron productos, intente nuevamente','warning');
				return false;
			}
			if(products[id] === undefined){
				showMessage('Producto','producto no no encontrado, intente nuevamente','warning');
				return false;
			}
			showProductElements(products[id]);
			orderGroups(orderProperties());
		}

		function showProductElements(product){
			setActualView('editproduct');
			editor('new',{'type':'product', 'id':product.id, 'saved':false, 'timestamp': getTimestamp()});
			$('#txtid').val(product.id);
	    $('#txttitle').val(product.title);
	    $('#txtcode').val(product.code);
	    $('#txtprice').val(product.price);
	    $('#txtquantity').val(product.cantidad);
	    $('#txtdescription').val(product.detail);
			if(product.available)
				$('#optavailable').attr('checked', true );
			else
				$('#optavailable').removeAttr('checked');
			$('#optavailable').bootstrapSwitch('state',product.available);

	    addListProperties('#list_properties_zone', product.properties);
	    addListGroups		 ('#list_groups_zone', product.groups);
		}

	function addListGroups(container, groups){
		if(Object.keys(groups).length == 0)
			return false;
		var list = [];
		for(var key in groups){
			var group 	= groups[key];
			group.type  = 'group';
			list.push(group);
		}
		addItemsToList(container, list).done( function(){
			for(i=0;i<list.length;i++){
				var order  = list[i].order;
				var container = '#list_group_'+order;
				var boxgroup = '#item-group-'+order;
				addListProperties(container, list[i].properties);
				addEditActionProductItem(boxgroup);
				addDeleteActionProductItem(boxgroup);
			}
			updateTotalContainer('#list_groups_zone','#list_groups_total','item-group');
			sortAllProductLists();
		});
	}

	function addListProperties(container, properties){
		if(Object.keys(properties).length == 0)
			return false;
		var list = [];
		for(var key in properties){
			var property 	= properties[key];
			property.type = 'property';
			list.push(property);
		}
		addItemsToList(container, list).done(function(){
			addEditActionProductItem(container);
			addDeleteActionProductItem(container);
		});
	}

	function addItemsToList(container, list){
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    return $.ajax({
      method: "POST",
      url: "{{ route('editor.items.order') }}",
      data: JSON.stringify({items:list}),
      beforeSend: function(){
      	overlayContent('#editor-content');
      },
      success:function(data){
      	removeOverlay('#editor-content');
        switch(data.meta.status){
          case 'ok'		: $(container).html(data.data.content);
          							showToast('Editor','se creó la lista de elementos','success');
          							updateTotalContainer('#list_properties_zone','#list_properties_total');
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-content');
      	showMessage('Aviso','Se produjo un error al intentar crear la lista de elementos, intente nuevamente','warning');
      }
    });
	}

	function getLastOrder(list){
		var ids=[];
		$(list+' li').each(function(i){
		   ids.push(parseInt($(this).attr('item-order')));
		});
		if(ids.length>0)
			return Math.max.apply( null, ids );
		return 0;
	}

	function updateProductList(list, type, order, title, html){
		var item = '#item-'+type+'-'+order;

		overlayContent('#editor-content');

		if(!$(item).length)
			$('#'+list).append(html);
		else
			updateItem(item,type,html);

		addEditActionProductItem(item);
		addDeleteActionProductItem(item);

		removeOverlay('#editor-content');
	}

	function updateItem(item, type, html){
		var container = $(item).parent();
		if(!container.length){
			showMessage('Aviso','no se pudo actualizar el elemento, intente nuevamente','warning');
			return false;
		}
		var itemid = $(item).attr('id');
		var lastid = 'remove-'+type+'-0';
		$(item).after(html);
		$(item).attr('id', lastid);
		if(type=='group'){
			$('#'+lastid).find('li').appendTo($('#'+itemid).find('.list-container'));
		}
		$('#'+lastid).addClass('removed-item').remove();
	}

	function addEditActionProductItem(item){
		var link = $(item).find('.item-menu-edit');
		if(link.length){
			$(link).unbind();
			$(link).on('click',function(e){
				e.preventDefault();
				editProductItem(this);
				return false;
			});
		}
	}

	function addDeleteActionProductItem(item){
		var link = $(item).find('.item-menu-delete');
		if(link.length){
			$(link).unbind();
			$(link).on('click',function(e){
				e.preventDefault();
				deleteProductItem(this);
				return false;
			});
		}
	}

	function editProductItem(item){
		var type 	= $(item).attr('item-type');
		var order	= $(item).attr('item-order');
		console.log('edit item type->'+type+' order->'+order);
		switch(type){
			case 'property' : editProperty(order); break;
			case 'group'    : editGroup(order); break;
			default: showMessage('Aviso', 'el elemento seleccionado no es editable', 'warning');
		}
	}

	function deleteProductItem(item){
		var type 	= $(item).attr('item-type');
		var order	= $(item).attr('item-order');
		console.log('delete item type->'+type+' order->'+order);
		switch(type){
			case 'property' : deleteProperty(order); break;
			case 'group'    : deleteGroup(order); break;
			default: showMessage('Aviso', 'el elemento seleccionado no es removible', 'warning');
		}
	}

	function sortAllProductLists(){
		var lists = getAllListSources();
		var names = getAllListNames(lists,-1,'list_origin_properties');
		for(index=0;index<lists.length;index++){
			var lstnames = getAllListNames(lists,index,'list_origin_properties');
			var item 		 = $('#list_group_'+lists[index].order);
			var countList = '#list_group_'+lists[index].order;
			var countSpan = '#list_group_'+lists[index].order+'_total';
			var lst = Sortable.create( item[0], {
  			group : {
  				name : lists[index].name,
  				put  : lstnames,
  			},
  			animation : 100,
  			sort : true,
  			dataIdAttr : 'sort-id',
			});
		}
		Sortable.create(list_properties_zone, {
  			group: {
  				name: 'list_origin_properties',
  				put:  getAllListNames(lists,-1,false),
  			},
  			animation: 100,
  			sort: true,
  			dataIdAttr: 'sort-id',
  			onAdd: function (evt) {
        	updateTotalContainer('#list_properties_zone','#list_properties_total');
    		},
    		onRemove: function (evt) {
        	updateTotalContainer('#list_properties_zone','#list_properties_total');
    		},
		});
	}

	function getAllListNames(list, except, add){
		var names = [];
		if(add)
			names.push(add);
		for(i=0;i<list.length;i++){
			if(i!=except)
				names.push(list[i].name);
		}
		return names;
	}

	function getAllListSources(){
		var txt = [];
		console.log('buscando containers en la lista de grupos')
		$('#list_groups_zone li[class="item-group"]').each(function(index){
			container = $(this).find('.list-container');
			console.log('index->',index,' container->',container, ' id->',$(container).attr('id'));
			if(container.length){
				listid = $(container).attr('id');
				order  = $(this).attr('item-order');
				txt.push({id:listid, order: order, name: listid});
			}
		});
		return txt;
	}

	</script>

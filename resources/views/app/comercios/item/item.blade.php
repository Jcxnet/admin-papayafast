<tr rowid="{{$comercio['id']}}">
  <td >
  	@include('app.comercios.item.name',['name'=>$comercio['name'],'description'=>$comercio['description'],'logo' =>$comercio['logo']])
  </td>
  <td>
  	@include('app.comercios.item.contact',['email'=>$comercio['email'],'phone'=>$comercio['phone']])
  </td>
  <td>
  	@include('app.comercios.item.ruc',['ruc'=>$comercio['ruc']])
  </td>
  <td>
  	@include('app.comercios.item.actions',['id'=>$comercio['id']])
  </td>
</tr>
<div class="modal fade" tabindex="-1" role="dialog" id="frmAddMenu">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-plus-square" aria-hidden="true"></i> Agregar Menú</h4>
      </div>
      <div class="modal-body">

      	<form class="form-horizontal">
      		<input type="hidden" id="txtid" value="" />
			  	<h4>Menú de <span id="lblcomercio" class="text-bold text-orange"></span></h4>

			  	<div class="form-group">
					  <label for="txtname" class="col-sm-2 control-label">Menú</label>
					  <div class="col-sm-10">
					    <input type="text" class="form-control" id="txtname" placeholder="Nombre del menú">
					  </div>
					</div>

					<div class="form-group">
					  <label for="selecttype" class="col-sm-2 control-label">Tipo de menú</label>
						<div class="col-sm-10">
							<select class="form-control" id="selecttype">
	              <option value=''>Selecciona el tipo de menú</option>
	              <option value="DELIVERY"> Delivery</option>
	              <option value="TAKEOUT"> Para llevar</option>
	              <option value="TAKEIN"> En local</option>
	            </select>
            </div>
          </div>

				</form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAddThisMenu">Agregar Menú</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
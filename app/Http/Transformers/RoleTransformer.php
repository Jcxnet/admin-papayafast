<?php

namespace App\Http\Transformers;

use App\Role;
use League\Fractal\TransformerAbstract;


class RoleTransformer extends TransformerAbstract{

	public function transform(Role $role){
		return [
			'id'	    		=> $role->uuid,
      'name'    		=> $role->name,
      'displayname'	=> $role->display_name,
      'description'	=> $role->description,
      'avatar'			=> $this->roleImage($role->uuid),
		];
	}

	private function roleImage($id){
		$avatar = \App\Http\Controllers\Controller::getImageItem($id,'ROLE','roles');
    if($avatar['exists'] == true)
    	return $avatar['data']['images']['conversions']['img200x200']['url'];
    return asset('img/avatar/avatar-15.jpg');
	}

}

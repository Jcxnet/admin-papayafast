@extends('layouts.app')

@section('htmlheader_title')
	Comercios
@endsection


@section('main-content')
	 <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de Comercios</h3>
          @permission('add-comercio')
          	<a href="#" id="btnAddComercio" class="btn btn-primary btn-flat pull-right">Agregar Comercio</a>
          @endpermission
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-striped ">
            <thead>
            <tr>
              <th>Comercio</th>
              <th>Contacto</th>
              <th>RUC</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($comercios as $comercio)
            	@include ('app.comercios.item.item',['comercio'=>$comercio])
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>Comercio</th>
              <th>Contacto</th>
              <th>RUC</th>
              <th></th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>


<div class="modal fade" tabindex="-1" role="dialog" id="frmAddComercio">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title text-blue"><i class="fa fa-plus-square" aria-hidden="true"></i> Agregar Comercio</h4>
      </div>
      <div class="modal-body">

				  <form class="form-horizontal" id="formComercio" enctype="multipart/form-data" method="post">
				  	<input type="hidden" id="txtid" name="id" value="" />
			      <div class="form-group">
			        <label for="txtname" class="col-sm-2 control-label">Comercio</label>
			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtname" name="name" placeholder="Nombre del comercio">
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="txtdescription" class="col-sm-2 control-label">Descripción</label>
			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtdescription" name="description" placeholder="Descripción del comercio">
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="txtemail" class="col-sm-2 control-label">Email</label>
			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtemail" name="email" placeholder="Email del comercio">
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="txtphone" class="col-sm-2 control-label">Teléfono</label>
			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtphone" name="phone" placeholder="Teléfono del comercio">
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="txtruc" class="col-sm-2 control-label">RUC</label>
			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtruc" name="ruc" placeholder="Número de RUC">
			        </div>
			      </div>

			      <div class="form-group">
              <div class="col-sm-2">
              	<label for="image" class="control-label">Logo</label>
              	<div class="products-list">
									<div class="product-img">
								    <img src="" alt="" id="logoimage" class="img-circle" />
								  </div>
								</div>
              </div>
              <div class="col-sm-10">
	              <input type="file" id="image" name="image">
	              <p class="help-block">Seleccione el logo para el Comercio.</p>
              </div>
            </div>

				  </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAddThisComercio">Agregar Comercio</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@push('pagescript')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>

  $(function () {

    initTable();
    actionButtons();
    formButtons();
    tableEvents();

  });

  function formButtons(){
  	$('#btnAddComercio').on('click',function(e){
    	e.preventDefault();
    	clearForm();
    	$('#btnAddThisComercio').html('Agregar Comercio');
    	$('#frmAddComercio').find('.modal-title').html('Agregar Comercio');
    	$('#frmAddComercio').removeClass('edit-comercio').addClass('new-comercio');
    	$('#frmAddComercio').modal({backdrop:'static',show: true});
    	return false;
    });

    $('#btnAddThisComercio').on('click',function(e){
    	e.preventDefault();
    	if( $('#frmAddComercio').hasClass('new-comercio') )
    		addNewComercio();
    	if( $('#frmAddComercio').hasClass('edit-comercio') )
    		editComercio();
    	return false;
    })
  }

  function initTable(){
  	$('#dataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "autoWidth": true,
      "responsive": {
        "details": false
    	},
      "columns": [
			    { "orderable": true  },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false }
			  ]
    });
  }

  function addNewComercio(){
  	var formData = new FormData(document.getElementById('formComercio'));
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('comercios.add') }}",
      data: formData,//{name:$('#txtname').val(), description: $('#txtdescription').val(),email: $('#txtemail').val(), phone: $('#txtphone').val(), ruc: $('#txtruc').val()},
      cache: false,
      contentType: false,
			processData: false,
      beforeSend:function(){
        showAlert('Agregar comercio','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Nuevo','Rol creado','success');
          							showToast('Nuevo','Comercio creado','success');
          							$('#frmAddComercio').modal('hide');
          							showComercio(data.data.cols,data.data.id);
          							actionButtons();
          							clearForm();
          							if(data.data.image.exists == false)
          								showMessage('Aviso',data.data.image.data.message,'warning');
          							break;
          case 'error': showMessage('Error','Error al crear el comercio:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar agregar el comercio, intente nuevamente','warning');
      }
    });
  }

  function editComercio(){
  	var formData = new FormData(document.getElementById('formComercio'));
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('comercios.edit') }}",
      data: formData,//{id:$('#txtid').val(),name:$('#txtname').val(), description: $('#txtdescription').val(),email: $('#txtemail').val(), phone: $('#txtphone').val(), ruc: $('#txtruc').val()},
      cache: false,
      contentType: false,
			processData: false,
      beforeSend:function(){
        showAlert('Editar comercio','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: //showMessage('Editar','Rol editado','success');
          							showToast('Editar','Comercio editado','success');
          							$('#frmAddComercio').modal('hide');
          							clearForm();
          							updateComercio(data.data);
          							if(data.data.image.exists == false)
          								showMessage('Aviso',data.data.image.data.message,'warning');
          							break;
          case 'error': showMessage('Error','Error al editar el comercio:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar editar el comercio, intente nuevamente','warning');
      }
    });
  }

  function showComercio(cols,id){
  	cols = JSON.parse(cols);
  	var table = $('#dataTable').DataTable();
		var node  = table.row.add( [cols.name, cols.contact, cols.ruc, cols.actions] ).node();
		table.column(0).data().sort().draw();
		$(node).attr('rowid',id);
  }

  function actionButtons(){
  	$('.btnDelete, .btnEdit').unbind();
  	$('.btnDelete').on('click',function(e){
  		e.preventDefault();
  		deleteComercio($(this).attr('itemid'));
  		return false;
  	});
  	$('.btnEdit').on('click',function(e){
  		e.preventDefault();
  		clearForm();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		$('#frmAddComercio').find('.modal-title').html('Editar Comercio');
  		$('#btnAddThisComercio').html('Editar Comercio');
  		$('#txtid').val(id);
  		$('#txtname').val(row.find('.product-title').attr('data-title'));
  		$('#txtdescription').val(row.find('.product-description').attr('data-description'));
  		$('#txtemail').val(row.find('.data-email').attr('data-email'));
  		$('#txtphone').val(row.find('.data-phone').attr('data-phone'));
  		$('#txtruc').val(row.find('.data-ruc').attr('data-ruc'));
  		$('#logoimage').attr('src',row.find('.img-circle').attr('src'));
    	$('#frmAddComercio').removeClass('new-comercio').addClass('edit-comercio');
    	$('#frmAddComercio').modal({backdrop:'static',show: true});
    	return false;
  	});
  }

  function clearForm(){
  	$('#txtname,#txtdescription,#txtid,#txtemail,#txtruc,#txtphone,#image').val('');
  	$('#logoimage').attr('src','');
  }

  function tableEvents(){
  	$('#dataTable').on( 'order.dt', function () {
    actionButtons();
		});

		$('#dataTable').on( 'page.dt', function () {
	    actionButtons();
		});
  }

  function deleteComercio(id){
  	var row = $("tr[rowid='"+id+"']");
  	var title = row.find('.product-title').html();

  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Eliminar Comercio',
      message: '&iquest;Desea eliminar el comercio <b>'+title+'</b>?',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-close-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-delete-action',
          icon: 'fa fa-trash',
          label: 'Eliminar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            deleteComercioAction(id,title,dialog,this,row);
          }
        }
      ]
  	});
  }

  function deleteComercioAction(id,title,dialog,$button,row){
  	$('#btn-delete-action, #btn-close-action').addClass('disabled');
	  $button.spin();
	  dialog.setClosable(false);
	  $('#btn-close-action').addClass('disabled');
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('comercios.delete') }}",
	    data: {id: id},
	    success:function(data){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							row.addClass('delete');
	        							var table = $('#dataTable').DataTable();
	        							table.row('.delete').remove().draw( false );
	        							showToast('Comercio eliminado','Se eliminó el comercio <b>'+title+'</b>','success');
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al eliminar el comercio:<br><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	$button.stopSpin();
	    	$('#btn-delete-action, #btn-close-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar eliminar el comercio, intente nuevamente','warning');
	    }
	  });
  }

  function updateComercio(data){
  	var table = $('#dataTable').DataTable();
  	var row = $("tr[rowid='"+data.id+"']");
  	row.addClass('update');
  	table.row('.update').remove().draw( false );
  	showComercio(data.cols,data.id);
  }

</script>
@endpush

<li><a href="{{ route('apikeys.all') }}"><i class='fa fa-link'></i> <span>API keys</span></a></li>
<li><a href="{{ route('roles.all') }}"><i class='fa fa-link'></i> <span>Roles</span></a></li>
<li><a href="{{ route('permissions.all') }}"><i class='fa fa-link'></i> <span>Permisos</span></a></li>
<li><a href="{{ route('rolesusers.all') }}"><i class='fa fa-link'></i> <span>Usuarios</span></a></li>
<li><a href="{{ route('comercios.all') }}"><i class='fa fa-link'></i> <span>Comercios</span></a></li>
<li><a href="{{ route('locals.all') }}"><i class='fa fa-link'></i> <span>Locales</span></a></li>
<li><a href="{{ route('menus.all') }}"><i class='fa fa-link'></i> <span>Menús</span></a></li>
<li><a href="#"><i class='fa fa-link'></i> <span>Reportes</span></a></li>
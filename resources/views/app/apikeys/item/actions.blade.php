<div class="btn-group text-center">
	@if($version != NULL)
	 @if (!$suspended)
	 	<button type="button" class="btn btn-danger btn-flat btnInactive" itemid="{{$id}}" title="Desactivar API"><i class="fa fa-pause"></i></button>
	 	<button type="button" class="btn btn-warning btn-flat btnActive hidden" itemid="{{$id}}" title="Activar API"><i class="fa fa-play"></i></button>
	 @else
	 	<button type="button" class="btn btn-warning btn-flat btnActive" itemid="{{$id}}" title="Activar API"><i class="fa fa-play"></i></button>
	 	<button type="button" class="btn btn-danger btn-flat btnInactive hidden" itemid="{{$id}}" title="Desactivar API"><i class="fa fa-pause"></i></button>
	 @endif
	 	<button type="button" class="btn btn-info btn-flat btnEditAPI" itemid="{{$id}}" title="Editar API key"><i class="fa fa-pencil"></i></button>
  @else
  	<button type="button" class="btn btn-info btn-flat btnGenerateAPI" itemid="{{$id}}" title="Generar API key"><i class="fa fa-key"></i></button>
  @endif
</div>
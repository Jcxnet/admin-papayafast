<div class="modal fade" tabindex="-1" role="dialog" id="frmAddLocal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-plus-square" aria-hidden="true"></i> Agregar Local</h4>
      </div>
      <div class="modal-body">

      	<form class="form-horizontal" enctype="multipart/form-data">
      		<input type="hidden" id="txtid" value="" />
			  	<h4>Local de <span id="lblcomercio" class="text-bold text-orange"></span></h4>

	      	<div class="nav-tabs-custom">
	          <ul class="nav nav-tabs" id="local_tabs_main">
	            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" data-info="tab1">Datos</a></li>
	            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false" data-info="tab2">Ubicación</a></li>
	            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false" data-info="tab3">Contacto</a></li>
	            <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false" data-info="tab4">Horario</a></li>
	            <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false" data-info="tab5">Mapa Delivery</a></li>
	          </ul>
	          <div class="tab-content">
	            <div class="tab-pane active" id="tab_1">
	            	@include('app.locals.tabs.info')
	            </div><!-- /.tab-pane -->
	            <div class="tab-pane" id="tab_2">
	            	@include('app.locals.tabs.address')
	            </div><!-- /.tab-pane -->
	            <div class="tab-pane" id="tab_3">
	            	@include('app.locals.tabs.contact')
	            </div><!-- /.tab-pane -->
	            <div class="tab-pane" id="tab_4">
	            	@include('app.locals.tabs.timetable')
	            </div><!-- /.tab-pane -->
	            <div class="tab-pane" id="tab_5">
	            	@include('app.locals.tabs.zones')
	            </div><!-- /.tab-pane -->
	          </div><!-- /.tab-content -->
	        </div>

				</form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnAddThisLocal">Agregar Local</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Local extends Model
{
    use SoftDeletes;
		protected $table = 'locals';
		protected $dates = ['deleted_at'];
		protected $casts = [
				'code'			=> 'string',
				'address' 	=> 'array',
				'contact' 	=> 'array',
				'zone'			=> 'array',
				'timetable' => 'array',
				'image' 		=> 'array',
				'menu'			=> 'array',
				'delivery'	=> 'boolean',
		];

		public function comercio(){
			return $this->belongsTo('App\Comercio');
		}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;


use \League\Fractal\Manager;
use \League\Fractal\Resource\Collection as FractalCollection;
use \League\Fractal\Resource\Item as FractalItem;

//use App\Http\Transformers\RoleTransformer;
use GuzzleHttp\Client;


class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    private  $jsonLayout = ['data'=>[],'meta'=>[]];

    public function ajaxError($message){
    	$this->jsonLayout['meta']['status'] 	= 'error';
    	$this->jsonLayout['data']['message'] 	= $message;
    	return response()->json($this->jsonLayout,202);
    }

    public function ajaxData($data, $status='ok', $message=''){
    	$this->jsonLayout['meta']['status'] 	= $status;
    	$this->jsonLayout['meta']['message'] 	= $message;
    	$this->jsonLayout['data'] 	= $data;
    	return response()->json($this->jsonLayout);
    }

    public function getItem($item, $classTransformer){
	  	$fractal  		= new Manager();
	  	$transformer 	= new $classTransformer();
	    $resource 		= new FractalItem( $item, $transformer );
	    $resource 		= $fractal->createData($resource)->toArray();
	    return $resource['data'];
	  }

  	public function getCollection($items,$className,$classTransformer){
	   $list = new \Illuminate\Database\Eloquent\Collection;
	    $model = new $className;
	    $model->unguard();
	    foreach ($items as $item) {
	    	$list->push(new $className($item));
	    }
	  	$model->reguard();
	    $fractal  = new Manager();
	    $resource = new FractalCollection( $list, new $classTransformer() );
	    $resource = $fractal->createData($resource)->toArray();
	    return $resource['data'];
	  }

	  public function sendData($url=null,$data=[],$header=[],$request = "POST",$apikey=null,$json=true){
    	if(is_null($url))
    		return [ 'meta' =>['status'=>'error'], 'data' => ['message'=>'URL no proporcionada']];
    	$header = array_merge($header,['Accept:application/vnd.papayafast.v1+json','Cache-Control:no-cache']);
    	if($json)
    		$header = array_merge($header,['Content-Type: application/json']);

   		$data['apikey'] = $apikey;

   		$curl = curl_init();
			curl_setopt_array($curl, [
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER 	=> true,
			  CURLOPT_MAXREDIRS 		=> 10,
			  CURLOPT_TIMEOUT 			=> 30,
			  CURLOPT_HTTP_VERSION 		=> CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST 	=> $request,
			  CURLOPT_POSTFIELDS 		=> ($json)?json_encode($data):$data,
			  CURLOPT_HTTPHEADER 		=> $header,
			]);

			$response = json_decode(curl_exec($curl), true);
			$err = curl_error($curl);

			curl_close($curl);

      if ($err) {
			  return [ 'meta'=>['status'=>'error'], 'data' => ['message'=>"cURL Error #: $err"]];
			} else {
			  return $response;
			}
    }

    /**
     * envía una imagen a la API
     * @param  FileUpload $file
     * @param  string $id
     * @param  string $type
     * @param  string $collection
     * @return array
     */
    public function imageUpload($file, $id, $type, $collection){
			$data = [
  			'id'		=> $id, //$role->uuid,
  			'type' 	=> $type,//'ROLE',
  			'image' => curl_file_create($file->getRealPath(),$file->getClientMimeType(),'image'), //request->file('image')
  			'imagename'		=> $file->getClientOriginalName(),
  			'extension' 	=> $file->guessClientExtension(),
  			'collection' 	=> $collection, //'roles'
  		];
    	$item = $this->sendData( config('apiendpoints.API_IMAGES_UPLOAD'),$data,[],"POST",null,false);
    	return ['exists' => ($item['meta']['status'] == 'ok'), 'data' => $item['data']];
    }

    /**
     * recupera una imagen de la API
     * @param  string $id
     * @param  string $type
     * @param  string $collection
     * @return array
     */
    public function imageType($id, $type, $collection){
    	$data = [
  			'id'		=> $id,
  			'type' 	=> $type,
  			'collection' 	=> $collection,
  		];

  		$item = $this->sendData( config('apiendpoints.API_IMAGES_TYPE'),$data);
    	return ['exists' => ($item['meta']['status'] == 'ok'), 'data' => $item['data']];

    }

    /**
     * recupera una imagen de la API
     * @param  string $id
     * @param  string $type
     * @param  string $collection
     * @return array
     */
    public static function getImageItem($id, $type, $collection){

    	$data = [
  			'id'		=> $id,
  			'type' 	=> $type,
  			'collection' 	=> $collection,
  		];

    	$url = config('apiendpoints.API_IMAGES_TYPE');

    	if(is_null($url))
    		return ['exists' => false, 'data' => ['message' => 'URL no proporcionada']];

   		$header = array_merge(['Accept:application/vnd.papayafast.v1+json','Cache-Control:no-cache'],['Content-Type: application/json']);

   		$curl = curl_init();
			curl_setopt_array($curl, [
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER 	=> true,
			  CURLOPT_MAXREDIRS 			=> 10,
			  CURLOPT_TIMEOUT 				=> 30,
			  CURLOPT_HTTP_VERSION 		=> CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST 	=> 'POST',
			  CURLOPT_POSTFIELDS 			=> json_encode($data),
			  CURLOPT_HTTPHEADER 			=> $header,
			]);

			$response = json_decode(curl_exec($curl), true);
			$err = curl_error($curl);

			curl_close($curl);

      if ($err) {
			  return ['exists' => false, 'meta'=>['status'=>'error'], 'data' => ['message'=>"cURL Error #: $err"]];
			} else {
			  return ['exists' => ($response['meta']['status'] == 'ok'), 'data' => $response['data']];
			}

    }

    public function enumMenuType($type){
    	switch (strtolower($type)) {
    		case 'delivery'	: return 'Delivery'; 	break;
    		case 'takein'		: return 'Local'; 		break;
    		case 'takeout'	: return 'Llevar'; 		break;
    		default 				: return 'No definido';
    	}
    }

    /**
     * verifica si el Usuario tienen el Rol
     * @param  string $role
     * @return boolean
     */
    public function userhasRole($role){
    	return  \Entrust::user()->hasRole($role);
    }

    /**
     * verifica si el Usuario tiene el Permiso
     * @param  string $permission
     * @return boolean
     */
    public function userhasPermission($permission){
    	return  \Entrust::user()->can($permission);
    }
    /**
     * retorna el ID del Comercio al que pertenece el Usuario
     * @return integer
     */
    public function userComercio(){
    	return \Entrust::user()->comercio_id;
    }

    /**
     * retorna el Rol del Usuario
     * @return string
     */
    public function userRole(){
    	return \Entrust::user()->roles->first()->name;
    }
}

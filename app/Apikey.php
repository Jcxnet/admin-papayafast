<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Apikey extends Model
{
    use SoftDeletes;
		protected $table = 'apps';
		protected $dates = ['deleted_at'];
		protected $casts = ['processors' => 'array', 'suspended' => 'boolean'];

		public function comercio(){
			return $this->belongsTo('App\Comercio');
		}
}

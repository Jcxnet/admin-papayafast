<span class="text-left clearfix">
<a href="#" class="data-latlng margin-r-5" data-latlng="{{$address['point']['lat']}}|{{$address['point']['lng']}}"><i class="fa fa-map-marker"></i></a>
<span class="data-address" data-address="{{$address['address']}}">{{$address['address']}}</span>
</span>
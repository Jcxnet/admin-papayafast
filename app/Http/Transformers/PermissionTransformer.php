<?php

namespace App\Http\Transformers;

use App\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract{

	public function transform(Permission $permission){
		return [
			'id'	    		=> $permission->uuid,
      'name'    		=> $permission->name,
      'displayname'	=> $permission->display_name,
      'description'	=> $permission->description,
      'action'			=> $permission->action,
		];
	}

}

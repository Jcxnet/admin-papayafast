<?php

namespace App\Http\Transformers;

use App\Menu;
use League\Fractal\TransformerAbstract;

class MenucomercioTransformer extends TransformerAbstract{

	public function transform(Menu $menu){
		//$items = $menu->menu;
		return [
			'id'	 			=> $menu->uuid,
      'type'  		=> $menu->type,
      'title' 		=> $menu->title,
      'active'		=> $menu->active,
      'version'		=> $menu->version,
      'totals'		=> $this->menuTotals($menu->menu),
		];
	}

	private function menuTotals($menu){
		return [
			'pages' 			=> $this->keyTotal('pages',$menu),
			'buttons' 		=> $this->keyTotal('buttons',$menu),
			'products' 		=> $this->keyTotal('products',$menu),
			'properties' 	=> $this->keyTotal('properties',$menu),
		];
	}

	private function keyTotal($key,$menu){
		$item = collect($menu)->pluck($key)->filter(function($value,$key){ return $value != NULL; })->values()->toArray();
		if(!is_array($item))
			return 0;
		if(!count($item)>0)
			return 0;
		return count($item[0]);
	}

}

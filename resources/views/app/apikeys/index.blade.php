@extends('layouts.app')

@section('htmlheader_title')
	API keys
@endsection


@section('main-content')
	 <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de Comercios - 	API keys</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-striped ">
            <thead>
            <tr>
              <th>Comercio</th>
              <th>Clases</th>
              <th>API key</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($comercios as $comercio)
            	@include ('app.apikeys.item.item',['comercio'=>$comercio])
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>Comercio</th>
              <th>Clases</th>
              <th>API key</th>
              <th></th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>


<div class="modal fade" tabindex="-1" role="dialog" id="frmAddKey">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-blue"><i class="fa fa-key" aria-hidden="true"></i> Generar API key</h4>
      </div>
      <div class="modal-body">

				  <form class="form-horizontal">
				  	<input type="hidden" id="txtid" value="" />
			      <div class="form-group">
			        <label for="txtname" class="col-sm-2 control-label">Comercio</label>
			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtname" value="" disabled>
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="selectOrder" class="col-sm-2 control-label">Pedidos</label>
			        <div class="col-sm-10">
			          <select class="form-control" id="selectOrder">
                  <option value=''>Selecciona una clase</option>
                  @foreach($orderClass as $class => $details)
                  	<option value="{{$class}}"> {{$details['detalle']}} - ({{$details['version']}})</option>
                  @endforeach
                </select>
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="selectPayment" class="col-sm-2 control-label">Pagos</label>
			        <div class="col-sm-10">
			          <select class="form-control" id="selectPayment">
                  <option value=''>Selecciona una clase</option>
                  @foreach($paymentClass as $class => $details)
                  	<option value="{{$class}}"> {{$details['detalle']}} - ({{$details['version']}})</option>
                  @endforeach
                </select>
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="txtapikey" class="col-sm-2 control-label">API key</label>
			        <div class="col-sm-10">
			          <input type="text" class="form-control" id="txtapikey" disabled>
			          <a href="#" id="generateApi">Generar API key</a>
			        </div>
			      </div>
				  </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btnApikeyValues">Asignar valores</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@push('pagescript')
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/password/password.generator.min.js') }}"></script>
<script>

  $(function () {

    initTable();
    formButtons();
    actionButtons();
    tableEvents();

  });

  function initTable(){
  	$('#dataTable').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "autoWidth": true,
      "responsive": {
        "details": false
    	},
      "columns": [
			    { "orderable": true  },
			    { "orderable": false },
			    { "orderable": false },
			    { "orderable": false }
			  ]
    });
  }

  function formButtons(){
  	$('#generateApi').on('click',function(e){
    	e.preventDefault()
    	var pass = $.passGen({'length' : 32, 'numeric' : true, 'lowercase' : true, 'uppercase' : true, 'special' : false});
    	$('#txtapikey').val(pass).focus();
    	return false;
    });

    $('#btnApikeyValues').on('click',function(e){
  		e.preventDefault();
  		sendApikey();
  		return false;
  	});
  }

  function actionButtons(){

  	$('.btnGenerateAPI,.btnEditAPI,.btnInactive,.btnActive').unbind();
  	$('.btnGenerateAPI').on('click',function(e){
  		e.preventDefault();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		clearForm();
  		$('#txtid').val(id);
  		$('#txtname').val(row.find('.product-title').attr('data-title'));
  		$('#frmAddKey').modal({backdrop:'static',show: true});
  		return false;
  	});

  	$('.btnEditAPI').on('click',function(e){
  		e.preventDefault();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		clearForm();
  		$('#txtid').val(id);
  		$('#txtname').val(row.find('.product-title').attr('data-title'));
  		$('#selectOrder').val(row.find('.data-orders').attr('data-orders'));
  		$('#selectPayment').val(row.find('.data-payments').attr('data-payments'));
  		$('#txtapikey').val(row.find('.data-token').attr('data-token'));
  		$('#frmAddKey').modal({backdrop:'static',show: true});
  		return false;
  	});

  	$('.btnInactive').on('click',function(e){
  		e.preventDefault();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		var comercio = row.find('.product-title').attr('data-title');
  		powerOffAPI(comercio,id);
  		return false;
  	});

  	$('.btnActive').on('click',function(e){
  		e.preventDefault();
  		var id = $(this).attr('itemid');
  		var row = $("tr[rowid='"+id+"']");
  		var comercio = row.find('.product-title').attr('data-title');
  		powerOnAction(comercio,id);

  		return false;
  	});

  }

  function sendApikey(){
  	$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('apikeys.values') }}",
      data: {id:$('#txtid').val(),orders: $('#selectOrder').val(),payments: $('#selectPayment').val(), apikey: $('#txtapikey').val()},
      beforeSend:function(){
        showAlert('API key','Enviando y verificando datos...','info');
      },
      success:function(data){
      	closeAllDialogs();
        switch(data.meta.status){
          case 'ok'		: showToast('API key','Datos recibidos','success');
          							$('#frmAddKey').modal('hide');
          							updateApikey(data.data);
          							actionButtons();
          							clearForm();
          							break;
          case 'error': showMessage('Error','Error al generar los datos:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	closeAllDialogs();
      	showMessage('Aviso','Se produjo un error al intentar enviar los datos, intente nuevamente','warning');
      }
    });
  }

  function clearForm(){
  	$('#txtid,#txtname,#txtapikey,#selectOrder,#selectPayment').val('');
  }

  function tableEvents(){
  	$('#dataTable').on( 'order.dt', function () {
    actionButtons();
		});

		$('#dataTable').on( 'page.dt', function () {
	    actionButtons();
		});
  }



  function powerOffAPI(comercio, id){
  	BootstrapDialog.show({
  		type: BootstrapDialog.TYPE_DANGER,
      title: 'Desactivar API',
      message: '&iquest;Desea desactivar el API de <b>'+comercio+'</b>?<br/>Si hace esto las funciones para <i>'+comercio+'</i> quedarán suspendidas y puede originar errores.',
      spinicon: 'fa fa-cog fa-spin',
      buttons: [
      	{
          id: 'btn-cancel-action',
          label: 'Cancelar',
          icon: 'fa fa-times',
          cssClass: 'btn-primary',
          action: function(dialog){
              dialog.close();
        	}
        },
        {
          id: 'btn-inactive-action',
          icon: 'fa fa-pause',
          label: 'Desactivar',
          cssClass: 'btn-danger',
          action: function(dialog) {
            powerOffAction(comercio, id,dialog,this);
          }
        }
      ]
  	});
  }

  function powerOffAction(comercio, id, dialog, button){
  	$('#btn-cancel-action, #btn-inactive-action').addClass('disabled');
	  button.spin();
	  dialog.setClosable(false);
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('apikeys.suspend') }}",
	    data: {id: id},
	    success:function(data){
	    	dialog.setClosable(true);
	    	button.stopSpin();
	    	$('#btn-cancel-action, #btn-inactive-action').removeClass('disabled');
	    	switch(data.meta.status){
	        case 'ok'		: closeAllDialogs();
	        							showToast('API key inactiva','Se inactivó la API key de <b>'+comercio+'</b>','success');
	        							var row = $("tr[rowid='"+id+"']");
	        							row.find('.btnInactive').hide();
	        							row.find('.btnActive').removeClass('hidden').show();
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al intentar desactivar la API key.<br/><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	dialog.setClosable(true);
	    	button.stopSpin();
	    	$('#btn-cancel-action, #btn-inactive-action').removeClass('disabled');
	    	showMessage('Aviso','Se produjo un error al intentar inactivar la API key, intente nuevamente','warning');
	    }
	  });
  }

  function powerOnAction(comercio, id){
	  $.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	  $.ajax({
	    method: "POST",
	    url: "{{ route('apikeys.activate') }}",
	    data: {id: id},
	    success:function(data){
	    	switch(data.meta.status){
	        case 'ok'		: showToast('API key activa','Se activó la API key de <b>'+comercio+'</b>','success');
	        							var row = $("tr[rowid='"+id+"']");
	        							row.find('.btnActive').hide();
	        							row.find('.btnInactive').removeClass('hidden').show();
	        							break;
	        case 'error': showMessage('Error','Ocurrió un error al intentar activar la API key.<br/><b>'+data.data.message+'</b>','danger'); break;
	        default : break;
	      }
	    },
	    error: function(){
	    	showMessage('Aviso','Se produjo un error al intentar activar la API key, intente nuevamente','warning');
	    }
	  });
  }

  function updateApikey(data){
  	var table = $('#dataTable').DataTable();
  	var row = $("tr[rowid='"+data.id+"']");
  	row.addClass('update');
  	table.row('.update').remove().draw( false );
  	showApikey(data.cols,data.id);
  }

  function showApikey(cols,id){
  	cols = JSON.parse(cols);
  	var table = $('#dataTable').DataTable();
		var node  = table.row.add( [cols.name, cols.clases, cols.apikey, cols.actions] ).node();
		table.column(0).data().sort().draw();
		$(node).attr('rowid',id);
  }

</script>
@endpush
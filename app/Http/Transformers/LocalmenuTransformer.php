<?php

namespace App\Http\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalmenuTransformer extends TransformerAbstract{

	public function transform(Local $local){
		return [
			'id'	 				=> $local->uuid,
      'name'  			=> $local->name,
      'menu'				=> $local->menu,
		];
	}

}

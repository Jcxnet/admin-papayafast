		<div class="box box-widget widget-user">
			<div class="widget-user-header bg-black" style="background: url('{{$page['image'][0]}}') center center;">
		    <h3 class="widget-user-username text-black text-bold">{!!$page['title']!!}</h3>
		    <h5 class="widget-user-desc">&nbsp;</h5>
		  </div>
		  <div class="widget-user-image">
		     <img class="img-circle" src="{{ asset('img/icon/menu.icon.png') }}" alt="">
		  </div>
		  <div class="box-body">
		  	<?php $pos = 1; ?>
		  	@foreach($items as $item)
		  		<?php if ($pos == 1):?>
		  			<div class="row">
		  		<?php endif; $pos++; ?>
		  		@if ($item['type'] == 'button')
		  			@include('app.editor.button.preview',['button'=>$item])
		  		@elseif ($item['type'] == 'product')
		  			@include('app.editor.product.preview',['product'=>$item])
		  		@endif
		  		<?php if ($pos>2): ?>
		  			</div>
		  		<?php $pos = 1; endif; ?>
		  	@endforeach
		  </div>
		  <div class="box-footer">
		    @if($back>0)
		    	<a href="#" class="btn btn-info btn-block btn-preview-back" back-id='{{$back}}'><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;Regresar</a>
		    @endif
		  </div>
		</div>

<script type="text/javascript">
	 $(function (){
  	$('.add-to-cart, .remove-from-cart').on('click', function() {
    	$(this).parents('.flipable').toggleClass('flipped');
  	});
  	groupsMultiSelect();
  });

	function groupsMultiSelect(){
	 	$('.multiselect-group').unbind();
	 	$('.multiselect-group').each(function(){
	 		$(this).find('option').first().attr('selected',true);
	 	});
  	$('.multiselect-group').multiselect({
				enableFiltering: false,
	      includeSelectAllOption: false,
	      moveSelectedToTop: true,
	   		onChange: function(option, checked) {
	       	var item = $(this)[0].$select;
	       	$(item).multiselect('deselectAll', true);
          $(item).multiselect('select', option.val());
         }
	  });

	}
</script>
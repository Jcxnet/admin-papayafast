
<li class="list_origin_item item-menu item-{{$type}} {{((!$item['available'])?'item-unavailable':'')}}" id="item-{{$type}}-{{$item['order']}}" sort-id="{{$type}}-{{$item['order']}}" item-type="{{$type}}" item-id="{{$item['id']}}" item-order="{{$item['order']}}" item-title="{{$item['title']}}" item-price="{{$item['price']}}" item-code="{{$item['code']}}" item-quantity="{{$item['cantidad']}}" item-available="{{$item['available']}}">
	<span class="drag fa fa-arrows" aria-hidden="true"></span>
	&nbsp;<span class="item-menu-title">{{$item['title']}}</span>
	<small class="item-menu-spec"><i class="fa fa-hashtag"></i><span class="item-code">{{$item['code']}}</span></small>
	<small class="item-menu-spec"><i class="fa fa-dollar"></i><span class="item-price">{{$item['price']}}</span></small>
	<span class="pull-right item-menu-edit" item-type="{{$type}}" item-order="{{$item['order']}}" item-id="{{$item['id']}}"><i class="fa fa-pencil"></i></span>
	<span class="pull-right item-menu-delete" item-type="{{$type}}" item-order="{{$item['order']}}" item-id="{{$item['id']}}"><i class="fa fa-times"></i></span>
</li>
@if($apikey['processors']['orders'] != NULL AND $apikey['processors']['payments'] != NULL)
<span class="text-left clearfix"><i class="fa fa-tags margin-r-5"></i><span class="data-orders" data-orders="{{$apikey['processors']['orders']}}">{{$apikey['processors']['orders']}}</span></span>
<span class="text-left clearfix"><i class="fa fa-credit-card margin-r-5"></i><span class="data-payments" data-payments="{{$apikey['processors']['payments']}}">{{$apikey['processors']['payments']}}</span></span>
@else
<span class="text-left clearfix"><i class="fa fa-exclamation margin-r-5"></i>Sin definir</span>
@endif
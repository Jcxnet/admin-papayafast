	<script type="text/javascript">

		$(function(){
			formButton();
		});

		function getButtonData(){
			var page = getContainerType('#list_add_zone','page');
			page = (page.length>0)?page[0]:null;
			var data = {
				id 		: $('#txtid').val(),
				title : $('#txttitle').val(),
				style : $('input:radio[name=optbuttontype]:checked').val(),
				image : [],
				page  : page,
				order : 1,
			};
			return JSON.stringify(data);
		}

		function formButton(){
			$('#btnButtonSubmit').unbind();
			$('#btnButtonSubmit').on('click',function(e){
				e.preventDefault();
				validateButtonForm();
				return false;
			});
		}

		function validateButtonForm(){
			var dataForm = getButtonData();
			$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
	    $.ajax({
	      method: "POST",
	      url: "{{ route('editor.button.add') }}",
	      data: dataForm,
	      beforeSend:function(){
	        overlayContent('#editor-content');
	      },
	      success:function(data){
	      	removeOverlay('#editor-content');
	        switch(data.meta.status){
	          case 'ok'		: saveButton(dataForm);
	          							showToast('Botón','los datos se actualizaron correctamente','success');
	          							saveMenuSystem();
	          							updateSidebar('list_origin_button','button',data.data.id, data.data.title, data.data.buttonitem);
	          							break;
	          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
	          default : break;
	        }
	      },
	      error: function(){
	      	removeOverlay('#editor-content');
	      	showMessage('Aviso','Se produjo un error al intentar agregar el elemento, intente nuevamente','warning');
	      }
	    });
		}

		function saveButton(data){
			var menu 		= getMenu();
			var button  = JSON.parse(data);

			menu['buttons'][button.id] = button;
			editor('new',{'type':'button', 'id':button.id, 'saved':true, 'timestamp': getTimestamp()});
			saveMenu(menu);
		}

		function showButtonValues(id){
			var buttons = getMenuKey('buttons');
			if(!Object.keys(buttons).length){
				showMessage('Botón','no se encontraron botones, intente nuevamente','warning');
				return false;
			}
			if(buttons[id] === undefined){
				showMessage('Botón','el botón no se encuentra, intente nuevamente','warning');
				return false;
			}
			showButtonElements(buttons[id]);
		}

		function showButtonElements(button){
			setActualView('editbutton');
			editor('new',{'type':'button', 'id':button.id, 'saved':false, 'timestamp': getTimestamp()});
			var itemPage 	= getMenuItems('pages',[button.page]);
			$('#txtid').val(button.id);
	    $('#txttitle').val(button.title);
	    $('input[name=optbuttontype][value="'+button.style+'"]').prop("checked",true);
	    addMenuItems(getButtonItems(itemPage));
		}

		function getButtonItems(page){
			var list = [];
			for(i=0;i<page.length;i++){
				list.push({type:'page',id:page[i].id,title:page[i].title,order:1});
			}
			return {items:list};
		}


	</script>
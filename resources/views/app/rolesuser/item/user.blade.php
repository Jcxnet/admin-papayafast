<span class="hide">{{$user['name']}}</span>
<div class="products-list">
	<div class="product-img">
    <img src="{{$user['avatar']}}" alt="" class="img-circle" />
  </div>
	<div class="product-info">
    <a href="#" class="product-title" data-title="{{$user['name']}}">{{$user['name']}}</a>
     <span class="product-description" data-description="{{$user['email']}}">
       {{$user['email']}}
     </span>
  </div>
</div>
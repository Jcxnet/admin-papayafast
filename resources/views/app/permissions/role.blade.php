<th item-role="{{$role['name']}}" item-role-id="{{$role['id']}}">
	<div class="user-panel">
		<div class="pull-left image">
		    <img src="{{$role['avatar']}}" class="img-circle" alt="">
		</div>
		<div class="pull-left info">
		    <p class="text-black">{{$role['displayname']}}</p>
		    <a href="#" class="text-blue">{{$role['description']}}</a>
		</div>
	</div>
</th>
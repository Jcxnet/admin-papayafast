APP_ENV=local
APP_DEBUG=true
APP_KEY=base64:UC9FxRg+pf1MHEq1kZt/XCgho5KIx1AY+zUMFyXOCMM=
APP_URL=http://admin.localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=papaya.api
DB_USERNAME=root
DB_PASSWORD=root

CACHE_DRIVER=redis
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=mailgun
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=tls

MAILGUN_DOMAIN		=	sandboxa5c2d3d4b5d94ffbb713b237803dc261.mailgun.org
MAILGUN_SECRET 		= key-473fd1621818b3b8821bcb8c600f0255
MAIL_SYSTEM_EMAIL	= noreply@papayafastadmin.com
MAIL_SYSTEM_NAME	=	'Papayafast Admin'

API_DEBUG = true

<?php
	$name = isset($comercio)?$comercio:'';
?>
@if(count($role)==0)
	<div class="role-title" data-role="Sin rol asignado">Sin rol asignado</div>
@else
	<div class="role-title" data-role="{{$role['displayname']}}"><span class="label label-success text-sm">{{$role['displayname']}}</span> - {{$name}}</div>
@endif
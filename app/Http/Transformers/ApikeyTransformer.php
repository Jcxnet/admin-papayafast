<?php

namespace App\Http\Transformers;

use App\Apikey;
use League\Fractal\TransformerAbstract;

class ApikeyTransformer extends TransformerAbstract{

	public function transform(Apikey $apikey){
		return [
			'id'	 				=> $apikey->uuid,
      'name'  			=> $apikey->name,
      'version' 		=> $apikey->version,
      'processors'	=> $apikey->processors,
      'token'				=> $apikey->token,
		];
	}


}

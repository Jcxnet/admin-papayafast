<div class="box box-primary box-solid">
  	<div class="box-header with-border bg-blue">
      <h3 class="box-title"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;Página</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool remove-form" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>

		<div class="box-body">
  		<form class="form">
  			<input type="hidden" id="txtid" name="txtid" />
        <div class="form-group">
          <label for="txttitle" class="control-label">Título</label>
          <input type="text" class="form-control" id="txttitle" placeholder="Título">
        </div>
        <div class="form-group">
          @include ('app.editor.page.container',['items'=>[]])
        </div>
      </form>
    </div>

    <div class="box-footer">
      <button class="btn btn-default remove-form">Cancelar</button>
      <button class="btn btn-primary pull-right" id="btnPageSubmit">Guardar cambios</button>
    </div><!-- /.box-footer -->


</div>

@include ('app.editor.page.functions')

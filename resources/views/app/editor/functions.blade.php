<script type="text/javascript">

	var editor = null;
	var listPages = null;
	var listButtons = null;
	var listProducts = null;
	var listAddZone = null;

	$(function(){
		editor = store.namespace('editor');
		editor.clearAll();
		saveMenu({!!json_encode($menu)!!});
		setActualView('resume');
		sidebarElements();
		actionButtons();
		sortEditorLists();
		sortSidebarLists();
		previewMenuAction();
		mainPageAction();
	});

	function previewMenuAction(){
		$('.menu-preview').unbind();
		$('.menu-preview').on('click',function(e){
			e.preventDefault();
  		previewMenu($(this).attr('item-id'));
  		return false;
		});
	}

	function sidebarElements(){
		$(".accordion-set-header").on("click", function(){
	    if($(this).hasClass('active')){
	      $(this).removeClass("active");
	      $(this).siblings('.accordion-set-content').slideUp(200);
	      $(".accordion-set-header i.accordion-icon-header").removeClass("fa-minus").addClass("fa-plus");
	    }else{
	      $(".accordion-set-header i.accordion-icon-header").removeClass("fa-minus").addClass("fa-plus");
		    $(this).find("i.accordion-icon-header").removeClass("fa-plus").addClass("fa-minus");
		    $(".accordion-set-header").removeClass("active");
		    $(this).addClass("active");
		    $('.accordion-set-content').slideUp(200);
		    $(this).siblings('.accordion-set-content').slideDown(200);
	    }
	  });
	}

	function sortEditorLists(){

		if($('#list_origin_page').length){
			listPages = Sortable.create(list_origin_page, {
	  			group: {
	  				name: 'list_origin_page_group',
	  				pull: 'clone',
	  				put: false,
	  			},
	  			animation: 100,
	  			sort: false,
	  			ghostClass: 'item-menu-ghost',
	  			chosenClass: 'item-menu-chosen',
	  			draggable: '.list_origin_item',
	  			dataIdAttr: 'sort-id',
			});
		}

		if($('#list_origin_button').length){
			listButtons = Sortable.create(list_origin_button, {
	  			group: {
	  				name: 'list_origin_button_group',
	  				pull: 'clone',
	  				put: false,
	  			},
	  			animation: 100,
	  			sort: false,
	  			ghostClass: 'item-menu-ghost',
	  			chosenClass: 'item-menu-chosen',
	  			draggable: '.list_origin_item',
	  			dataIdAttr: 'sort-id',

			});
		}

		if($('#list_origin_product').length){
			listProducts = Sortable.create(list_origin_product, {
	  			group: {
	  				name: 'list_origin_product_group',
	  				pull: 'clone',
	  				put: false,
	  			},
	  			animation: 100,
	  			sort: false,
	  			ghostClass: 'item-menu-ghost',
	  			chosenClass: 'item-menu-chosen',
	  			draggable: '.list_origin_item',
	  			dataIdAttr: 'sort-id',

			});
		}
	}

	function sortSidebarLists(){
		if($('#sidebar_clone_zone').length){
			Sortable.create(sidebar_clone_zone, {
				group:{
					name: 'sidebar_clone_zone_group',
					put: ['list_origin_page_group','list_origin_button_group','list_origin_product_group'],
				},
				dataIdAttr: 'sort-id',
				onAdd: function (evt) {
					sidebarAction('clone', evt.item, evt.newIndex);
		    },
			});
		}

		if($('#sidebar_drop_zone').length){
			Sortable.create(sidebar_drop_zone, {
				group:{
					name: 'sidebar_drop_zone_group',
					put: ['list_origin_page_group','list_origin_button_group','list_origin_product_group'],
				},
				dataIdAttr: 'sort-id',
				onAdd: function (evt) {
					sidebarAction('delete',evt.item, evt.newIndex);
		    },
			});
		}

	}

	function sortContainerLists(type){

		if($('#list_add_zone').length){

			var fromlist = '';
			switch(type){
				case 'page'			: fromlist = ['list_origin_button_group','list_origin_product_group']; break;
				case 'button'		: fromlist = ['list_origin_page_group']; break;
				case 'product'	: return false;
				default 				: return false;
			}

			if(listAddZone != null)
				listAddZone.destroy();

			listAddZone = Sortable.create(list_add_zone, {
  			group: {
  				name: 'list_add_zone_group',
  				put: fromlist,
  			},
  			animation: 100,
  			ghostClass: 'item-menu-ghost',
  			chosenClass: 'item-menu-chosen',
  			sort: true,
  			dataIdAttr: 'sort-id',
  			onAdd: function (evt) {
		      switch(type){
		      	case 'page'  : checkDuplicate('#list_add_zone',evt.newIndex, $(evt.item).attr('sort-id')); break;
		      	case 'button': pushItem('#list_add_zone',evt.newIndex); break;
		      }
		      addEditAction('#item-'+$(evt.item).attr('sort-id'));
		      updateTotalContainer('#list_add_zone');
		    },
		    onSort: function (evt) {
        	updateTotalContainer('#list_add_zone');
    		},
			});
		}

		if($('#list_drop_zone').length && $('#list_add_zone').length){
			Sortable.create(list_drop_zone, {
				group:{
					name: 'list_drop_zone_group',
					put: ['list_add_zone_group'],
				},
				dataIdAttr: 'sort-id',
				onAdd: function (evt) {
					removeItemList(evt.item, evt.newIndex, '#list_drop_zone li');
		    },
			});
		}

	}

	function actionButtons(){
		$('.item-menu-add, .none-button-add, .item-menu-edit').unbind();
		$('.item-menu-add, .none-button-add').on('click',function(e){
			e.preventDefault();
			var type = $(this).attr('item-type');
			showItemForm(type);
			return false;
		});
		addEditAction('.item-menu-edit');
	}

	function addEditAction(item){
		$(item).on('click',function(e){
			e.preventDefault();
			editMenuItem(this);
			return false;
		});
	}

	function editMenuItem(item){
		var type 	= $(item).attr('item-type');
		var id 		= $(item).attr('item-id');

		switch(type){
			case 'page' 	:  showItemForm(type).done(function(){
												showPageValues(id);
												}); break;
			case 'button'	: showItemForm(type).done(function(){
												showButtonValues(id);
												}); break;
			case 'product': showItemForm(type).done(function(){
												showProductValues(id);
												}); break;
			default: showMessage('Error','el elemento seleccionado no es editable','danger');
		}
	}

	function sidebarAction (action, item, index){
		var title = $(item).find('.item-menu-title').html();;
		var type 	= $(item).attr('item-type');
		var id 		= $(item).attr('item-id');
		var list 	= '';
		var menukey = '';

		switch(type){
			case 'page'		: list = 'list_origin_page'; 		type ='la página'; 	menukey='pages'; 		break;
			case 'button'	: list = 'list_origin_button'; 	type ='el botón'; 		menukey='buttons'; 	break;
			case 'product': list = 'list_origin_product'; 	type ='el producto'; menukey='products';	break;
			default: showMessage('Aviso','el tipo de elemento es desconocido','warning');
							 return false;
		}

		var msg = '';
		switch(action){
			case 'clone' 	: msg = '&iquest;desea duplicar '+type+' <b>'+title+'</b> y todos sus elementos?';
											cloneItem('Duplicar',msg, menukey, list, id, item, index);
											break;
			case 'delete' : msg = '&iquest;desea eliminar '+type+' <b>'+title+'</b> y todos sus elementos?';
											removeItem('Eliminar',msg, menukey, list, id, item, index);
											break;
			default:  showMessage('Aviso','la acción no se encuentra disponible','warning');
		}

	}

	function cloneItem(title, message, key, list, id, item, index){
		 var dialog = BootstrapDialog.confirm({
        title:  title,
        message: message,
        type: BootstrapDialog.TYPE_INFO,
        closable: false,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Duplicar',
        btnOKClass: 'btn-info',
        callback: function(result) {
        		dialog.close();
        		removeItemList(item, index, '#sidebar_clone_zone li');
        		if(result) {
              cloneItemAction(key, id, $(item).attr('item-type'),$(item).find('.item-menu-title').html(),list);
            }
        }
    });
	}

	function removeItem(title, message, key, list, id, item, index){
		 var dialog = BootstrapDialog.confirm({
        title:  title,
        message: message,
        type: BootstrapDialog.TYPE_DANGER,
        closable: false,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Eliminar',
        btnOKClass: 'btn-danger',
        callback: function(result) {
        		dialog.close();
        		removeItemList(item, index, '#sidebar_drop_zone li');
        		if(result) {
              removeItemAction(key, id, $(item).attr('item-type'),$(item).find('.item-menu-title').html(),list);
            }
        }
    });
	}

	function cloneItemAction(key, id, type, title, list){
		var items = getMenuKey(key);
		var clone = JSON.parse(JSON.stringify(items[id]));
		clone.id  = getNextItemId(type);
		items[clone.id] = clone;
		updateMenuKey(key,items);
		showToast('Duplicar','el elemento se duplicó correctamente','success');
		saveMenuSystem().done(function(){
			if( getActualView() ==  'resume')
				showMenuResume();
		});
		addEditAction('#item-'+type+'-'+id+' .item-menu-edit');
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('editor.items.html') }}",
      data: JSON.stringify({item:{id: clone.id, type: type, title: title, code: clone.code, price: clone.price }}),
      beforeSend:function(){
        overlayContent('#editor-sidebar');
      },
      success:function(data){
      	removeOverlay('#editor-sidebar');
      	switch(data.meta.status){
          case 'ok'		: updateSidebar(list,type, data.data.item.id, data.data.item.title, data.data.content);
          							showMessage('Duplicar','se agregó el elemento duplicado a la lista','success');
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-sidebar');
      	showMessage('Aviso','Se produjo un error al intentar agregar el elemento','warning');
      }
    });
	}

	function removeItemAction(key, id, type, title, list){
		var items = getMenuKey(key);
		var item 	= items[id];
		overlayContent('#editor-sidebar');
		removeReferences(type,id);
		items[id] = null;
		updateMenuKey(key,items);
		showToast('Eliminar','el elemento se eliminó correctamente','success');
		saveMenuSystem().done(function(){
			$('#item-'+type+'-'+id).addClass('removed-item');
			$('#item-'+type+'-'+id).remove();
			if( getActualView() ==  'resume')
				showMenuResume();
			else
				updateEditorView(id, type);
		});
		removeOverlay('#editor-sidebar');
	}

	function removeReferences(type,id){
		switch(type){
			case 'page':
				removeItemFrom('buttons','page',id);
				break;
			case 'button':
				removeItemFrom('pages','buttons',id);
				break;
			case 'product':
				removeItemFrom('pages','products',id);
				break;
		}
	}

	function removeItemFrom(key, property, id){
		var items = getMenuKey(key);
		var size = (Object.keys(items).length)?Object.keys(items).length:items.length;
		for(index=0;index<size;index++){
			if(items[index]){
				var prop = items[index][property];
				if(isArray(prop)){
					for(i=0;i<prop.length;i++){
						if(parseInt(prop[i])==parseInt(id)){
							console.log('found & remove!');
							prop.splice(i,1);
							break;
						}
					}
					items[index].property = prop;
				}else{
					if(parseInt(prop) == parseInt(id))
						items[index].property = null;
				}
			}
		}
		updateMenuKey(key,items);
	}

	function showMenuResume(){
		setActualView('resume');
		var menu 			= getMenu('menu');
		var pagetitle = (menu.page == false || menu.page == 'false')?'Por definir':menu.pages[parseInt(menu.page)].title;

		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('editor.menu.resume') }}",
      data: {title: menu.title, comercio:menu.comercio, type:menu.type, version:menu.version, created:menu.created, updated:menu.updated, pagetitle: pagetitle, total:{page:menu.total.page, button:menu.total.button, product:menu.total.product}},
      beforeSend:function(){
        overlayContent('#editor-content');
      },
      success:function(data){
      	removeOverlay('#editor-content');
      	switch(data.meta.status){
          case 'ok'		: updateContent(data.data.content);
          							boxEvents();
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-content');
      	showMessage('Aviso','Se produjo un error al intentar mostrar los datos del menú','warning');
      }
    });
	}

	function showItemForm(type){
		var nextid = getNextItemId(type);
		if(!nextid){
			showMessage('Error','No se pudo determinar el identificador para el Elemento','warning');
			return false;
		}
		setActualView('new'+type);
		editor('new',{'type':type, 'id':nextid, 'saved':false, 'timestamp': getTimestamp()});

		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    return $.ajax({
      method: "POST",
      url: "{{ route('editor.add.form') }}",
      data: {type: type},
      beforeSend:function(){
        overlayContent('#editor-content');
      },
      success:function(data){
      	removeOverlay('#editor-content');
        switch(data.meta.status){
          case 'ok'		: updateContent(data.data.content);
          							boxEvents();
          							sortContainerLists(type);
          							$('#txtid').val(nextid);
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-content');
      	showMessage('Aviso','Se produjo un error al intentar agregar el elemento, intente nuevamente','warning');
      }
    });
	}

	function getNextItemId(type){
		var next = 0;
		var max = getMenuKey('max');
		switch(type){
			case 'page'			: next = max.page; break;
			case 'button'		: next = max.button; break;
			case 'product'	: next = max.product; break;
			case 'property'	: next = max.property; break;
			default 				: return false;
		}
		return parseFloat(next)+1;
	}

	function getMaxId(list) {
		if(typeof(list)=='object'){
			var arr = Object.keys( list ).map(function ( index ) {  return parseInt(list[index].id); });
			return Math.max.apply( null, arr );
		}
		return Math.max.apply( null, list );
	}

	function updateContent(content){
		$('#editor-content').html(content);
	}

	function boxEvents(){
		$('.remove-form').unbind();
		$('.remove-form').on('click', function (e) {
    	setActualView(getLastView());
    	showActualView();
		});
	}

	function saveMenu(menu){
		store.editor.set('menu',menu);
	}

	function reorderItems(items, orderlist, type){
		for(i=0;i<orderlist.length;i++){
			if(orderlist[i].type == type){
				items[parseInt(orderlist[i].id)].order = orderlist[i].order;
			}
		}
		return items;
	}


	function saveMenuSystem(){
		menu = getMenu();
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    return $.ajax({
      method: "POST",
      url: "{{ route('editor.menu.save') }}",
      data: JSON.stringify(menu),
      beforeSend:function(){
        overlayContent('#editor-content');
      },
      success:function(data){
      	removeOverlay('#editor-content');
        switch(data.meta.status){
          case 'ok'		: saveMenu(data.data.menu);
          							showToast('Menú','los datos se actualizaron correctamente','success');
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-content');
      	showMessage('Aviso','Se produjo un error al intentar actualizar el menú, intente nuevamente','warning');
      }
    });
	}

	function setActualView(view){
		editor('view',{actual: view, last: getLastView() });
	}

	function getMenu(){
		var menu = store.editor.get('menu');
		if(menu == null)
			return [];
		return menu;
	}

	function getMenuKey(key){
		var menu = store.editor.get('menu');
		if(menu[key]  === undefined || menu[key] == null || menu[key] == '')
			return [];
		return menu[key];
	}

	function getMenuItems(key,list){
		var items = getMenuKey(key);
		var selection = [];
		if(Object.keys(items).length){
			for(i=0;i<list.length;i++){
				if(items[parseInt(list[i])]){
					selection.push(items[parseInt(list[i])]);
				}
			}
			return selection;
		}
		return [];
	}

	function updateMenuKey(key, values){
		var menu = store.editor.get('menu');
		menu[key] = values;
		saveMenu(menu);
	}

	function getLastView(){
		var view = store.editor.get('view');
		if(view == null)
			return '';
		if(view.actual)
			return view.actual;
		return '';
	}

	function getActualView(){
		var view = store.editor.get('view');
		return view.actual;
	}

	function showActualView(){
		var view = store.editor.get('view');
		if(view.actual == '')
			view.actual = view.last;
		if(view.actual== ''){
			showMenuResume();
		}
		else{
			var txt1 = view.actual.substring(0,3);
			var txt2 = view.actual.substring(0,4);
			if(txt1=='new' || txt2 == 'edit')
				showMenuResume();
			else
				showToast('Update content','mostrar vista '+view.actual,'info');
		}
	}

	function checkDuplicate(list, index, id){
		var total = $(list).find('li[sort-id="'+id+'"]').length;
		if(total>1){
			$(list+' li').eq(index).addClass('removed-item');
			$(list+' li').eq(index).remove();
			showToast('Duplicado','El elemento ya se encuentra en la lista','warning');
		}else{
			var id = $(list+' li').eq(index).attr('id');
			$(list+' li').eq(index).attr('id','list-add-'+id);
		}
	}

	function pushItem(list, index){
		var total = $(list).find('li').length;
		if(total>1){
			index = (index==0)?1:0;
			$(list+' li').eq(index).remove();
		}
	}

	function updateEditorView(id, type){
		var itemlist 	= '#item-'+type+'-'+id;
		var itemnew 	= '#list-add-item-'+type+'-'+id;
		if($('#list_add_zone').length){
			if($(itemlist).length){
				$(itemlist).addClass('removed-item');
				$(itemlist).remove();
			}
			if($(itemnew).length){
				$(itemnew).addClass('removed-item');
				$(itemnew).remove();
			}
			updateTotalContainer('#list_add_zone');
		}
	}

	function removeItemList(item, index, droplist){
		$(item).addClass('removed-item');
		setInterval(function(){
			$(droplist).eq(index).remove();
			clearInterval();
		},500);
	}

	function getContainerType(item, type){
		var list = [];
		$(item).find('li[item-type="'+type+'"]').each(function(index){
			list.push($(this).attr('item-id'));
		});
		return list;
	}

	function getContainerItems(item){
		var list = [];
		$(item).find('li').each(function(index){
			list.push({id:$(this).attr('item-id'),type:$(this).attr('item-type'),order:index+1});
		});
		return list;
	}

	function updateTotalContainer(list,total,itemClass){
		total = (total == undefined)?'#list_add_zone_total':total;
		if(itemClass == undefined)
			$(total).html($(list+' li').length);
		else
			$(total).html($(list+' li.'+itemClass).length);
	}

	function updateSidebar(list, type, id, title, html){
		overlayContent('#editor-sidebar');
		var itemid = '#item-'+type+'-'+id;
		if($(itemid).length){
			$(itemid).find('.item-menu-title').html(title);
		}else{
			if(!$('#'+list).length){ //create sidebar with sortable list
				createSidebar(type,id,title).done(function(){
					if($(itemid).length)
						$(itemid).find('.item-menu-title').html(title);
				});
			}else{
				$('#'+list).append(html);
				addEditAction(itemid+' .item-menu-edit');
			}
		}
		removeOverlay('#editor-sidebar');
	}

	function createSidebar(type, id, title){
		var container = '#sidebar-'+type+'-items';
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    return $.ajax({
      method: "POST",
      url: "{{ route('editor.sidebar.create') }}",
      data: {id: id, type: type, title: title},
      success:function(data){
      	removeOverlay('#editor-sidebar');
        switch(data.meta.status){
          case 'ok'		: $(container).find('.panel').html(data.data.sidebar);
          							showToast('Editor','se creó la lista de elementos','success');
          							sortEditorLists();
          							actionButtons();
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-sidebar');
      	showMessage('Aviso','Se produjo un error al intentar crear la lista de elementos, intente nuevamente','warning');
      }
    });
	}

	function addMenuItems(items){
		if(!items.items.length){
			showToast('Editor','la lista de elementos está vacía','info');
			return false;
		}
		var container = '#list_add_zone';
		$.ajaxSetup({ headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"} });
    $.ajax({
      method: "POST",
      url: "{{ route('editor.items.order') }}",
      data: JSON.stringify(items),
      beforeSend: function(){
      	overlayContent('#editor-content');
      },
      success:function(data){
      	removeOverlay('#editor-content');
        switch(data.meta.status){
          case 'ok'		: $(container).html(data.data.content);
          							showToast('Editor','se creó la lista de elementos','success');
          							updateTotalContainer('#list_add_zone');
          							break;
          case 'error': showMessage('Error','se produjo un error:<br><b>'+data.data.message+'</b>','danger'); break;
          default : break;
        }
      },
      error: function(){
      	removeOverlay('#editor-content');
      	showMessage('Aviso','Se produjo un error al intentar crear la lista de elementos, intente nuevamente','warning');
      }
    });
	}

function updatePageMain(id){
	var menu = getMenu();
	menu.page = id;
	saveMenu(menu);
}

function mainPageAction(){
	$('.page-menu-main').unbind();
	$('.page-menu-main').on('click',function(e){
		e.preventDefault();
		var id = $(this).attr('item-id');
		var type = ($(this).attr('item-type') == 'page');
		if(!type)
			return false;
		updatePageMain(id);
		saveMenuSystem().done(function(){
			showToast('Página principal','se actualizó la página principal del menú','success');
			if( getActualView() ==  'resume')
					showMenuResume();
		});
		return false;
	});
}

</script>